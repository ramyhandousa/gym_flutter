@extends('admin.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">

                <form action="{{route('reports.index')}}" method="GET" class="app-search">
                    {{--<a href=""><i class="fa fa-search" style="position: absolute;  top: 10px; right: 165px;color: green;"> </i></a>--}}
                    <input type="text" name="name" placeholder="Search..." class="form-control">

                </form>

            </div>
            <h4 class="page-title">قائمة تقارير الحجوزات </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box  ">
                <div class="row">
                    @foreach($reports as $report)

                            <div class="col-lg-3 col-md-6">
                                <div class="card-box widget-user">
                                    <div>
                                        <a href="{{route('gym_data_report')}}?id={{$report->gym->id}}">
                                            <img src="{{URL('/')}}  {{ '/' .$report->gym->image}}" class="img-responsive img-circle" alt="user">

                                        </a>
                                        <div class="wid-u-info">
                                            <h4 class="m-t-0 m-b-5 font-600">{{$report->gym->name}}</h4>
                                            <p class="text-muted m-b-5 font-13">{{$report->gym->email}}</p>
                                            <small class="text-warning"><b>{{$report->gym->phone}}</b></small>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>
        function accept(fff) {
            var url = $(fff).attr('data-href');
            swal({
                title: "هل انت متأكد من التفعيل ؟",
                text: "سوف تكتب رسالة لسبب التفعيل.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#0edd35",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-success waves-effect waves-light',
                closeOnConfirm: false,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    window.location.href = url
                }
            });
        }

        function suspend(fff) {
            var url = $(fff).attr('data-href');
            swal({
                title: "هل تريد تعطيل المستخدم ؟",
                text: "سوف تكتب رسالة لبيان سبب التعطيل.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: false,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    window.location.href = url
                }
            });
        }

    function Delete(fff) {
    var url = $(fff).attr('data-href');
    swal({
    title: "هل انت متأكد من حذفه؟",
    text: "سوف تكتب رسالة لبيان سبب الحذف.",
    type: "error",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "موافق",
    cancelButtonText: "إلغاء",
    confirmButtonClass: 'btn-danger waves-effect waves-light',
    closeOnConfirm: false,
    closeOnCancel: true,
    }, function (isConfirm) {
    if (isConfirm) {
    window.location.href = url
    }
    });
    }


    @if(session()->has('errors'))
    setTimeout(function () {
        showErrors('{{ session()->get('errors') }}');
    }, 1000);

    @endif

    function showErrors(message) {

        var shortCutFunction = 'error';
        var msg = message;
        var title = 'فشل!';
        toastr.options = {
            positionClass: 'toast-top-center',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };

        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;

    }
    
    </script>


@endsection

