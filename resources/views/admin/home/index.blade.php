@extends('admin.layouts.master')
@section('title', 'الصفحة الرئيسية')


@section('content')

    @can('statistics_manage')
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('maincp.control_panel')</h4>
        </div>
    </div>


    

        <div class="row statistics">

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> @lang('trans.clients')</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$usersCount}}</h2>
                            <p class="text-muted m-b-0">عدد المستخدمين المسجلين في التطبيق  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> عدد النوادي الرياضية المضافة في النظام     </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$gym_count_admin}}</h2>
                            <p class="text-muted m-b-0">عدد النوادي الرياضية المضافة في النظام  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- end col -->


        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد المدن المسجلة في النظام</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$citiesCount}}</h2>
                            <p class="text-muted m-b-0">  عدد المدن المسجلة في النظام  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الأحياء المسجلة في النظام      </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$cityChildren}}</h2>
                            <p class="text-muted m-b-0">عدد الأحياء المسجلة في النظام</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات الجديدة التي لم يتم الموافقة عليها بعد في النظام </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderPending}}</h2>
                            <p class="text-muted m-b-0"> عدد الحجوزات الجديدة التي لم يتم الموافقة عليها بعد في النظام   </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>



        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات التي تمت الموافقة عليها ولم تنفذ بعد</h4>
                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-group-work zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0"> {{$orderAccepted}} </h2>
                            <p class="text-muted m-b-0"> عدد الحجوزات التي تمت الموافقة عليها ولم تنفذ بعد</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <!-- end col -->


        <!-- end col -->

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> عدد الحجوزات في اليوم الحالي   </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-group-work zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0"> {{$orderToday}} </h2>
                            <p class="text-muted m-b-0">عدد الحجوزات في اليوم الحالي</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- end col -->

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات الملغاة من قبل الأندية الرياضية       </h4>
                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-eye-off zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderRefuseByGym}}  </h2>
                            <p class="text-muted m-b-0">عدد الحجوزات الملغاة من قبل الأندية الرياضية   </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات الملغاة من قبل المستخدمين قبل تأكيد رد النادي الرياضي    </h4>
                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-eye-off zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderRefuseByUser}}  </h2>
                            <p class="text-muted m-b-0">عدد الحجوزات الملغاة من قبل المستخدمين قبل تأكيد رد النادي الرياضي   </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> مجمل أرباح التطبيق من الحجوزات المكتملة    </h4>
                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-eye-off zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$walletOrders}}  </h2>
                            <p class="text-muted m-b-0"> مجمل أرباح التطبيق من الحجوزات المكتملة   </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> عدد رسائل تواصل معنا   الغير مقروءة  </h4>
                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-eye-off zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$messageNotReadCount}}  </h2>
                            <p class="text-muted m-b-0"> عدد رسائل تواصل معنا   </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- end col -->
    </div>
    
    @else
     <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">مرحبا بك في تطبيق الجيم</h4>
        </div>
    </div>
    
    @endcan

    
@endsection
