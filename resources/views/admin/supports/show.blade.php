@extends('admin.layouts.master')

@section('title' , __('maincp.message_details'))

@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                <a href="{{ route('support.index') }}"
                   class="btn btn-custom  waves-effect waves-light"> @lang('maincp.back')<span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                </a>
            </div>
            <h4 class="page-title">@lang('maincp.message_details') </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="bg-picture card-box">
                <div class="profile-info-name">
                    <div class="profile-info-detail">


                        @if($message->user_id != "")

                            <h3
                                        class="m-t-0 m-b-10">{{($message->user)?($message->user->name)?: $message->user->username :'' }}</h3>

                        @else

                            <h3 c lass="m-t-0 m-b-10">{{ $message->name }}</h3>


                        @endif



                        @if($message->type_id == 1)
                            <span class="label label-danger">@lang('maincp.complaints')</span>
                        @elseif($message->type_id == 2)
                            <span class="label label-success">@lang('maincp.suggestions')</span>
                        @else
                            <span class="label label-inverse">@lang('maincp.others')</span>

                        @endif
                        <p class="m-t-10">{{ $message->message }}</p>
                        <p>
                            <i class="fa fa-calendar"></i> {{ $message->created_at->format('Y/m/d  ||  H:i:s ') }}
                            <i class="fa fa-envelope"></i> @if($message->user) {{ $message->user->email }} @endif
                        </p>
                        <p>
                            <i class="fa fa-phone"></i> @if($message->user_id != "") {{ $message->user->phone }} @else {{ $message->phone }} @endif
                        </p>


                    </div>


                    <div id="supportReplies">


                        @foreach(App\Models\Support::orderBy('created_at', 'desc')->whereParentId($message->id)->get() as $row)
                            <div class="card bg-lightdark p-20 m-t-20">
                                {{ $row->message }}
                            </div>
                            <p class="m-t-10">

                            <div class="row">

                                <div class="col-md-6">
                                    <i class="fa fa-calendar"></i> {{ $row->created_at->format('Y/m/d  ||  H:i:s ') }}
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <!--/ meta -->


        </div>
        <div class="col-md-6 col-xs-12">
            <form method="post" action="{{ route('support.reply',$message->id) }}" class="card-box">
                            <span class="input-icon icon-right">
                                <textarea rows="5" class="form-control" name="message" required
                                          placeholder="@lang('maincp.reply_to_current_message')   ..."
                                          data-parsley-error-message="@lang('trans.message_required') "></textarea>
                            </span>


                <div class="p-t-10">
                    <button class="btn btn-sm btn-primary waves-effect waves-light">@lang('maincp.send')
                        <i style="display: none;" id="spinnerDiv" class="fa fa-spinner fa-spin"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>



@endsection


@section('scripts')


    <script>


        @if(session()->has('success'))

        setTimeout(function () {
            showMessage('{{ session()->get('success') }}');
        }, 3000);


        @endif



        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('support.contact.delete') }}',
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                            }

                            $tr.find('td').fadeOut(1000, function () {
                                $tr.remove();
                            });
                        }
                    });
                } else {

                    swal({
                        title: "تم الالغاء",
                        text: "انت لغيت عملية الحذف تقدر تحاول فى اى وقت :)",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "موافق",
                        confirmButtonClass: 'btn-info waves-effect waves-light',
                        closeOnConfirm: false,
                        closeOnCancel: false

                    });

                }
            });
        });


        function showMessage(message) {

            var shortCutFunction = 'success';
            var msg = message;
            var title = 'نجاح!';
            toastr.options = {
                positionClass: 'toast-top-center',
                onclick: null,
                showMethod: 'slideDown',
                hideMethod: "slideUp",
            };
            var $toast = toastr[shortCutFunction](msg, title);
            // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;


        }


    </script>

    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);


            $('#spinnerDiv').show();

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data.status == true) {


                        console.log(data.data.message);
                        $('#spinnerDiv').hide();
                        $("[name='message']").val('');
                        $("[name='reply_type']").val('');


                        if (data.data.reply_type == 0)
                            typeReply = " إرسال عبر رسائل (SMS) "
                        else
                            typeReply = " إرسال عبر البريد الإلكتروني "

                        var div = "<div class='card bg-lightdark p-20 m-t-20 show' >"
                            + data.data.message
                            + "</div>"
                            + "<p class=\"m-t-10\">"
                            + "<div class='row'>"
                            + "<div class='col-md-6'>"
                            + "<i class='fa fa-calendar'></i> " + data.data.created
                            + "</div>"
                            + "</div>"
                            + "</div>"


                        $('#supportReplies').prepend(div);

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }

                    if (data.status == false) {

                        // var shortCutFunction = 'error';
                        // var msg = data.message;
                        // var title = 'نجاح';
                        // toastr.options = {
                        //     positionClass: 'toast-top-left',
                        //     onclick: null
                        // };
                        // var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        // $toastlast = $toast;


                        setTimeout(function () {
                            $('#spinnerDiv').hide();
                        }, 1000);
                    }




                    {{--setTimeout(function () {--}}
                    {{--window.location.href = '{{ route('categories.index') }}';--}}
                    {{--}, 3000);--}}
                },
                error: function (data) {
                    $('#spinnerDiv').hide();
                }
            });
        });

    </script>

@endsection


