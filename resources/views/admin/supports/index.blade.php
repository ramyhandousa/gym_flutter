@extends('admin.layouts.master')

@section('title', __('maincp.call_us'))

@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">

              <a href="{{ route('types.index') }}"
                                   class="btn btn-custom  waves-effect waves-light" style="margin-left: 10px;">
                                    أنواع الرسائل 
             </a>
             
                <button type="button" class="btn btn-custom  waves-effect waves-light"
                        onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                </button>




            </div>

            <h4 class="page-title">@lang('maincp.call_us')</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">

                <div class="dropdown pull-right">
                </div>

                <h4 class="header-title m-t-0 m-b-30">@lang('maincp.contact_us')</h4>

                <table class="table m-0  table-striped table-hover table-condensed" id="datatable-fixed-header">
                    <thead>
                    <tr>
                        <th>
                            @lang('maincp.type_of_sender')
                        </th>

                        <th>@lang('maincp.mobile_number') </th>

                        <th>@lang('maincp.date_of_the_message') </th>
                        <th>الرسالة  </th>
                        <th>@lang('maincp.choose') </th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach($supports as $row)
                        <tr>
                            <td>

                                {{ optional($row->user)->name ?: 'لا يوجد اسم ' }}

                            </td>

                            <td>
                                @if($row->user != '')
                                    {{ ($row->user->phone)?:'--' }}
                                @else
                                    {{ $row->phone }}
                                @endif
                            </td>



                            <td>{{ $row->created_at->format('F Y d') }}</td>
                            <td>{{ $row->message }}</td>
                            <td>
                                @if(count($row->children) > 0)

                                @else
                                    <button type="button" id="myReplyMessage{{$row->id}}"   data-toggle="modal" data-target="#exampleModalCenter{{$row->id}}">
                                        <i class="fa fa-reply"></i>
                                    </button>
                                @endif

                                {{--<a href="{{ route('support.show', $row->id) }}"--}}
                                   {{--class="btn btn-icon btn-xs waves-effect btn-info m-b-5">--}}
                                    {{--<i class="fa fa-reply"></i>--}}
                                {{--</a>--}}
                                <a href="javascript:;" id="elementRow{{ $row->id }}" data-id="{{ $row->id }}" data-url="{{ route('support.contact.delete') }}"
                                   class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>
                        </tr>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter{{$row->id}}" tabindex="-1"
                                role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle"> الرد علي {{optional($row->user)->name}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{ route('support.reply',$row->id) }}" >
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <textarea type="text"   name="message"
                                                       parsley-trigger="change" required
                                                       placeholder="نص الرسالة..." class="form-control"
                                                          data-parsley-required-message="هذا الحقل إلزامي"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                                            <button type="submit"   id="spinnerDiv" class="btn btn-primary hideButton">إرسال الرد   </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                    @endforeach
                    </tbody>
                </table>


                {{--<div class="articles">--}}

                {{--                    @include('admin.categories.load')--}}

                {{--</div>--}}
            </div>
        </div><!-- end col -->

    </div>
    <!-- end row -->



@endsection


@section('scripts')


    <script>


        @if(session()->has('success'))

        setTimeout(function () {
            showMessage('{{ session()->get('success') }}');
        }, 3000);


        @endif



        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            var $url = $(this).attr('data-url');

            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('support.contact.delete') }}',
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                            }

                            $tr.find('td').fadeOut(1000, function () {
                                $tr.remove();
                            });
                        }
                    });
                } else {

                    swal({
                        title: "تم الالغاء",
                        text: "انت لغيت عملية الحذف تقدر تحاول فى اى وقت :)",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "موافق",
                        confirmButtonClass: 'btn-info waves-effect waves-light',
                        closeOnConfirm: false,
                        closeOnCancel: false

                    });

                }
            });
        });


        function showMessage(message) {

            var shortCutFunction = 'success';
            var msg = message;
            var title = 'نجاح!';
            toastr.options = {
                positionClass: 'toast-top-center',
                onclick: null,
                showMethod: 'slideDown',
                hideMethod: "slideUp",
            };
            var $toast = toastr[shortCutFunction](msg, title);
            // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;


        }
    </script>

    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            $('.hideButton').hide();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data.status == true) {
                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        $('.hideButton').hide();
                        setTimeout(function () {
                            location.reload();
                        }, 1500);

                    }

                    if (data.status == false) {

                        var shortCutFunction = 'error';
                        var msg = data.message;
                        var title = 'فشل';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        setTimeout(function () {
                            $('.hideButton').show();
                        }, 1000);
                    }

                },
                error: function (data) {
                    $('#spinnerDiv').hide();
                }
            });
        });

    </script>



@endsection


