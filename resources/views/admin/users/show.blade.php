@extends('admin.layouts.master')
@section('title', __('maincp.user_data'))
@section('content')


    <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('users.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">@lang('maincp.user_data') </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            @if($user->defined_user == 'gym')
                                                <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="true">الأيام المتاحة له </a></li>
                                                <li class=""><a href="#tab3" data-toggle="tab" aria-expanded="true">     الصور الخاصه لديه  </a></li>
                                            @endif
                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>@lang('maincp.personal_data')</h4>
                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.full_name') :</label>
                                                            <input class="form-control" value="{{ $user->name }}"><br>
                                                        </div>

                                                        @if($user->phone )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.mobile_number') :</label>
                                                                <input class="form-control" value="{{ $user->phone }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($user->email )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.e_mail')  :</label>
                                                                <input class="form-control" value="{{ $user->email }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($user->city )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>الحي  :</label>
                                                                <input class="form-control" value="{{ optional($user->city)->name  }}"><br>
                                                            </div>
                                                        @endif

                                                        @if( optional($user->city)->parent )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>المدينة  :</label>
                                                                <input class="form-control" value="{{ optional($user->city->parent)->name  }}"><br>
                                                            </div>
                                                        @endif
                                                        

                                                      <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                           <label>@lang('maincp.address') :</label>
                                                           <input class="form-control" value="{{ $user->address }}"><br>
                                                       </div>

                                                        <!--<div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">-->
                                                        <!--    <label>@lang('maincp.account_access_count'):</label>-->
                                                        <!--    <input class="form-control" value="{{ $user->login_count > 0 ? $user->login_count : 0 }} @lang('maincp.once')"><br>-->
                                                        <!--</div>-->
                                                    </div>



                                                    <div class="col-sm-4">
                                                        <div class="card-box">
                                                            <div class="row">

                                                                <div class="card-box" style="overflow: hidden;">
                                                                    <h4 class="header-title m-t-0 m-b-30">@lang('institutioncp.personal_image')</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage($user->image, request()->root().'/public/assets/admin/images/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage($user->image, request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                                            </a>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane m-t-10 fade " id="tab2">

                                                @if( $filterDays && count( $filterDays ) > 0)
                                                <div class="row">

                                                    <h3 class="plan-title">تفاصيل الايام المتاحة</h3>
                                                    @foreach($filterDays as $day)
                                                    <article class="pricing-column col-lg-3 col-sm-6">

                                                    <div class="inner-box card-box">
                                                        <div class="plan-header text-center">
                                                            <h3 class="plan-title"> يوم {{optional($day->day)->name}}</h3>
                                                        </div>
                                                        <ul class="plan-stats list-unstyled ">


                                                            <li>يبدا من  : {{$day->time_from}}</li>

                                                            <li>وينتهي في  : {{$day->time_to}}</li>

                                                        </ul>
                                                    </div>
                                                </article>
                                                    @endforeach
                                                </div>
@else
                                                    لا يوجد اي أيام لهذا المستخدم
                                                @endif

                                            </div>
                                            <div class="tab-pane m-t-10 fade " id="tab3">

                                                @if( $user['gallery'] && count( $user['gallery']) > 0)
                                                <div class="row">
                                                    @foreach($user['gallery'] as $value)
                                                    <article class="pricing-column col-lg-3 col-sm-6">

                                                    <div class="card-box" style="overflow: hidden;">
                                                        {{--<h4 class="header-title m-t-0 m-b-30">الصورة رقم </h4>--}}
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <a data-fancybox="gallery"
                                                                   href="{{ $helper->getDefaultImage( request()->root().'/public/assets/admin/images/default.png',request()->root().'/public/assets/admin/images/default.png') }}">
                                                                    <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                         src="{{ $helper->getDefaultImage(request()->root().'/'.$value->url, request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                                </a>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </article>
                                                    @endforeach
                                                </div>
@else
                                                    لا يوجد اي صور لهذا الجيم
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>


    </form>

@endsection

