
@extends('admin.layouts.master')
@section('title', 'عرض  بياناتي ')
@section('styles')

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap -->

    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/bootstrap-rtl.min.css">
    <!-- <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
    <link rel="stylesheet" href='{{request()->root()}}/public/gym/css/animate.css'>
    <!-- time picker -->
    <link rel="stylesheet" type="text/css" href="{{request()->root()}}/public/gym/css/jquery.ptTimeSelect.css" />
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />
    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/style.css">


@endsection

@section('content')


    <div class="container">
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                من فضلك إختار موقعك من علي الخريطة
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('Gym.testImage') }}" id="main-form" enctype="multipart/form-data" data-parsley-validate
                  novalidate>
            {{ csrf_field() }}
            <!-- map -->

                <!-- upload image -->
                <div class="col-12 field wow fadeInDown{{ $errors->has('files') ? ' has-error' : '' }}">
                    <div class="text-center">
                        <input type="file"  id="files" name="images[]" multiple />
                        <input type="hidden" id="myFiles" name="myFiles[]">
                        <label for="files" class="custom-file-upload text-center">
                            <i class="fas fa-camera"></i>
                        </label>
                        @if($errors->has('files'))
                            <p class="help-block">
                                {{ $errors->first('files') }}
                            </p>
                        @endif

                        @if($errors->any())
                            <h4 style="color: red">{{$errors->first()}}</h4>
                        @endif
                    </div>

                </div>
                <!--  -->

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="col-md-12 form-group wow fadeInUp">
                    <button type="submit" id="mySubmit" class="btn btn-send"> تأكيد </button>
                </div>
            </form>
        </div>

    </div>



@endsection


@section('scripts')

    {{--<script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery-3.2.1.min.js"></script>--}}
    {{--    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/bootstrap.min.js"></script>--}}
    <!-- <script type="text/javascript" src="js/owl.carousel.min.js"></script> -->
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/wow.min.js"></script>
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/scripts.js"></script>
    <!-- time picker scripts -->
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery.ptTimeSelect.js"></script>
    <!-- map script -->

    <script>



//        $(document).on('change','#files',function (value) {
//
//
//
//            if (parseInt( $(".pip").length) > 0){
//
//                swal({
//                    title: "سوف يتم اخد الصورة الاولي فقط شخصية  ",
//                    text: "سيتم إضافة الباقي لك في المعرض الخاص بك بحد اقصي 5 صور",
//                    type: "success",
////                        showCancelButton: true,
//                    confirmButtonColor: "#DD6B55",
//                    confirmButtonText: "موافق",
//                    confirmButtonClass: 'btn-success waves-effect waves-light',
//                    closeOnConfirm: true,
////                        closeOnCancel: true,
//                });
//
//            }
//
//
//        })

        $('#files').bind('change', function() {

//            console.log(this.value)
//               var  inputs =     this.value,
//                    arr = [];

//            for(var i=0, len=inputs.length; i<len; i++){
//                if(inputs[i].type === "hidden"){
//                    arr.push(inputs[i].value);
//                }
//            }
            //this.files[0].size gets the size of your file.
           console.log(document.getElementsByClassName("imageThumb"));

        });



    </script>


@endsection