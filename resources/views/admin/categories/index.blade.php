@extends('admin.layouts.master')

@section('title', __('maincp.cities'))

@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15 ">
                {{--<a href="{{ route('categories.create') }}@if(request('type') == 'subcategories' )?type=subcategories  @endif"--}}
                   {{--type="button" class="btn btn-custom waves-effect waves-light"--}}
                   {{--aria-expanded="false">--}}
                {{--<span class="m-l-5">--}}
                {{--<i class="fa fa-plus"></i>--}}
                {{--</span>--}}
                    {{--إضافة القسم--}}
                {{--</a>--}}
            </div>
            <h4 class="page-title">{{$pageName}} </h4>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="zmdi zmdi-more-vert"></i> --}}
                    {{--</a>--}}

                </div>

                <h4 class="header-title m-t-0 m-b-30"> إدارة {{$pageName}}</h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        {{--<th>صورة القسم</th>--}}
                        <th>اسم القسم </th>
                        <th>اسم صاحب القسم </th>
                        @if(request('type') == 'subcategories' )
                            <th>اسم القسم الرئيسي </th>
                        @endif

                        <th>تاريخ القسم </th>
                        <th>@lang('trans.status')</th>

                        {{--                        <th>@lang('trans.created_at')</th>--}}
{{--                        <th>@lang('trans.options')</th>--}}
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($categories as $row)
                        <tr>

                            {{--<td>--}}
                                {{--<img style="width: 50px;  height: 50px;"--}}
                                     {{--src="{{ $helper->getDefaultImage($row->image, request()->root().'/public/assets/admin/images/default.png') }}"/>--}}
                            {{--</td>--}}

                            <td>{{ $row->name }}</td>
                            <td>{{ $row->userName }}</td>

                            @if(request('type') == 'subcategories' )
                                <td>{{ optional($row->parent)->name }}</td>
                            @endif

                            <td>
                                {{ $row->created_at->format("Y/m/d") }}
                            </td>

                            <td>

                                <div class="StatusActive{{ $row->id }}"  style="display: {{ $row->is_suspend == 0 ? "none" : "block" }}; text-align: center;">
                                    <img  width="23px" src="{{ request()->root() }}/public/assets/admin/images/false.png" alt="">
                                </div>

                                <div class="StatusNotActive{{ $row->id }}" style="display: {{ $row->is_suspend == 0 ? "block" : "none" }};  text-align: center;">
                                    <img width="23px" src="{{ request()->root() }}/public/assets/admin/images/ok.png" alt="">
                                </div>

                            </td>

                            {{--<td>--}}

                                {{--<a href="{{ route('categories.edit', $row->id) }}@if(request('type')  == 'subcategories' )?type=subcategories @endif"--}}
                                   {{--data-toggle="tooltip" data-placement="top"--}}
                                   {{--data-original-title=" تعديل"--}}
                                   {{--class="btn btn-icon btn-xs waves-effect  btn-info">--}}
                                    {{--<i class="fa fa-edit"></i>--}}
                                {{--</a>--}}


                                {{--<a href="javascript:;" data-id="{{ $row->id }}" data-type="0"--}}
                                   {{--data-url="{{ route('categories.suspend') }}"--}}
                                   {{--style="@if($row->is_suspend == 0) display: none;  @endif"--}}
                                   {{--class="btn btn-xs  btn-success success suspendElement suspend{{ $row->id }}"--}}
                                   {{--id="suspendElement" data-message="@lang('trans.activationElement')"--}}
                                   {{--data-toggle="tooltip" data-placement="top"--}}
                                   {{--title="" data-original-title="تفعيل">--}}
                                    {{--<i class="fa fa-unlock"></i>--}}

                                {{--</a>--}}

                                {{--<a href="javascript:;" data-id="{{ $row->id }}" data-type="1"--}}
                                   {{--data-url="{{ route('categories.suspend') }}"--}}
                                   {{--style="@if($row->is_suspend == 1) display: none;  @endif"--}}
                                   {{--class="btn btn-xs btn-trans btn-danger danger suspendElement unsuspend{{ $row->id }}"--}}
                                   {{--id="suspendElement"--}}
                                   {{--data-message="@lang('trans.stopActivationElememt')"--}}
                                   {{--data-toggle="tooltip" data-placement="top"--}}
                                   {{--title="" data-original-title="{{ __('trans.suspend') }}">--}}

                                    {{--<i class="fa fa-lock"></i>--}}
                                {{--</a>--}}


                                {{--<a href="javascript:;" data-url="{{ route('categories.delete') }}" id="elementRow{{ $row->id }}" data-id="{{ $row->id }}"--}}
                                   {{--class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">--}}
                                    {{--<i class="fa fa-remove"></i>--}}
                                {{--</a>--}}


                            {{--</td>--}}
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')



    <script>

        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                                $tr.find('td').fadeOut(1000, function () {
                                    $tr.remove();
                                });
                            }

                            if (data.status == false) {
                                var shortCutFunction = 'warning';
                                var msg =data.message;
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                            }
                        }
                    });
                }
            });
        });





        $(document).ready(function () {
            //$('#datatable').dataTable();
            //$('#datatable-keytable').DataTable( { keys: true } );
            $('#datatable-responsive').DataTable();

        });


    </script>


@endsection



