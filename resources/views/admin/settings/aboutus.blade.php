@extends('admin.layouts.master')
@section('title' ,__('maincp.about_us'))
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">

    {{ csrf_field() }}

    <!-- Page-Title -->

        <div class="row">
            <div class="col-sm-12 ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">@lang('maincp.data_about_the_application') </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('about_us_ar') ? 'has-error' : '' }}">
                            <label for="about_us_ar">عن التطبيق باللغة العربية   </label>
                            <textarea id="editor100200" class="form-control msg_body" required
                                      name="about_us_ar">{{ $setting->getBody('about_us_ar') }}</textarea>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('about_us_en') ? 'has-error' : '' }}">
                            <label for="about_us_en">عن التطبيق باللغة الإنجليزية  </label>
                            <textarea id="editor100200" class="form-control msg_body" required
                                      name="about_us_en">{{ $setting->getBody('about_us_en') }}</textarea>
                        </div>
                    </div>


                    {{--<div class="col-xs-6">--}}
                        {{--<div class="form-group {{ $errors->has('our_mission_ar') ? 'has-error' : '' }}">--}}
                            {{--<label for="about_us_ar">مهمتنا باللغة العربية   </label>--}}
                            {{--<textarea id="editor100200" class="form-control msg_body" required--}}
                                      {{--name="our_mission_ar">{{ $setting->getBody('our_mission_ar') }}</textarea>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xs-6">--}}
                        {{--<div class="form-group {{ $errors->has('our_mission_en') ? 'has-error' : '' }}">--}}
                            {{--<label for="about_us_en">مهمتنا باللغة الإنجليزية  </label>--}}
                            {{--<textarea id="editor100200" class="form-control msg_body" required--}}
                                      {{--name="our_mission_en">{{ $setting->getBody('our_mission_en') }}</textarea>--}}
                        {{--</div>--}}
                    {{--</div>--}}



                    {{--<div class="col-xs-6">--}}
                        {{--<div class="form-group {{ $errors->has('rate_us_ar') ? 'has-error' : '' }}">--}}
                            {{--<label for="about_us_ar">قيمنا باللغة العربية   </label>--}}
                            {{--<textarea id="editor100200" class="form-control msg_body" required--}}
                                      {{--name="rate_us_ar">{{ $setting->getBody('rate_us_ar') }}</textarea>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xs-6">--}}
                        {{--<div class="form-group {{ $errors->has('rate_us_en') ? 'has-error' : '' }}">--}}
                            {{--<label for="about_us_en">قيمنا باللغة الإنجليزية  </label>--}}
                            {{--<textarea id="editor100200" class="form-control msg_body" required--}}
                                      {{--name="rate_us_en">{{ $setting->getBody('rate_us_en') }}</textarea>--}}
                        {{--</div>--}}
                    {{--</div>--}}



                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
 <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
<script>


        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor2');

</script>

@endsection




