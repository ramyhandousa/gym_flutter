<!DOCTYPE html>
<html lang="ar">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title-->
    <title>اسهل</title>

    <!--Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{request()->route()}}/public/Site/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{request()->route()}}/public/Site/css/bootstrap-rtl.min.css">
    <link rel="stylesheet " href="{{request()->route()}}/public/Site/css/animate.css ">
    <link rel="stylesheet " href="{{request()->route()}}/public/Site/css/style.css ">
    <link rel="shortcut icon" type="image/ico" href="{{request()->route()}}/public/Site/images/ico.ico">
</head>

<body>
<!-- logo -->
<header>
    <div class="container">
        <img src="{{request()->route()}}/public/Site/images/logo.png" class="my-5">
    </div>
</header>
<!-- page content -->
<div class="container">
    <div class="row ltr">
        <div class="col-md-6 text-center">
            <img src="images/Mockup.png" class="img-fluid vector">
        </div>
        <div class="col-md-6 m-auto">
            <h1 class="py-3">
                مرحبا بك
            </h1>
            <h1 class="py-3">
                الخدمات المكتبية أصبحت الآن أسهل
            </h1>
            <p class="py-4 w-700">
                يمكنك الانضمام الآن لتصبح ضمن مزودين خدمات أسهل لتتمكن من عرض خدماتك وتسويقها بكل سهوله واستقبال طلبات المشاريع الجديدة
            </p>
            <button type="button" class="btn btn-modal px-4" data-toggle="modal" data-target="#signupModal">
                انضم الان
            </button>

            <!-- store -->
            <div class="my-5 py-5">
                <a href="">
                    <img src="{{request()->route()}}/public/Site/images/googleplay.png" class="img-fluid">
                </a>
                <a href="">
                    <img src="{{request()->route()}}/public/Site/images/appstore.png" class="img-fluid">
                </a>
            </div>
        </div>

    </div>
</div>
<!-- signupModal -->
<div class="modal fade" id="signupModal">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body container">
                <h5 class="text-center">
                    مرحبا بك, للانضمام برجاء ادخال البيانات الاتيه
                </h5>
                <form class="mx-5 px-5 mt-5 signup-form" action="">
                    <div class="form-group ">
                        <input type="text" class="form-control" id="userName">
                        <label for="userName" class="input-label">الاسم</label>
                    </div>
                    <div class="form-group ">
                        <input type="tel" class="form-control" id="phone">
                        <label for="phone" class="input-label">رقم الجوال</label>
                    </div>
                    <div class="form-group ">
                        <input type="email" class="form-control" id="email">
                        <label for="email" class="input-label">البريد الالكتروني</label>
                    </div>
                    <div class="form-group ">
                        <input type="password" class="form-control" id="password">
                        <label for="password" class="input-label">كلمة المرور</label>
                    </div>
                    <div class="form-group form-check px-0">
                        <h6>
                            التخصصات التي تعمل بها
                        </h6>
                        <div class="row">
                            <div class="col-md-4">
                                <input type="checkbox" id="1" name="category" value="">
                                <label for="1">تنسيق</label>
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="2" name="category" value="">
                                <label for="2">بحوث</label>
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="3" name="category" value="">
                                <label for="3">تلخيصات</label>
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="4" name="category" value="">
                                <label for="4">ترجمة</label>
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="5" name="category" value="">
                                <label for="5">باوربوينت</label>
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="6" name="category" value="">
                                <label for="6">سيرة ذاتيه</label>
                            </div>
                        </div>

                    </div>
                    <div class="form-group " id="terms">
                        <input type="checkbox" id="terms-check" name="category" value="">
                        <label for="terms-check" data-toggle="modal" data-target="#termsModal">الشروط والأحكام</label>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-signup">طلب انضمام</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- terms modal -->
<div class="modal" id="termsModal">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">

            <!--  Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- content -->
            <div class="modal-body p-5">
                <p>
                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد
                    النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم
                    أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق. هذا النص يمكن أن يتم تركيبه على أي تصميم دون
                    مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو
                    العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي
                    المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث
                    عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق. هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.
                </p>
                <p>
                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد
                    النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم
                    أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق. هذا النص يمكن أن يتم تركيبه على أي تصميم دون
                    مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.
                </p>
            </div>


        </div>
    </div>
</div>
<!--scripts -->
<script type="text/javascript " src="{{request()->route()}}/public/Site/js/jquery-3.2.1.min.js "></script>
<script type="text/javascript " src="{{request()->route()}}/public/Site/js/bootstrap.min.js "></script>
<script type="text/javascript" src="{{request()->route()}}/public/Site/js/wow.min.js"></script>
<script type="text/javascript" src="{{request()->route()}}/public/Site/js/scripts.js"></script>

</body>

</html>