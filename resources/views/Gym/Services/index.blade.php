@extends('Gym.layouts.master')

@section('title', __('maincp.cities'))

@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15 ">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong"
                   type="button" class="btn btn-custom waves-effect waves-light"
                   aria-expanded="false">
                <span class="m-l-5">
                <i class="fa fa-plus"></i>
                </span>
                    إضافة خدمة
                </a>
            </div>
            <h4 class="page-title">{{$pageName}} </h4>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">خدماتنا</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="row" method="post" id="myServices" action="{{ route('services_dashboard.store' ) }}">
                    {{ csrf_field() }}
                    @foreach($services as $item)
                        <div class="col-md-6">
                            <input type="checkbox" id="{{$item->id}}" name="services[]"
                                   @if($item->id ==  optional($gym_service->where('service_id', $item->id)->first())->service_id )checked @endif
                                    value="{{$item->id}}">
                            <label for="{{$item->id}}">{{$item->name}}</label>
                        </div>
                     @endforeach
                    <div class="col-md-12 modal-footer ">
                        <button type="submit" id="submitServices" class="btn btn-done">تأكيد</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal">الغاء</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="zmdi zmdi-more-vert"></i> --}}
                    {{--</a>--}}

                </div>

                <h4 class="header-title m-t-0 m-b-30"> إدارة {{$pageName}}</h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>صورة الخدمة</th>
                        <th>اسم الخدمة </th>
                        <th>حالة الخدمة </th>
                        <th>@lang('trans.options')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($gym_service as $row)
                        <tr>
                            <td style="width: 15%;">
                                <a data-fancybox="gallery"
                                   href="{{ $helper->getDefaultImage(request()->root() . optional($row->service)->image, request()->root().'/public/assets/admin/custom/images/default.png') }}">
                                    <img style="width: 50%; border-radius: 50%; height: 49px;"
                                         src="{{ $helper->getDefaultImage(request()->root() . optional($row->service)->image, request()->root().'/public/assets/admin/custom/images/default.png') }}"/>
                                </a>
                            </td>
                            <td>{{ optional($row->service)->name }}</td>
                            <td>
                                <div class="StatusActive{{ $row->id }}"  style="display: {{ $row->is_suspend == 0 ? "none" : "block" }}; text-align: center;">
                                    <img  width="23px" src="{{ request()->root() }}/public/assets/admin/images/false.png" alt="">
                                </div>
                                <div class="StatusNotActive{{ $row->id }}" style="display: {{ $row->is_suspend == 0 ? "block" : "none" }};  text-align: center;">
                                    <img width="23px" src="{{ request()->root() }}/public/assets/admin/images/ok.png" alt="">
                                </div>
                            </td>
                            <td>

                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="0"
                                   data-url="{{ route('services_dashboard.suspend') }}"
                                   style="@if($row->is_suspend == 0) display: none;  @endif"
                                   class="btn btn-xs  btn-success success suspendElement suspend{{ $row->id }}"
                                   id="suspendElement" data-message="@lang('trans.activationElement')"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="تفعيل">
                                    <i class="fa fa-unlock"></i>
                                </a>
                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="1"
                                   data-url="{{ route('services_dashboard.suspend') }}"
                                   style="@if($row->is_suspend == 1) display: none;  @endif"
                                   class="btn btn-xs btn-trans btn-danger danger suspendElement unsuspend{{ $row->id }}"
                                   id="suspendElement"
                                   data-message="@lang('trans.stopActivationElememt')"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="{{ __('trans.suspend') }}">
                                    <i class="fa fa-lock"></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')



    <script>

        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                                $tr.find('td').fadeOut(1000, function () {
                                    $tr.remove();
                                });
                            }
                            if (data.status == false) {
                                var shortCutFunction = 'warning';
                                var msg =data.message;
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                            }
                        }
                    });
                }
            });
        });





        $(document).ready(function () {
            //$('#datatable').dataTable();
            //$('#datatable-keytable').DataTable( { keys: true } );
            $('#datatable-responsive').DataTable();

        });


    </script>


@endsection



