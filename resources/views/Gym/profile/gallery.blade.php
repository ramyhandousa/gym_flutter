@extends('Gym.layouts.master')
@section('title', 'عرض  بياناتي ')
@section('styles')

@endsection

@section('content')
    <h1>معرض الصور لديك</h1>
    <form method="post" action="{{ route('Gym.updateGallery') }}"  enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="wrapper">

            @if(count($gallery) > 0)
                @foreach($gallery as $value)

                    <div class="col-xs-2">
                        <div class="form-group">
                            <label for="usernames"> الصورة   </label>
                            <a href="javascript:;" data-url="{{ route('Gym.deleteGallery') }}" id="elementRow{{ $value->id }}" data-id="{{ $value->id }}"
                               class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                <i class="fa fa-remove"></i>
                            </a>
                            <input type="file"  data-default-file="{{ request()->root() . '/' .$value->url }}" name="myImage[{{$value->id}}][url]" class="dropify" data-max-file-size="6M"/>
                        </div>
                    </div>
                @endforeach
            @endif

            @for ($i = 0;$i < 5 - count($gallery)  ;$i++)

                    <div class="col-xs-2">
                        <div class="form-group">
                            <label for="usernames">الصورة  </label>
                            <input type="file" name="images[]" class="dropify" data-max-file-size="6M"/>
                        </div>
                    </div>

            @endfor
        </div>

        <button type="submit" class="form-control"> تحديث المعرض </button>
    </form>
@endsection



@section('scripts')

<script>
    $('body').on('click', '.removeElement', function () {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');

        swal({
            title: "هل انت متأكد؟",
            text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
            type: "error",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "موافق",
            cancelButtonText: "إلغاء",
            confirmButtonClass: 'btn-danger waves-effect waves-light',
            closeOnConfirm: true,
            closeOnCancel: true,
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {id: id},
                    dataType: 'json',
                    success: function (data) {

                        if (data.status == true) {
                            var shortCutFunction = 'success';
                            var msg = 'لقد تمت عملية الحذف بنجاح.';
                            var title = data.title;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;

                            setTimeout(function(){// wait for 5 secs(2)
                                location.reload(); // then reload the page.(3)
                            }, 1200);
                        }
                        if (data.status == false) {
                            var shortCutFunction = 'warning';
                            var msg =data.message;
                            var title = data.title;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;

                        }
                    }
                });
            }
        });
    });



</script>

@endsection