@extends('Gym.layouts.master')
@section('title', __('maincp.users_manager'))


@section('styles')


    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{request()->root()}}/public/gym/css/jquery.ptTimeSelect.css" />

    <style>

    </style>

@endsection
@section('content')



    {{--<form method="POST" action="{{ route('classes_dashboard.store') }}" enctype="multipart/form-data"--}}
          {{--data-parsley-validate novalidate>--}}
    {{--{{ csrf_field() }}--}}

    <form method="Get" id="apply-form" action="{{ route('classes_dashboard.daysClasses') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة {{$pageName}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 showDays" >
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30"> إضافة {{$pageName}}</h4>

                    <div class="row">


                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}}   باللغة العربية</label>
                                <input type="text" name="name_ar"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   باللغة العربية{{$pageName}}  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="name_en"> {{$pageName}}   باللغة الإنجليزية</label>
                                <input type="text" name="name_en"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   باللغة الإنجليزية {{$pageName}}  ..."/>
                                <p class="help-block" id="error_name_en"></p>
                                @if($errors->has('name_en'))
                                    <p class="help-block">
                                        {{ $errors->first('name_en') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="captain_name_ar">إسم الكابتن   باللغة العربية</label>
                                <input type="text" name="captain_name_ar"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="      إسم الكابتن   باللغة العربية."/>
                                <p class="help-block" id="error_captain_name_ar"></p>
                                @if($errors->has('captain_name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('captain_name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="captain_name_en">إسم الكابتن   باللغة الإنجليزية</label>
                                <input type="text" name="captain_name_en"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   إسم الكابتن   باللغة الإنجليزية   ..."/>
                                <p class="help-block" id="error_captain_name_en"></p>
                                @if($errors->has('captain_name_en'))
                                    <p class="help-block">
                                        {{ $errors->first('captain_name_en') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}} وصف باللغة العربية</label>
                                <textarea type="text" name="description_ar" class="form-control m-input description" required
                                          placeholder="إدخل  وصف عن  الكلاس  العربية   "   ></textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('description_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('description_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}}  وصف باللغة الإنجليزية</label>
                                <textarea type="text" name="description_en" class="form-control m-input description" required
                                          placeholder="إدخل  وصف عن  الكلاس بالإنجليزي   "   ></textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('description_en'))
                                    <p class="help-block">
                                        {{ $errors->first('description_en') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" id="submitMyForm" type="submit">
                           إختيار الأيام
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->
            {{--<div class="col-md-4 showDays">--}}
                {{--<div class="card-box">--}}
                    {{--<div class="row">--}}

                        {{--<div class="col-xs-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="usernames">صورة الكلاس  </label>--}}
                                {{--<input type="file" name="image" class="dropify" data-max-file-size="6M" required data-parsley-required-message="من فضلك ارفع صورة للكلاس "/>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery.ptTimeSelect.js"></script>

    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>


    <script type="text/javascript">

        $(document).ready(function() {

            $('.js-example-basic-multiple').select2();

        });

        $('input[name=isOffer]').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.priceOffer').show();
            } else {
                $('.priceOffer').hide();
            }
        });


//        $('input[name=isOffer]').on('click', function (e) {
//            if ($(this).is(':checked')) {
//                $('.priceOffer').show();
//            } else {
//                $('.priceOffer').hide();
//            }
//        });
//
//        $('input[name=dealOfToday]').on('click', function (e) {
//
//            if ($(this).is(':checked')) {
//                $('.dealField').show();
//            } else {
//                $('.dealField').hide();
//            }
//        });

//        $( '.styled' ).one( "click", function() {
//            var shortCutFunction = 'success';
//            var msg = 'سوف يتم معاملة العرض علي انه من ضمن العروض اليومية ';
//            var title = 'نجاح';
//            toastr.options = {
//                positionClass: 'toast-top-left',
//                onclick: null
//            };
//            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
//            $toastlast = $toast;
//        });




        //        $('form').on('submit', function (e) {
        //
        //            e.preventDefault();
        //
        //            var formData = new FormData(this);
        //
        //            var form = $(this);
        //            form.parsley().validate();
        //
        //            if (form.parsley().isValid()){
        //                $('.loading').show();
        //
        //                $.ajax({
        //                    type: 'POST',
        //                    url: $(this).attr('action'),
        //                    data: formData,
        //                    cache: false,
        //                    contentType: false,
        //                    processData: false,
        //                    success: function (data) {
        //                        $('.loading').hide();
        //                        // $('form').trigger("reset");
        //                        console.log(data);
        //
        //                        var shortCutFunction = 'success';
        //                        var msg = data.message;
        //                        var title = 'نجاح';
        //                        toastr.options = {
        //                            positionClass: 'toast-top-left',
        //                            onclick: null
        //                        };
        //                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
        //                        $toastlast = $toast;
        //                        setTimeout(function () {
        //                            window.location.href = data.url;
        //                        }, 2000);
        //                    },
        //                    error: function (data) {
        //                    }
        //                });
        //            }else {
        //                $('.loading').hide();
        //            }
        //        });

    </script>

    <script type="text/javascript">
        $('#time input').ptTimeSelect();

    </script>
@endsection

