
@extends('Gym.layouts.master')
@section('title', 'عرض  بياناتي ')
@section('styles')

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap -->

    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/bootstrap-rtl.min.css">
    <!-- <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
    <link rel="stylesheet" href='{{request()->root()}}/public/gym/css/animate.css'>
    <!-- time picker -->
    <link rel="stylesheet" type="text/css" href="{{request()->root()}}/public/gym/css/jquery.ptTimeSelect.css" />
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />
    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/style.css">

    <style>
        .session {
            display: none;
        }
    </style>
@endsection

@section('content')

    <form method="POST" action="{{ route('classes_dashboard.update',  $class_days->first()->gym_class->id) }}" enctype="multipart/form-data"
    data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}

        <div class="col-md-12 wow fadeInDown">
            <div class="text-center">
                @if($errors->any())
                    <h4 style="color: red">{{$errors->first()}}</h4>
                @endif
                <h3>صورة الكلاس  </h3>
                <br>
                <input type='file' name="image" accept='image/*' onchange='openFile(event)'><br>
                <label for="files" class="custom-file-upload text-center clearMyphoto">
                    <i class="fas fa-camera"></i>
                    <img style="width: 50%" id='output'>

                </label>

                <span class="pip">
                         <a data-fancybox="gallery" style="width: 25%"
                            href="{{ $helper->getDefaultImage(request()->root().optional($class_days->first()->gym_class)->image, request()->root().'/public/assets/admin/custom/images/default.png') }}">
                                <img style="width: 25%; border-radius: 10%;"
                                     src="{{ $helper->getDefaultImage(request()->root() .optional($class_days->first()->gym_class)->image, request()->root().'/public/assets/admin/custom/images/default.png') }}"/>
                            </a>
                </span>

            </div>

            @foreach($data as $key => $value)
                <input type="hidden" name="{{$key}}" value="{{$value}}">
            @endforeach
            <h3>
                ايام العمل

            </h3>

            @if($days->where('day_id',1)->first()->working == 'open')
                <div class=" workdays">
                    <div class="check" id="check">
                        <input type="checkbox"
                               @if($class_days->where('day_id',1)->first()) checked @endif
                               id="1" name="day[1]" value="1">
                        <label for="1">السبت</label>
                    </div>

                    <div class="sessionClass-1  session"  @if($class_days->where('day_id',1)->first() ) style="display: block" @endif>
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[1][duration_session]" value="{{$class_days->where('day_id',1)->first()->duration_session}}" min="0" id="session1" oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>

                    <div id="time" class=" time-1"
                            @if($class_days->where('day_id',1)->first() ) style="display: block" @endif
                    >
                        <input type="hidden"  name="day[1][day_id]" value="1" >

                        <input type="text" id="from1" name="day[1][start]" class="form-control"
                                                                      value="{{$class_days->where('day_id',1)->first()->start}}"
                               placeholder="من" required>
                    </div>

                </div>
            @endif


            @if($days->where('day_id',2)->first()->working == 'open')
                <div class=" workdays">
                    <div class="check" id="check">
                        <input type="checkbox"
                               @if($days->where('day_id',2)->first()) checked @endif
                               id="2" name="day[2]" value="2">
                        <label for="2">الأحد</label>
                    </div>

                    <div class="sessionClass-2 session">
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[2][duration_session]" min="0" value="{{$class_days->where('day_id',2)->first()->duration_session}}"  id="session2"  oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>

                    <div id="time" class=" time-2"
                         @if($days->where('day_id',2)->first() ) style="display: block" @endif
                    >
                        <input type="hidden"  name="day[2][day_id]" value="2" >
                        <input type="text" id="from2" name="day[2][start]"
                               value="{{$class_days->where('day_id',2)->first()->start}}"
                               placeholder="من">
                    </div>
                </div>
            @endif


            @if($days->where('day_id',3)->first()->working == 'open')
                <div class=" workdays">
                    <div class="check" id="check">
                        <input type="checkbox"
                               @if($class_days->where('day_id',3)->first() ) checked @endif
                               id="3" name="day[3]" value="3">
                        <label for="3">الاثنين</label>
                    </div>

                    <div class="sessionClass-3  session">
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[3][duration_session]" min="0" value="{{optional($class_days->where('day_id',3)->first())->duration_session}}"  id="session3" oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>

                    <div id="time" class=" time-3"
                         @if($class_days->where('day_id',3)->first()) style="display: block" @endif
                    >
                        <input type="hidden"  name="day[3][day_id]" value="3" >
                        <input type="text" id="from3" name="day[3][start]"
                               value="{{optional($class_days->where('day_id',3)->first())->start}}"
                               placeholder="من">
                    </div>
                </div>
            @endif

            @if($days->where('day_id',4)->first()->working == 'open')
                <div class=" workdays">
                    <div class="check">
                        <input type="checkbox"
                               @if($class_days->where('day_id',4)->first() ) checked @endif
                               id="4" name="day[4]" value="4">
                        <label for="4">الثلاثاء</label>
                    </div>

                    <div class="sessionClass-4  session">
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[4][duration_session]" min="0" id="session4" value="{{ optional($class_days->where('day_id',4)->first())->duration_session}}"   oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>

                    <div id="time" class=" time-4"
                         @if($class_days->where('day_id',4)->first() ) style="display: block" @endif
                    >
                        <input type="hidden"  name="day[4][day_id]" value="4" >
                        <input type="text" id="from4" name="day[4][start]"
                               value="{{optional($class_days->where('day_id',4)->first())->start}}"
                               placeholder="من">
                    </div>
                </div>
            @endif

            @if($days->where('day_id',5)->first()->working == 'open')
                <div class=" workdays">
                    <div class="check">
                        <input type="checkbox"
                               @if($class_days->where('day_id',5)->first()) checked @endif
                               id="5" name="day[5]" value="5">
                        <label for="5">الأربعاء</label>
                    </div>

                    <div class="sessionClass-5  session">
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[5][duration_session]" min="0" id="session5" value="{{optional($class_days->where('day_id',5)->first())->duration_session}}"   oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>

                    <div id="time" class=" time-5"
                         @if($class_days->where('day_id',5)->first()) style="display: block" @endif
                    >
                        <input type="hidden"  name="day[5][day_id]" value="5" >
                        <input type="text" id="from5" name="day[5][start]"
                               value="{{optional($class_days->where('day_id',5)->first())->start}}"
                               placeholder="من">
                    </div>
                </div>
            @endif

            @if($days->where('day_id',6)->first()->working == 'open')
                <div class=" workdays">

                    <div class="check">
                        <input type="checkbox"
                               @if($class_days->where('day_id',6)->first()) checked @endif
                               id="6" name="day[6]" value="6">
                        <label for="6">الخميس</label>
                    </div>

                    <div class="sessionClass-6 session">
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[6][duration_session]" min="0" value="{{optional($class_days->where('day_id',6)->first())->duration_session}}"
                               id="session6" oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>

                    <div id="time" class=" time-6"
                         @if($class_days->where('day_id',6)->first() ) style="display: block" @endif
                    >
                        <input type="hidden"  name="day[6][day_id]" value="6" >
                        <input type="text" id="from6" name="day[6][start]"
                               value="{{optional($class_days->where('day_id',6)->first())->start}}"
                               placeholder="من">
                    </div>
                </div>
            @endif

            @if($days->where('day_id',7)->first()->working == 'open')
                <div class=" workdays">
                    <div class="check">
                        <input type="checkbox"
                               {{--@if($days->where('day_id',7)->first()->working == 'open') checked @endif --}}
                               id="7" name="day[7]" value="7">
                        <label for="7">الجمعة</label>
                    </div>
                    <div class="sessionClass-7  session">
                        <label for="userName">مدة الكلاس *</label>
                        <input type="number" name="day[7][duration_session]" id="session7" value="{{optional($class_days->where('day_id',7)->first())->duration_session}}"
                               min="0"  oninput="this.value = Math.abs(this.value)" class="form-control  number" required>
                    </div>
                    <div id="time" class=" time-7" @if($class_days->where('day_id',7)->first() ) style="display: block" @endif  >

                        <input type="hidden"  name="day[7][day_id]" value="7" >
                        <input type="text" id="from7" name="day[7][start]"
                               value="{{optional($class_days->where('day_id',7)->first())->start}}"
                               placeholder="من">
                    </div>
                </div>

            @endif



        </div>

        <div class="form-group text-right m-t-20 pull-left">
            <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                حفظ البيانات
            </button>
            <button onclick="window.history.back();return false;" type="reset"
                    class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                @lang('maincp.disable')
            </button>
        </div>

    </form>

@endsection


@section('scripts')

    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery-3.2.1.min.js"></script>
    {{--    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/bootstrap.min.js"></script>--}}
    {{--<script type="text/javascript" src="js/owl.carousel.min.js"></script>  --}}
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/wow.min.js"></script>
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/scripts.js"></script>
    <!-- time picker scripts -->
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery.ptTimeSelect.js"></script>


    <script>


        window.onload = function () {
            if (localStorage.getItem("hasCodeRunBefore") === null) {
                swal({
                    title: "تم تسجيل البيانات الأساسية  ",
                    text: "سوف يتم إضافة الايام وفقا لايام العمل في الجيم ",
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "موافق",
                    confirmButtonClass: 'btn-success waves-effect waves-light',
                    closeOnConfirm: true,
                },function (isConfirm) {

                    if (isConfirm) {
                        var shortCutFunction = 'warning';
                        var msg = 'سوف يتم إضافة اليوم وفقا لتواجد مدة الكلاس والتوقيت الصحيح ';
                        var title = 'تحذير';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                    }

                });
                localStorage.setItem("hasCodeRunBefore", true);
            }
        }




        $(':checkbox').on('change', function() {

            var isChecked = $(this).is(":checked");
            var   value     =  $(this).val();
            console.log(isChecked)
            console.log(value)

//            var inputTime = document.getElementById("from"+value);
//            var inputSession1 = document.getElementById("session"+value);
//            console.log(inputTime)
//            console.log(inputSession1)
//            if (isChecked){
//                $('#form').submit(function() {
//                    if ($.trim($("#from"+value)) === "" || $("#from"+value) === undefined) {
//                        alert('you did not fill out one of the fields');
//                        return false;
//                    }
//                });
//            }


        });



    </script>

    <script>
        var openFile = function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('output');
                output.src = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        };
    </script>

@endsection