@extends('Gym.layouts.master')
@section('title', 'الصفحة الرئيسية')


@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">البيانات الخاصة لديكم</h4>
        </div>
    </div>


    

        <div class="row statistics">

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> عدد الحجوزات الجديدة التي لم يتم الموافقة عليها</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderPending}}</h2>
                            <p class="text-muted m-b-0">عدد الحجوزات الجديدة التي لم يتم الموافقة عليها     </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> عدد الحجوزات التي تم الموافقة عليها ولم تنفذ     </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderAccepted}}</h2>
                            <p class="text-muted m-b-0">عدد الحجوزات التي تم الموافقة عليها ولم تنفذ   </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- end col -->


        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات اليوم ( بناءا علي اليوم )  </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderToday}}</h2>
                            <p class="text-muted m-b-0">عدد الحجوزات اليوم ( بناءا علي اليوم )  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات المكتملة مع التطبيق</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$orderComplete}}</h2>
                            <p class="text-muted m-b-0">عدد الحجوزات المكتملة مع التطبيق</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>



        <!-- end col -->
    </div>


    
@endsection
