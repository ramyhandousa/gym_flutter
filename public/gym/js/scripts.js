// =================== wow script ===================
new WOW().init();
// ==================================================
$(document).ready(function() {
    //=================== top-slider slider ===================
    // $('#top-slider').owlCarousel({
    //         loop: true,
    //         margin: 20,
    //         items: 5,
    //         nav: false,
    //         center: true,
    //         dots: true,
    //         autoplay: true,
    //         autoplayTimeout: 1000,
    //         responsive: {
    //             0: {
    //                 items: 1
    //             },
    //             600: {
    //                 items: 5
    //             },
    //             1000: {
    //                 items: 5
    //             }
    //         }
    //     })
    // ========================= workdays toggle =======================
    $('input[type="checkbox"]').click(function() {
        var inputValue = $(this).attr("id");
        $(".time-" + inputValue).toggle();
        $(".sessionClass-" +inputValue).toggle();
    });
    // ======================== upload images ==============================
    if (window.File && window.FileList && window.FileReader) {
        $("#files").on("change", function(e) {
            var files = e.target.files,
                filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function(e) {
                    var file = e.target;
                    $("<span class=\"pip\">" +
                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                        "<br/><span class=\"remove\">X</span>" +
                        "</span>").insertAfter("#files");
                    $(".remove").click(function() {
                        $(this).parent(".pip").remove();
                    });
                });
                fileReader.readAsDataURL(f);
            }
        });
    }
    // =================== time picker plugin call ================
    $('#time input').ptTimeSelect();

});
// ========================= map =======================
function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(51.508742, -0.120850),
        zoom: 5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}
// =================== toggle password visibility ===================
function togglePass() {

    var x = document.getElementsByClassName("pass");
    var i;
    for (i = 0; i < x.length; i++) {
        if (x[i].type === "password") {
            x[i].type = "text";
        } else {
            x[i].type = "password";
        }
    }
}
// =================== disabled signup btn ===================
$(function() {
    enable_cb();
    $("#terms-check").click(enable_cb);
});

function enable_cb() {
    if (this.checked) {
        $(".btn-signup").removeAttr("disabled");
    } else {
        $(".btn-signup").attr("disabled", true);
    }
}