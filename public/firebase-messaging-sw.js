// importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js');   
importScripts('https://www.gstatic.com/firebasejs/5.8.5/firebase-app.js'); 

// importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-messaging.js'); 
importScripts('https://www.gstatic.com/firebasejs/5.8.5/firebase-messaging.js');

 var config = {
    apiKey: "AIzaSyC0f8dd1dtf1h-wVxJjtiIk3dq2teCzWf0",
    authDomain: "reservaapp-599f0.firebaseapp.com",
    databaseURL: "https://reservaapp-599f0.firebaseio.com",
    projectId: "reservaapp-599f0",
    storageBucket: "",
    messagingSenderId: "865603333137",
    // appId: "1:865603333137:web:db1764a4afcc82a7ee95b0",
    // measurementId: "G-6J59S9T52C"
  };
  firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: 'https://i.pinimg.com/736x/61/f1/cb/61f1cb5e0db9cb346501c68fdf75a10d.jpg',
    
  };


self.addEventListener('notificationclick', function (event) {
  event.notification.close();

  var clickResponsePromise = Promise.resolve();
    clickResponsePromise = clients.openWindow('facebook');

  event.waitUntil(Promise.all([clickResponsePromise, self.analytics.trackEvent('notification-click')]));
});



  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
