<?php


Auth::routes();



Route::get('/', 'HomeController@index')->name('home');

Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');

Route::group(['prefix' => 'Gym' ], function () {


    Route::get('/login', 'LoginController@login')->name('Gym.login');
    Route::post('/login', 'LoginController@postLogin')->name('Gym.postLogin');

    // Password Reset Routes...

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('Gym.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('Gym.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('Gym.password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});

Route::group(['prefix' => 'Gym', 'middleware' => ['admin']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('Gym.home');

    Route::get('/myGallery', 'ProfileController@myGallery')->name('Gym.myGallery');
    Route::get('/myWallet', 'ProfileController@myWallet')->name('Gym.myWallet');

    Route::post('/updateGallery', 'ProfileController@updateGallery')->name('Gym.updateGallery');
    Route::post('/deleteGallery', 'ProfileController@deleteGallery')->name('Gym.deleteGallery');

    Route::resource('gym_profile', 'ProfileController');

    Route::resource('classes_dashboard', 'ClassessController');
    Route::get('daysClasses','ClassessController@daysClasses')->name('classes_dashboard.daysClasses');
    Route::get('edit_daysClasses','ClassessController@editClassDays')->name('classes_dashboard.editClassDays');

    Route::post('classes_dashboard/suspend', 'ClassessController@suspend')->name('classes_dashboard.suspend');
    Route::post('classes_dashboard/delete', 'ClassessController@delete')->name('classes_dashboard.delete');

    Route::post('changeMyPassword' ,'ProfileController@changePassword')->name('Gym.changePassword');

    Route::resource('services_dashboard', 'ServicesController');
    Route::post('services_dashboard/suspend', 'ServicesController@suspend')->name('services_dashboard.suspend');


    Route::resource('Order_Gym', 'Order_GymController');
    Route::post('Order_Gym/acceptedOrder', 'Order_GymController@acceptedOrder')->name('Order_Gym.acceptedOrder');
    Route::post('Order_Gym/refuseOrder', 'Order_GymController@refuseOrder')->name('Order_Gym.refuseOrder');


    Route::resource('contact_us_inbox', 'ContactUsController');
    Route::get('updateIsRead', 'ContactUsController@updateIsRead')->name('Gym.support.updateIsRead');
    Route::get('updateIsDeleted', 'ContactUsController@updateIsDeleted')->name('Gym.support.updateIsDeleted');
    Route::get('removeAllMessages', 'ContactUsController@removeAllMessages')->name('Gym.support.removeAllMessages');


    Route::get('information_admin','HomeController@information_admin')->name('information_admin');

    Route::post('/Gym_logout', 'LoginController@logout')->name('Gym.logout');

});


Route::get('myDays',function (){

    $days = \App\Models\GymDay::whereGymId(Auth::id())->get();
    return view('Gym.classes.days',compact('days'));
});

Route::get('qr-code-g', function (\Illuminate\Http\Request $request) {

    $user = \App\User::where('id',$request->id)->first();

    if (!$user || !$request->id){
        abort(404);
    }
//    $url = \URL('/') .'/api/my_gym/' .   $user->id;
    $url =  $user->id;
    \SimpleSoftwareIO\QrCode\Facades\QrCode::size(200)
        ->format('png')
        ->generate($url, public_path('qrcode.png'));

//    return view('qrCode',compact('url' ,'user'));
    return view('qrCode',compact('url' ,'user'));

});

Route::get('testingNow',function (\Illuminate\Http\Request $request){

    $order = \App\Models\Order::where('id',$request->id)->first();
    $name =  $order->user->name  ;
    $date =  $order->date  ;

     \SimpleSoftwareIO\QrCode\Facades\QrCode::encoding('ISO-8859-1')->size(200)
        ->format('png')
        ->merge($name , $date , public_path('qrcode.png'));


    return view('Gym.orders.qrCode',compact('name' ,'date'));
});