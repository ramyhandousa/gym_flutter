<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'namespace' => 'Api',
], function () {


    Route::group(['prefix' => 'Auth'], function () {

        Route::post('login','LoginController@login');
        Route::post('register', 'RegisterController@register');
        Route::post('forgetPassword', 'ForgotPasswordController@forgetPassword');
        Route::post('resetPassword', 'ForgotPasswordController@resetPassword');
        Route::post('checkCode', 'ResetPasswordController@checkCodeActivation');
        Route::post('resendCode', 'ResetPasswordController@resendCode');
        Route::post('changPassword', 'ResetPasswordController@changPassword')->middleware('apiToken');
        Route::post('editProfile', 'UserController@editProfile')->middleware('apiToken');
        Route::post('logOut','LoginController@logOut')->middleware('apiToken');

    });


    Route::apiResource('users', 'UserController');
    Route::post('users/makeFavourite','UserController@makeFavourite');
    Route::get('listFavourite','UserController@listFavourite');

    Route::apiResource('my_gym','GymController');
    Route::get('sessions_day' ,'GymController@sessions_day');


    Route::get('listNotification','UserController@listNotification');
    Route::post('users/ratePlace','UserController@ratePlace');

    Route::get('/cities','ListController@listCity');
    Route::get('/days','ListController@listDays');
    Route::get('/keywords','ListController@listKeywords');
    Route::get('/classifications','ListController@classifications');
    Route::get('/listPlaces','ListController@listPlaces');
    Route::get('/getPlaceById','ListController@getPlaceById');
    Route::get('/getPlaceDetails','ListController@getPlaceDetails');
    Route::get('/listOffers','ListController@listOffers');
    Route::get('/getOfferById','ListController@getOfferById');
    Route::get('/home_index','ListController@indexUser');
    Route::get('/listRatingGlobal','ListController@listRating');

    Route::group(['prefix' => 'search'], function () {
        Route::get('filter','SearchController@filterData');
    });
    
    Route::apiResource('categories', 'CategoriesController');
    Route::post('categories/editCategory','CategoriesController@editCategory');

    
    
     Route::group(['prefix' => 'settingUser'], function () {
        Route::post('update_front_places','SettingUserController@updateFrontPlaces');
        Route::post('update_details','SettingUserController@updateDetails');
        Route::post('dayPlaces','SettingUserController@dayPlaceWorking');
        Route::post('uploadImage','SettingUserController@uploadImage');
        Route::post('setting_notify','SettingUserController@settingNotify');
        Route::get('list_rating','SettingUserController@listRating');
    });


    Route::group(['prefix' => 'setting'], function () {
        Route::get('about_us','SettingController@aboutUs');
        Route::get('list_support','SettingController@getTypesSupport');
        Route::post('contact_us','SettingController@contactUs');

        Route::get('socialMedia','SettingController@socialMedia');
        Route::get('terms_user','SettingController@terms_user');
        Route::post('testNotify','SettingController@testNotify');
    });
    
    

    Route::apiResource('orders', 'OrderController');
    Route::group(['prefix' => 'my_orders'], function () {
        Route::post('edit_order', 'OrderController@editOrder');
        Route::post('rate', 'OrderController@rateGym');
        Route::post('scanOrder', 'OrderController@scanOrder')->middleware('scanOrder');
        Route::post('userRefuseOrder', 'OrderController@UserRefuseOrder');
        Route::get('listOrderAccepted', 'OrderController@listOrderAccepted');
        Route::get('listOrderFinish', 'OrderController@listOrderFinish');
        Route::get('listOrderRefuse', 'OrderController@listOrderRefuse');
    });

    Route::group(['prefix' => 'my_points'], function () {

        Route::get('/', 'PointController@allPoints');
        Route::get('/wallet', 'PointController@wallet');
        Route::post('confirmPoints', 'PointController@confirmPoints');
    });

});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});


Route::get('/test_orderNow', function() {
    $exitCode = \App\Models\Order::whereId(4)->first();

    return new \App\Http\Resources\scanOrderModel($exitCode);
    // return what you want
});




Route::get('/gym_days', function(Request $request) {
    $exitCode = \App\Models\GymDay::whereGymId($request->gymId)->get();

    return \App\Http\Resources\placeWorkingRecource::collection($exitCode);
    // return what you want
});


Route::get('sum_tra',function (){

    return \App\Models\Transaction::whereUserId(3)->sum('value');
});





