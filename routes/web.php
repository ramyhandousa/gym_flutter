<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();



Route::get('/', 'HomeController@index')->name('home');

Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');
    
    Route::group(['prefix' => 'administrator'], function () {


        Route::get('/login', 'Admin\LoginController@login')->name('admin.login');
        Route::post('/login', 'Admin\LoginController@postLogin')->name('admin.postLogin');
        
        // Password Reset Routes...
        
        Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
        Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
        Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
        Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');
        
    });
Route::group(['prefix' => 'administrator', 'middleware' => ['admin']], function () {

    Route::get('/', 'Admin\HomeController@index')->name('home');
    Route::get('/home', 'Admin\HomeController@index')->name('admin.home');


    // administrators --- Admin Panel users
    // add admin or some one who controll Or help for admin panel

    Route::resource('helpAdmin', 'Admin\HelpAdminController');
    Route::get('helpAdmin/{id}/delete', 'Admin\HelpAdminController@delete')->name('helpAdmin.delete');
    Route::post('helpAdmin/{id}/delete', 'Admin\HelpAdminController@deleteHelpAdmin')->name('helpAdmin.message.delete');
    Route::post('helpAdmin/{id}/suspend', 'Admin\HelpAdminController@suspendHelpAdmin')->name('helpAdmin.message.suspend');
    Route::get('user/{id}/delete', 'Admin\UsersController@delete')->name('user.for.delete');
    Route::post('user/suspend', 'Admin\HelpAdminController@suspend')->name('user.suspend');
    // Roles routes ..........
    Route::resource('roles', 'Admin\RolesController');
    Route::post('role/delete', 'Admin\RolesController@delete')->name('role.delete');


    Route::resource('users', 'Admin\UsersController');
    Route::post('accepted','Admin\UsersController@accpetedUser')->name('user.accepted');

    Route::get('settings/aboutus', 'Admin\SettingsController@aboutus')->name('settings.aboutus');
    Route::get('settings/taxs', 'Admin\SettingsController@taxs')->name('settings.taxs');
    Route::get('settings/termsGym', 'Admin\SettingsController@termsGym')->name('settings.termsGym');
    Route::get('settings/terms', 'Admin\SettingsController@terms')->name('settings.terms');
    Route::get('settings/suspendElement', 'Admin\SettingsController@suspendElement')->name('settings.suspendElement');
    Route::get('settings/minutes_control', 'Admin\SettingsController@minutes_control')->name('settings.minutes_control');
    Route::get('settings/points', 'Admin\SettingsController@points')->name('settings.points');

//        Route::resource('faqs', 'Admin\FaqsController');

    Route::get('/settings/app-general-settings', 'Admin\SettingsController@appGeneralSettings')->name('settings.app.general');
    Route::get('settings/contacts', 'Admin\SettingsController@contactus')->name('settings.contactus');


    Route::post('/settings', 'Admin\SettingsController@store')->name('administrator.settings.store');

    Route::post('contactus/reply/{id}', 'Admin\SupportsController@reply')->name('support.reply');
    Route::get('contactus', 'Admin\SupportsController@index')->name('support.index');
    Route::get('contactus/{id}', 'Admin\SupportsController@show')->name('support.show');
    Route::post('support/contact/delete', 'Admin\SupportsController@delete')->name('support.contact.delete');


    Route::resource('supports', 'Admin\SupportsController');
    Route::post('supports/delete', 'Admin\SupportsController@delete')->name('supports.delete');

    Route::resource('types', 'Admin\TypesSupportController');


//        Route::get('public/notifications', 'Admin\NotificationsController@publicNotifications')->name('public.notifications');
//        Route::get('public/notifications/create', 'Admin\NotificationsController@createPublicNotifications')->name('create.public.notifications');


    Route::post('city/delete/group', 'Admin\CitiesController@groupDelete')->name('cities.group.delete');
    Route::post('cities/delete', 'Admin\CitiesController@delete')->name('city.delete');
    Route::resource('cities', 'Admin\CitiesController');
    Route::post('city/suspend', 'Admin\CitiesController@suspend')->name('city.suspend');

    // -------------------------------------- categories .................
    Route::resource('categories', 'Admin\CategoriesController');
    Route::post('categories/delete', 'Admin\CategoriesController@delete')->name('categories.delete');
    Route::post('categories/suspend', 'Admin\CategoriesController@suspend')->name('categories.suspend');


    // -------------------------------------- Day .................
    Route::resource('days', 'Admin\DaysController');
    Route::post('days/delete', 'Admin\DaysController@delete')->name('brands.delete');


    // -------------------------------------- placeClassification .................
    Route::resource('placeClassification', 'Admin\PlaceClassificationController');
    Route::post('placeClassification/delete', 'Admin\PlaceClassificationController@delete')->name('placeClassification.delete');

    // -------------------------------------- Keywords .................
    Route::resource('keywords', 'Admin\KeywordsController');
    Route::post('keywords/delete', 'Admin\KeywordsController@delete')->name('keyword.delete');

    // -------------------------------------- Keywords .................
    Route::resource('facilities', 'Admin\facilitiesController');
    Route::post('facilities/delete', 'Admin\facilitiesController@delete')->name('facilities.delete');


    // -------------------------------------- purchaseTypes .................
    Route::resource('purchaseTypes', 'Admin\purchaseTypesController');
    Route::post('purchaseTypes/delete', 'Admin\purchaseTypesController@delete')->name('purchaseTypes.delete');


 
    // -------------------------------------- meals .................
    Route::resource('meals', 'Admin\MealsController');
    Route::post('meals/delete', 'Admin\MealsController@delete')->name('meals.delete');
    Route::post('meals/suspend', 'Admin\MealsController@suspend')->name('meals.suspend');
    
    
     // -------------------------------------- offers .................
    Route::resource('offersAdmin', 'Admin\OfferController');
    Route::post('offersAdmin/delete', 'Admin\OfferController@delete')->name('offersAdmin.delete');
    Route::post('offersAdmin/accepted', 'Admin\OfferController@accepted')->name('offersAdmin.accepted');



    Route::resource('reports', 'Admin\ReportsController');
    Route::get('gym_data_report', 'Admin\ReportsController@gym_data')->name('gym_data_report');


    Route::get('/testImageView', 'Admin\UsersController@testImageView')->name('Gym.testImageView');
    Route::post('/testImage', 'Admin\UsersController@testImage')->name('Gym.testImage');



    Route::resource('WalletAllGyms', 'Admin\WalletAllGymsController');

    Route::post('/logout', 'Admin\LoginController@logout')->name('administrator.logout');

});



Route::get('/sub', function (Illuminate\Http\Request $request) {

    $cities =\App\Models\City::whereParentId($request->id)->get();

    if (!empty($cities) && count($cities) > 0){
        return response()->json( $cities);
    }else{

        return response()->json(401);
    }


})->name('getSub');



Route::get('roles', function () {

    $user = auth()->user();
//    $user->retract('admin');
    $user->assign('*');
    Bouncer::allow('*')->everything();
    $user->allow('users_manage');
});


Route::post('user/update/token', function (Illuminate\Http\Request $request) {

    $user = \App\User::whereId($request->id)->first();


    if ($request->token) {
        $data = \App\Models\Device::where('device', $request->token)->first();
        if ($data) {
            $data->user_id = $user->id;
            $data->save();
        } else {


            $data = new \App\Models\Device;
            $data->device = $request->token;
            $data->user_id = $user->id;
            $data->device_type = 'web';
            $data->save();
        }
    }


})->name('user.update.token');


Route::get('newAccount',function (){
    return view('emails.newAccount');
});


Route::get('request_pass',function (){
    return   view('Site.testing');
});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
