<?php
namespace App\Traits;


use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

trait UUID
{
    protected static function bootUUID()
    {
        static::creating(function ($model) {

//            if (! $model->getKey()) {
//                $model->{$model->getKeyName()} = (string) Str::uuid();
//            }

            if (Schema::hasColumn('users', 'uuid'))
            {
                $model->uuid = (string) Str::uuid();
            }

//            if (Schema::hasColumn('users', 'api_token'))
//            {
//                $model->api_token = str_random(60);
//            }


        });
    }

//    public function getIncrementing()
//    {
//        return false;
//    }
//
//    public function getKeyType()
//    {
//        return 'string';
//    }


}