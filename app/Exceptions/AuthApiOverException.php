<?php

namespace App\Exceptions;

use Exception;

class AuthApiOverException extends Exception
{
    public function render($request)
    {
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }


}
