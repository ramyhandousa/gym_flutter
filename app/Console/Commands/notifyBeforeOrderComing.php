<?php

namespace App\Console\Commands;

use App\Libraries\PushNotification;
use App\Models\OrderOld;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;

class notifyBeforeOrderComing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifyOrder:notifyBeforeOrderComing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    public $push;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PushNotification $push)
    {
        parent::__construct();
        $this->push = $push;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = Setting::where('key','notify_reserva')->first();
        
        $orders = OrderOld::whereHas('place', function ($user){

            $user->whereDoesntHave('roles')->whereDoesntHave('abilities')
                ->where('defined_user','!=','other')
                ->where('is_active',1) ->where('is_suspend',0)
                ->where('is_accepted',1)
                ->whereHas('devices')
                ->whereHas('userSetting',function ($q){
                    $q->where('near_revers_date' , 'true');
                });

        })->whereDate('book_day' ,'>=',Carbon::today())
            ->whereStatus('pending')->select('id','user_id','place_id','book_day','come_time')
            ->with('place.devices')->get();

        $carbon  =  Carbon::now() ;
        $timeNow =  $carbon->toTimeString();
        $dataNow =  $carbon->toDateString();

        foreach ($orders as $order):

            if( $dataNow == $order->book_day &&  Carbon::createFromFormat('H:i:s', $order->come_time)->subMinute($setting?(int)$setting->body:30)->toTimeString() == $timeNow):

                foreach ($order['place']['devices'] as $device):

                    $devicesIos     =       $device['devices']->where('device_type','Ios')->pluck('device');
                    $devicesAndroid =  $device['devices']->where('device_type','android')->pluck('device');
                    $this->push->sendPushNotification($devicesAndroid, $devicesIos, 'الحجوزات',
                        ' إقتراب موعد الحجز لديك علي الطلب رقم ' . $order->id ,
                        [
                            //                        'type' => 7,
                            'orderId' => $order->id
                        ]
                    );
                endforeach;

            endif;

        endforeach;

        // \Log::info("This is some useful information. ");
    }
}
