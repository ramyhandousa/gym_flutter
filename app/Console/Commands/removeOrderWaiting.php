<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\OrderOld;
use Illuminate\Console\Command;

class removeOrderWaiting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'removeOrder:removeOrderWaiting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'user can make another order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->addMinutes(15)->toDateTimeString();

        $order = OrderOld::where('created_at' ,'<=',$date)->whereStatus(0)->delete();

        \Log::info("This is some useful information. $order");
    }
}
