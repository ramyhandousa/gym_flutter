<?php

namespace App;

use App\Models\City;
use App\Models\DayTranslation;
use App\Models\Device;
use App\Models\GymClass;
use App\Models\GymClassDay;
use App\Models\GymDay;
use App\Models\GymImage;
use App\Models\GymService;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Point;
use App\Models\Transaction;
use App\Traits\UUID;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable; 
use Illuminate\Foundation\Auth\User as Authenticatable; 
use Silber\Bouncer\Database\HasRolesAndAbilities;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable ,HasRolesAndAbilities ,UUID ;


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
        
    }

    protected $fillable = [
        'defined_user',
        'name',
        'phone',
        'email',
        'city_id',
        'password',
        'image',
        'latitute',
        'longitute',
        'address',
        'is_active',
        'is_accepted',
        'is_suspend',
        'message',
        'login_count',
    ];

    
    protected $hidden = [
        'password','remember_token'
    ];

    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }
            
        } else {
            redirect(route('admin.login'));
        }
    }
    
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }


    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }


    public function my_favorites()
    {

        return $this->belongsToMany(User::class, 'favorite_user', 'user_id', 'gym_id');

    }

    public function days()
    {
        return $this->hasMany(GymDay::class ,'gym_id');
    }

    public function gallery()
    {
        return $this->hasMany(GymImage::class ,'gym_id');
    }

    public function services()
    {
        return $this->hasMany(GymService::class ,'gym_id')->where('is_suspend',0);
    }


    public function sessions()
    {
        return $this->hasMany(GymClass::class ,'gym_id')->where('is_suspend',0);
    }


    public function sessions_day()
    {

        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();

        return $this->hasMany(GymClass::class ,'gym_id')->whereHas('daysClass',function ($q) use ($dayId){
            $q->where('day_id' , $dayId->day_id);
        });
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }


    public function orders_gym(){
        return $this->hasMany(Order::class,'gym_id');
    }


    public function points(){
        return $this->hasMany(Point::class);
    }

    public function points_not_confirm(){
        return $this->hasMany(Point::class)->where('type','not_confirm');
    }


    public function wallet(){
        return $this->hasMany(Transaction::class);
    }



}
