<?php


namespace App\Http\Helpers;
use App\User;
class Main
{


    function html_rate_icons($rate)
    {
        $html = '';

        $flooredRate = floor($rate);
        $hasHalfStar = $flooredRate < $rate;
        $emptyStars = 5 - round($rate);

        if ($rate > 0) {
            foreach (range(1, $flooredRate) as $star) {
                $html .= '<i class="fa fa-star text-warning" style="font-size: 25px; color: #f39c12 !important"></i>';
            }
            if ($hasHalfStar) {
                $html .= '<i class="ionicons ion-ios-star-half fa-flip-horizontal text-warning" style="font-size: 25px; color: #f39c12 !important"></i>';
            }
        }

        if ($emptyStars > 0) {
            foreach (range(1, $emptyStars) as $star) {
                $html .= '<i class="fa fa-star text-warning" style="font-size: 25px; color: #ebeff2  !important; "></i >';
            }
        }

        return $html;
    }


    function success_response($data, $msg, $additional)
    {
        return response()->json([
            'status' => true,
            'message' => $msg,
            'data' => $data,
            'additional' => $additional
        ], 200);
    }
    
    
    public function userType($id)
    {

        $user = User::find($id);


        if (!$user) {
            return $type = '--';
        }
        
        switch ($user->is_user) {
            case $user->is_user = 0 :
                $type = __('maincp.o_individuals');
                break;
            case $user->is_user = 1;
                $type = __('maincp.retails');
                break;

            case $user->is_user = 2;
                $type = __('maincp.wholesale');
                break;

            case $user->is_user = 3;
                $type =__('maincp.agencies');
                break;

            case $user->is_user = 4;
                $type = __('maincp.insurances');
                break;

            case $user->is_user = 5;
                $type = __('maincp.maintenance_centers');
                break;

            default:
                $type = __('maincp.visitor');
        }
        return $type;


    }




    function getOrderStatus()
    {
        return view('institution.general.orderStatus');
    }




}

