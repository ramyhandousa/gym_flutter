<?php

namespace App\Http\Middleware;

use App\Models\Order;
use App\User;
use Carbon\Carbon;
use Closure;

class scanOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $today = Carbon::now();
        $date = $today->format('Y-m-d');

        // Make Sure Gym Id Request
        $gym = User::whereId($request->gymId)->first();

            if (!$gym){  return $this->gymNotFound();  }

        // Check User Have Any Orders
            $userValidOrders = User::where( 'api_token' , request()->headers->get('apiToken') )->whereHas('orders');

                if (!$userValidOrders->first()){   return $this->checkUserHaveOrder();   }

        // Check User Have Order With This Gym
            $checkHaveOrderWithGym = $userValidOrders->whereHas('orders',function ($q) use ($gym){
//                        $q->whereGymId($gym->id)->whereStatus('pending');
                        $q->whereGymId($gym->id);
                    })->first();

                        if (!$checkHaveOrderWithGym){  return $this->checkOrderWithGym();  }

        // Check If User Have Order Today with This Gym
            $orderDay = $userValidOrders->whereHas('orders',function ($q) use ($gym , $date){
//                            $q->whereGymId($gym->id)->where('date','=',$date)->whereStatus('pending');
                            $q->whereGymId($gym->id)->where('date','=',$date);
                        })->first();

                            if (!$orderDay){ return $this->checkOrderPending();  }


        return $next($request);
    }



    private  function gymNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'من فضلك تاكد من تفحص الباركود للجيم'   ],200);
    }

    function checkUserHaveOrder(){
        return response()->json([   'status' => 400,  'error' => (array) 'نأسف لا يوجد اي حجوزات'   ],200);

    }

    function checkOrderWithGym(){
        return response()->json([   'status' => 400,  'error' => (array) 'نأسف لا يوجد لك حجز لدينا علي هذا الجيم'   ],200);

    }
    function checkOrderPending(){
        return response()->json([   'status' => 400,  'error' => (array) 'نأسف لا يوجد لك حجز لدينا اليوم'   ],200);

    }
}
