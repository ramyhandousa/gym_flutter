<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Countery;
use App\Models\Order;
use App\Models\OrderOld;
use App\User;
use Closure;

class validOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   public function handle($request, Closure $next)
    {
        if ($request->route()->uri ==  'api/orders' && $request->isMethod('post')){

            $gym = User::whereId($request->gymId)
                ->whereDoesntHave('roles')->whereDoesntHave('abilities')
                ->whereIsActive(1)->whereIsSuspend(0)
                ->where('defined_user','gym')->whereHas('days')->first();

            if (!$gym){
                return $this->GymForfav();
            }

        }else{

            $order = Order::whereId($request->orderId)->first();

            if ( ! $order ) {   return $this->OrderNotFound();  }

        }

        return $next($request);
    }


    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }

    private  function GymForfav(){
        return response()->json([   'status' => 400,  'error' => (array) 'لا يوجد جيم لإضافته للطلب لديك'   ],200);
    }
}
