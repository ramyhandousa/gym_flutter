<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthApiOverException;
use App\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;

class UserApiToken
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, ...$guards)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ( ! $user ) { return   $this->UserNotFound(); }

        return $next($request);
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

//    protected function authenticate($request, array $guards)
//    {
//
//        if (empty($guards)) {
//            return $this->auth->authenticate();
//        }
//
//        foreach ($guards as $guard) {
//            if ($this->auth->guard($guard)->check()) {
//                return $this->auth->shouldUse($guard);
//            }
//        }
//
//
//        throw new AuthApiOverException( $request );
//
//    }
       

       
     
}
