<?php

namespace App\Http\Middleware;

use App\Models\Meal;
use App\Models\Offer;
use Closure;

class checkOffer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->mealId){

            $meal = Meal::find($request->mealId);

            // If meal Not Found
            if (!$meal){  return $this->MealNotFound();  }

            // Check Price Offer Not Bigger Than Meal Price
            if($meal->price <= $request->price){ return $this->offerPriceError(); }

            // Check If Found Offer Before Or Not && Your Request store Not Edit Offer
            $checkOffer = Offer::whereMealId($request->mealId)->whereStatus(0)->first();

            if ($checkOffer && $request->route()->uri ==  'api/offers'){

                return response()->json( [
                    'status' => 400 ,
                    'error' =>  (array)  trans('global.offer_found_before'),
                ] , 200 );

            }
        }

        if ($request->offerId){

            $offer = Offer::find($request->offerId);

            if (!$offer){  return $this->offerNotFound();  }

        }


        return $next($request);
    }

    private  function MealNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذه الوجبة غير موجود '   ],200);
    }

    private  function offerPriceError(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.offer_price_error')   ],200);
    }

    private  function offerNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.offer_not_found')   ],200);
    }


}
