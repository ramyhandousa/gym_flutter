<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class checkUserById
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->id )->first();

        if ( ! $user ) { return   $this->UserNotFound(); }

        return $next($request);
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

}
