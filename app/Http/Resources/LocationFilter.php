<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , $this->latitute) ,
            'longitute' => $this->when($this->longitute , $this->longitute) ,
        ];
    }
}
