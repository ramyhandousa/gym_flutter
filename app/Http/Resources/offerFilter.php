<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\PlaceWorkingTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class offerFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();
        $place =  PlaceWorkingTime::wherePlaceId($this->user_id)->whereDayId($dayId ? $dayId->day_id : 1)->first();
        $user_place = User::whereId($this->user_id)->first();

        return [
            'id' => $this->id,
            'defined_offer' =>  $this->defined_offer ,
                $this->mergeWhen($this->meal,
                    [
                        'name'          => $this->meal['name'],
                        'desc'          => $this->meal['desc'],
                        'price'         => $this->meal['price'] ,
                        'price_offer'   => $this->price   ,
                        'images'        => $this->meal['myImages']   ,
                    ]),
            'period_start' => $this->period_start,
            'period_end' => $this->period_end,

            'startTime'     =>
                config('app.locale') == 'ar' ? $this->getArabicMonth($this->period_start) :
                    Carbon::parse($this->period_start)->toFormattedDateString()
            ,
            'endTime'     =>
                config('app.locale') == 'ar' ? $this->getArabicMonth($this->period_end) :
                    Carbon::parse($this->period_end)->toFormattedDateString()
            ,

            'images' => $this->when(!$this->meal && $this->images , $this->myImages) ,
            'placeId'=> $this->user_id,
            'name_place' => $user_place->fullName ,
            'logo_place' => $this->when($user_place->image , $user_place->image) ,
            'day_of_week' => $place ?
                                        $place->working == 'open'? $place->end :'off'
                                    : 'off',
//            'time_of_day' => $place->working == 'open'?  date('h:i a ', strtotime($place->end)) :'off',
        ];
    }


    private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        return $day . ' ' . $month ;
    }
}
