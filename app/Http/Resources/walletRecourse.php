<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class walletRecourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'progress' => $this->progress == 'deposit' ? true : false,
            'deposit' => $this->progress == 'deposit' ? 'شحن محفظة' : 'مدفوع',
            'value' => (int) $this->value,
            'time_from' => $this->when($this->created_at , date('h:i a ', strtotime($this->created_at))) ,
            'created_at' => $this->when($this->created_at , $this->created_at->toDateString()) ,
         ];
    }
}
