<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class placeDetailsRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'book_payment' => $this->when($this->book_payment , $this->book_payment),
            'book_price' => $this->when($this->book_price , $this->book_price),
            'classifications' => $this->when($this->classifications ,(int) $this->classifications),
        ];
    }
}
