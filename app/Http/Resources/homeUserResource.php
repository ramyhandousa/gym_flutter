<?php

namespace App\Http\Resources;

use App\Models\Offer;
use App\Models\OrderOld;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class homeUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    { 
       
        $order = $this->firstOrder($request);

        $dataOffer =  $this->maxOfferInApp();
        
        return [
            'near_order' =>   $this->when($order , new OrderRecource($order)) ,
             'max_offer' =>  $this->when($dataOffer , $dataOffer    ) ,

        ];
    }
    
    
    private function firstOrder($request){
        $today = date('Y-m-d');
        $time = Carbon::now()->format('H:i:s');

        $user = User::whereId($request->id)->whereHas('orders')->first();

        // First Check Order Belong To
        if ($user['defined_user'] == 'other'):
            $myOrder = OrderOld::whereUserId($request->id)->where('status','accepted');
        else:
            $myOrder = OrderOld::where('place_id',$request->id)->where('status','accepted');
        endif;

        if ($myOrder->exists()): // If Found Order Check Order In My day after Time Now Or get After This day

            $order = $myOrder->where('book_day',$today)->where('come_time','>',$time)
                            ->orWhere('book_day','>',$today)->where('status','accepted')
                            ->orderBy('book_day')->orderBy('come_time')->first();
        else:
            $order = [];
        endif;
        
         return $order;
             
       
    }


    private function maxOfferInApp(){

        $offers =  Offer::where('defined_offer','meal')->where('is_accepted',1)->whereDate('period_end','>',today())
                            ->whereHas('meal')->whereHas('user',function ($user){
                                $user->where('is_suspend', 0);
                            })->with('meal')->get();

        if (count($offers) > 0){

            $offerDiscount = $offers->where('discount', $offers->max('discount'))->first();
            $dataOffer =  new offerFilter($offerDiscount);

        }else{
            $dataOffer = [];
        }

        return $dataOffer;
    }
}
