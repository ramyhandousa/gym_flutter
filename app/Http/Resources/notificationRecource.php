<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class notificationRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->when($this->order_id , $this->order_id) ,
            'offer_id' => $this->when($this->offer_id , $this->offer_id) ,
            'title' => $this->when($this->title , $this->title) ,
            'body' => $this->when($this->body , $this->body) ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),

        ];
    }
}
