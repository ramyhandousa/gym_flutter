<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\GymClass;
use App\Models\GymClassDay;
use Illuminate\Http\Resources\Json\JsonResource;

class sessionGymRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();
        $sessionClassDay = GymClassDay::where('gym_classes_id',$this->id)->whereDayId($dayId->day_id)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'captain_name' => $this->captain_name,
            'description' => $this->description,
            'image' => \URL::to('/')  .$this->image,
            'time_session' => $sessionClassDay ? $sessionClassDay->start : " ",
            'duration_session' => $sessionClassDay ? $sessionClassDay->duration_session : " ",
        ];
    }
}
