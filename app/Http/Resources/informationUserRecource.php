<?php

namespace App\Http\Resources;

use App\Models\PlaceClassification;
use Illuminate\Http\Resources\Json\JsonResource;

class informationUserRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    { 
        if($this->placeInformation){
            $classification = PlaceClassification::whereId($this->placeInformation->classifications)->first();
        }
        
        return [
            'id' => $this->id,
            $this->mergeWhen($this->placeInformation ,
                [ 
                    
                    'book_payment' =>   $this->placeInformation ? $this->placeInformation->book_payment ?  $this->placeInformation->book_payment : ''  : ' ' ,
                    'book_price' =>   $this->placeInformation  ?   $this->placeInformation->book_price  ? $this->placeInformation->book_price : 0  : ' ',
                    'classifications' => $this->when( isset( $classification) ,    isset( $classification) ? $classification : 0    ) ,
            ]),
                
            'revers_update' => $this->when($this->userSetting , $this->userSetting ? filter_var($this->userSetting->revers_update, FILTER_VALIDATE_BOOLEAN) : null),
            'near_revers_date' => $this->when($this->userSetting  ,  $this->userSetting ? filter_var($this->userSetting->near_revers_date, FILTER_VALIDATE_BOOLEAN) : null),
            'notify_offer' => $this->when($this->userSetting  ,  $this->userSetting ?  filter_var($this->userSetting->notify_offer, FILTER_VALIDATE_BOOLEAN) : null),
        ];
    }
}
