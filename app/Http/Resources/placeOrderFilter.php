<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class placeOrderFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->defined_user,
            'name' => $this->fullName,
            'image' => $this->when($this->image , $this->image) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , (double) $this->latitute) ,
            'longitute' => $this->when($this->longitute ,(double)  $this->longitute) ,
            'gallery' => $this->myImages,
            'meals_count' => $this->meals_count  ,
            'offers_count' =>  $this->offers_count ,
            'keywords_count' => is_string( $this->keywords)  ?  count(explode(',',$this->keywords)): 0 ,
         ];
    }
}
