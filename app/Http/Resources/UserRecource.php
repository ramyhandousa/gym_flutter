<?php

namespace App\Http\Resources;

use App\Models\Keyword;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;

class UserRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'user' => $this->defined_user,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->when($this->email , $this->email) ,
            'api_token' => $this->when($this->api_token , $this->api_token) ,
            'image' => $this->when($this->image , \URL::to('/') .'/' .$this->image) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , (double) $this->latitute) ,
            'longitute' => $this->when($this->longitute ,(double)  $this->longitute) ,
            'is_active' => $this->is_active ? 1  == true : false,
            'is_suspend' => $this->is_suspend ? 1  == true : false,
            'city' =>  $this->when($this->city_id , new CityRecource($this->city)),

        ];
    }


}
