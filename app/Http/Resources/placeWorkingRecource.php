<?php

namespace App\Http\Resources;

use App\Models\Day;
use Illuminate\Http\Resources\Json\JsonResource;

class placeWorkingRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
             $this->mergeWhen( $this->working == 'open',  [  'start' =>  $this->start ]),
              $this->mergeWhen( $this->working == 'open',  [ 'end' =>   $this->end ]),
            'working' => $this->working,
            'day' => $this->when($this->day_id , new dayRecource(Day::whereId($this->day_id)->first())),
        ];
    }
}
