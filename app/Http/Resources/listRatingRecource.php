<?php

namespace App\Http\Resources;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class listRatingRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userName' => $this->when($this->user_id , User::whereId($this->user_id)->first()->fullname),
            'rate_value' => $this->when($this->rating , $this->rating),
            'comment' => $this->when($this->comment , $this->comment),
            'time' => $this->when($this->created_at , Carbon::parse($this->created_at)->diffForHumans()),
        ];
    }
}
