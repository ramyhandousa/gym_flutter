<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\PlaceWorkingTime;
use Illuminate\Http\Resources\Json\JsonResource;

class placeIdFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // Get The day Equal Today
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();
        // Get Place equal Today To Check place is Open Or Off
        $place =  PlaceWorkingTime::wherePlaceId($this->id)->whereDayId($dayId ? $dayId->day_id : 1)->first();
        return [
            'id' => $this->id,
            'type' => $this->defined_user,
            'name' => $this->fullName,
            'phone' => $this->phone,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute ,(double) $this->latitute) ,
            'longitute' => $this->when($this->longitute , (double) $this->longitute) ,
            'gallery' => $this->myImages,
            'day_of_week' => $place ?
                                $place->working == 'open'?  $place->end :'off'
                            :'off',
            'rate' => $this->ratings_count != 0 ? $this->ratings->avg('rating') : 0,
            'rate_from_user' => $this->ratings_count != 0 ? $this->ratings->count('rating') : 0,
            'image' => $this->when($this->image , $this->image) ,
            'information' =>  $this->when($this->placeInformation || $this->userSetting , new informationUserRecource($this)),
            'meals_count' => $this->meals_count  ,
            'offers_count' =>  $this->offers_count ,
            'keywords_count' => is_string( $this->keywords)  ?  count(explode(',',$this->keywords)): 0 ,
            'place_information' => $this->when($this->placeInformation , new placeDetailsRecource($this->placeInformation))  ,
        ];
    }
}
