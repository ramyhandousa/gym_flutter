<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\PlaceWorkingTime;
use Illuminate\Http\Resources\Json\JsonResource;

class listPlaces extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();

        $place =  PlaceWorkingTime::wherePlaceId($this->id)->whereDayId($dayId ? $dayId->day_id : 1)->first();

        return [

            'id' => $this->id,
            'type' => $this->defined_user,
            'name' => $this->fullName,
            'time_of_day' => $place ?
                                $place->working == 'open'?  date('h:i a ', strtotime($place->end)) :'off'
                            :'off',
            'rate' => $this->when(count($this->ratings) > 0  , $this->ratings->avg('rating')),
            'logo' => $this->when($this->image , $this->image) ,
            'gallery' => $this->myImages,
        ];
    }
}
