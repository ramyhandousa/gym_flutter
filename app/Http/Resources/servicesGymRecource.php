<?php

namespace App\Http\Resources;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class servicesGymRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $gymServices = Service::whereId( $this->service_id)->first();

        $data =  new servicesRecource($gymServices);

        return array( $data)[0];
    }
}
