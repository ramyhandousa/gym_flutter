<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\GymDay;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class GymRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $minutes_control = Setting::whereKey('minutes_control')->first();

        $user = User::whereApiToken(request()->headers->get('apiToken'))->first();

        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();
        $my_day =  GymDay::whereDayId($dayId ? $dayId->day_id : 1)->first();
        return [

            'id' => $this->id,
            'name' => $this->name,
            'image' =>  $this->when($this->image , \URL::to('/') .'/'. $this->image) ,
            'Link_Qr' =>  $request->root() .'/' . config('app.locale') .'/qr-code-g?id=' . $this->id ,
            'price' => $this->when($this->price , (int)$this->price) ,
            'minutes_control' => $this->when($minutes_control , $minutes_control ? (int) $minutes_control->body : 0) ,
            'description' => $this->when($this->description , $this->description) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute ,  $this->latitute) ,
            'longitute' => $this->when($this->longitute ,  $this->longitute) ,
            'day_of_week' => $my_day ?  $my_day->working == 'open'? $my_day->time_from :'off'  : 'off',
            $this->mergeWhen(request()->headers->get('apiToken'),
                [
                    'is_favourite' => $user ?  count($user->my_favorites->where('id',$this->id)->first()) > 0 ? true : false : false,
                ]),
            'gallery' => GymGallery::collection($this->whenLoaded('gallery')),
            'sessionClass' => sessionGymRecource::collection($this->whenLoaded('sessions_day')),
            'services' => servicesGymRecource::collection($this->whenLoaded('services')) ,
        ];
    }
}
