<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\GymDay;
use Illuminate\Http\Resources\Json\JsonResource;

class GymRecourceFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();
        $my_day =  GymDay::whereDayId($dayId ? $dayId->day_id : 1)->first();

        return [

            'id' => $this->id,
            'name' => $this->name,
            'image' =>  $this->when($this->image , \URL::to('/') .'/'. $this->image) ,
            'Link_Qr' =>  $request->root() .'/' . config('app.locale') .'/qr-code-g?id=' . $this->id ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute ,  $this->latitute) ,
            'longitute' => $this->when($this->longitute ,  $this->longitute) ,
            'price' => $this->when($this->price , (int)$this->price) ,
            'day_of_week' => $my_day ?  $my_day->working == 'open'? $my_day->time_from :'off'  : 'off',

        ];


    }
}
