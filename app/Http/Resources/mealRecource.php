<?php

namespace App\Http\Resources;

use App\Models\MealImage;
use Illuminate\Http\Resources\Json\JsonResource;

class mealRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->when($this->desc , $this->desc) ,
            'price' => (int)$this->price,
            'images' => $this->when($this->images , $this->myImages) ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'category' =>  $this->when($this->category_id , new categoryRecource($this->category)),
            'user' => new UserFilterRecource($this->whenLoaded('user')),

        ];

    }

}
