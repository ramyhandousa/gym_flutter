<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class userSettingRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            $this->mergeWhen( $this->revers_update == 'true',  [  'revers_update' =>  $this->revers_update ]),
            $this->mergeWhen( $this->near_revers_date == 'true',  [ 'near_revers_date' =>   $this->near_revers_date ]),
            $this->mergeWhen( $this->notify_offer == 'true',  [ 'notify_offer' =>   $this->notify_offer ]),
        ];
    }
}
