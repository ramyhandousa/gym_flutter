<?php

namespace App\Http\Resources;

use App\Models\DayTranslation;
use App\Models\PlaceWorkingTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class offerRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();
        $place =  PlaceWorkingTime::wherePlaceId($this->user_id)->whereDayId($dayId ? $dayId->day_id : 1)->first();
        $user_place = User::whereId($this->user_id)->first();
        
        if($this->period_start && $this->period_end){
                $period_start = Carbon::parse($this->period_start);
                $period_end = Carbon::parse($this->period_end);
                
                $diff = $period_end->diffInDays($period_start);
        }
       
        return [

            'id' => $this->id,
            'defined_offer' => $this->defined_offer,
            'price' => $this->when($this->price , (int)$this->price) ,
            'meal_id' => $this->when($this->meal_id , $this->meal_id) ,
            'period_start' => $this->period_start,
            'period_end' => $this->period_end,
            'images' => $this->when($this->images , $this->myImages) ,
            // 'remaining'     => (string) ($this->getReminaingMonth($this->period_end) -$this->getReminaingMonth($this->period_start)) ,
            'remaining'     => $this->when( isset($diff)  ,(string) $diff ) ,
            'startTime'     =>
                config('app.locale') == 'ar' ? $this->getArabicMonth($this->period_start) :
                    Carbon::parse($this->period_start)->toFormattedDateString()
            ,
            'endTime'     =>
                config('app.locale') == 'ar' ? $this->getArabicMonth($this->period_end) :
                    Carbon::parse($this->period_end)->toFormattedDateString()
            ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'placeId'=> $this->user_id,
            'name_place' => $user_place->fullName ,
            'logo_place' => $this->when($user_place->image , $user_place->image) ,
            'day_of_week' => $place ?
                                        $place->working == 'open'? $place->end :'off'
                                    : 'off',
            'meal' =>   $this->when($this->meal ,  new mealRecource($this->whenLoaded('meal')))  ,

        ];

    }

    private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
                    "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
                    "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        return $day . ' ' . $month ;
    }
    
    
    
    private  function getReminaingMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date("D", strtotime($data)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);


        $dayName = config('app.locale') == 'ar' ? $ar_day :date("D", strtotime($data));
        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $month = config('app.locale') == 'ar' ? $months[$month] : $month;

        // $data  = [
        //     'dayName' => $dayName,
        //     'day' => $day,
        //     'month' => $month,
        // ];

        return   $day ;
    }

}
