<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderFilterRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $gym = User::whereId($this->gym_id)->first();

        return [
            'id' => $this->id,
            'time_from' => $this->when($this->time_from ,  $this->time_from) ,
            'time_to' => $this->when($this->time_to ,  $this->time_to) ,
            'day_order' =>  $this->date   ,
            'status' => $this->when($this->status , $this->status) ,
            'gym' =>  $this->when($this->gym_id , new UserFilterRecource($gym)),
        ];
    }

    private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date("D", strtotime($data)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);


        $dayName = config('app.locale') == 'ar' ? $ar_day :date("D", strtotime($data));
//        $day = date("d", strtotime($data));
//        $month = date("M", strtotime($data));
//        $month = config('app.locale') == 'ar' ? $months[$month] : $month;
//        $year = date("Y", strtotime($data));
//
//        $data  = [
//            'dayName' => $dayName,
//            'day' => $day,
//            'month' => $month,
//            'year' => $year
//        ];

        return $dayName;
    }


}
