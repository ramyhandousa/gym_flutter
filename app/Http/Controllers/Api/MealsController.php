<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\listMealRecource;
use App\Http\Resources\mealFilterRecource;
use App\Http\Resources\mealRecource;
use App\Models\Category;
use App\Models\Image;
use App\Models\Meal;
use App\Models\MealImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MealsController extends Controller
{

    public $public_path;
    public function __construct(  )
    {
        $this->middleware('apiToken' , [ 'except' =>  ['show' ,'uploadImage'] ]);

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->public_path = 'files/meals/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $list = $user->categories;

        foreach ($list as $value){

            $value['meals'] = [];
            $value['meals'] = Meal::whereUserId($user->id)->where('category_id', $value->id)->get();

        }

        $data = listMealRecource::collection($list);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    public function my_meals(Request $request){

        $user = Auth::user();

        $meals = $user->load('meals');

        $data = mealFilterRecource::collection($meals['meals']);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $user = Auth::user();

        $exists =  $user->categories->where('id',$request->categoryId)->first();

        if (!$exists){     return $this->CategoryNotBelongToY();  }

       $meal = new Meal();
       $meal->user_id = $user->id;
       $meal->name = $request->name;
       $meal->desc = $request->description;
       $meal->price = $request->price;
       $meal->category_id = $request->categoryId;

        // ids from upload Images
        if ($request->images ){

            $myids =  implode(',',$request->images) ;
            $filterIds = explode(',',$myids);
            $meal->images = $filterIds;
        }

       $meal->save();

        if ($request->images ){

            Image::whereIn('id' , $filterIds)->update(['images_id' => $meal->id]);
        }

        $data  =  new mealRecource($meal);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meal = Meal::find($id);

        if (!$meal){  return $this->MealNotFound();  }

        $data  =  new mealRecource($meal);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function editMeal(Request $request){

        $user = Auth::user();

        $exists =  $user->categories->where('id',$request->categoryId)->first();

        if (!$exists){     return $this->CategoryNotBelongToY();  }

        $meal = Meal::find($request->mealId);

        if (!$meal){  return $this->MealNotFound();  }

        $meal->user_id = $user->id;
        $meal->name = $request->name;
        $meal->desc = $request->description;
        $meal->price = $request->price;
        $meal->category_id = $request->categoryId;

        // ids from upload Images
        if ($request->images ){
            $this->updateImageForMeal($request, $meal);
        }

        $meal->save();

        $data  =  new mealRecource($meal);

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.meal_edit'),
            'data' => $data ,
        ] , 200 );

    }


    private function updateImageForMeal($request,$meal){

        $myids =  implode(',',$request->images) ;

        $filterIds = explode(',',$myids);

        $meal->images = $filterIds;

        $images = Image::whereIn('id' , $filterIds);

        $images->update(['images_id' => $meal->id]);

        // This For check Count Of Image Before Delete
        // Check For Ids Not used Any More in Table Image
        if (count($images->get()) > 0 ){

            $dataRemove =  Image::where('images_id',$meal->id)->whereNotIn('id',$filterIds)->get();

            if (count($dataRemove) > 0 ){

                foreach ($dataRemove as $item){
                    $item->delete();
                }

            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $exists =  $user->meals->where('id',$id)->first();
        $offer =  $user->offers()->where('meal_id',$id)->get();

        if ($exists){
            
             if (count($offer) > 0){
                $offer->each->delete();
            }

            $exists->delete();
            $exists->deleteTranslations();
            // remove Image For Table Image
            $this->deleteImageForMeal($id);


            return response()->json( [
                'status' => 200 ,
                'message' => trans('global.meal_delete'),
            ] , 200 );

        }else{

            return $this->MealNotFound();
        }

    }

    private function deleteImageForMeal($id){

        $images = Image::where('images_id',$id)->get();

        if (count($images) > 0 ){

            foreach ($images as $item){
                $item->delete();
            }

        }
    }

    public function uploadImage(Request $request){

        $upload = new Image();
        $upload->images_type = 'App\Models\Meal';

        if ($request->type){

            $upload->type = $request->type;
        }

        $upload->url =  'public/' . $this->public_path . \UploadImage::uploadImage( $request , 'url' , $this->public_path );
        $upload->save();

        return response()->json( [
            'status' => 200 ,
            'data' => $upload ,
        ] , 200 );
    }



    private  function CategoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذا القسم غير موجود '   ],200);
    }

    private  function MealNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذه الوجبة غير موجود '   ],200);
    }


    private  function CategoryNotBelongToY(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.category_not_belong')   ],200);
    }

}
