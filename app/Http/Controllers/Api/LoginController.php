<?php

namespace App\Http\Controllers\api;
use App\Http\Resources\UserRecource;
use App\Models\Device;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Validator;
class LoginController extends Controller
{
       
       public $headerApiToken;
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
       
       }
       
    public function login(Request $request){

      if ($user =  Auth::attempt([  'phone' => $request->phone, 'password' => $request->password  ])  ) {

          $user =  Auth::user();

          $reason = auth()->user()->message;

          if ($user->is_suspend == 1) {
              return response()->json( [
                  'status' => 400 ,
                  'error' =>  (array) __('trans.suspendBecause', ['phone' => Setting::getBody('phone_contact'), "reason" => $reason])
              ] , 200 );
          }

          $data = new UserRecource($user);

          $this->manageDevices($request, $user);

          return response()->json([
              'status' => 200,
              'message' =>     trans('global.logged_in_successfully'),
              'data' => $data
          ]);

      }else{

          return response()->json([
              'status' => 401,
              'error' => (array)   trans('global.username_password_notcorrect') ,
          ],200);

      }
  }
    
    public function logOut(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $this->deleteDevice($request,$user);
    
        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.logged_out_successfully') ,
        ] , 200 );
    
    }
    
    private function manageDevices($request, $user)
    {
        if ($request->deviceToken) {
            
            $data = Device::where('device', $request->deviceToken)->first();
            if ($data) {
                $data->user_id = $user->id;
                $data->save();
            } else {
                $data = new Device();
                $data->device = $request->deviceToken ;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }
            
        }
    }
    
    private function deleteDevice($request ,$userUpdateApi) {
        $device = Device::where('device', $request->deviceToken)->first();
        if ($device){
            $device->delete();
        }
    }
    
    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

}
