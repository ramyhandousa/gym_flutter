<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\homeUserResource;
use App\Http\Resources\listMealRecource;
use App\Http\Resources\listPlaces;
use App\Http\Resources\listRatingRecource;
use App\Http\Resources\LocationFilter;
use App\Http\Resources\mealFilterRecource;
use App\Http\Resources\offerFilter;
use App\Http\Resources\offerRecource;
use App\Http\Resources\OrderRecource;
use App\Http\Resources\placeIdFilter;
use App\Http\Resources\placeWorkingRecource;
use App\Models\City;
use App\Models\Day;
use App\Models\DayTranslation;
use App\Models\Keyword;
use App\Models\Meal;
use App\Models\Offer;
use App\Models\OrderOld;
use App\Models\PlaceClassification;
use App\Models\PlaceWorkingTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ListController extends Controller
{
    public function __construct(  )
    {

        $this->middleware('apiToken')->only(['listDays']);
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


    }

    public function indexUser(Request $request){

        $data = new homeUserResource($request);
         
        if( count($data->resolve()) == 0 ){
            
             return response()->json( [
                'status' => 200 ,
                'data' => new \stdClass() ,
            ] , 200 );
           
        }else{
            
              return response()->json( [
                'status' => 200 ,
                'data' => $data ,
            ] , 200 );
        }
    }



    public function listCity(Request $request , City $countery ){

        $cities =  $countery->newQuery();

        if ($request->has('subCity')) {

            $data  =   $countery->whereParentId($request->subCity)
                                ->where('is_suspend',0)->select('id')->get();
 
            return response()->json( [
                'status' => 200 ,
                'data' => $data ,
            ] , 200 );

        }


        $data = $cities->whereParentId(null)->where('is_suspend',0)->select('id')->get(); 

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );  

    }

    public function listDays( ){

        $places = PlaceWorkingTime::where('place_id' , Auth::id())->get();

        $data = placeWorkingRecource::collection($places);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    public function listKeywords( ){

        $data = Keyword::select('id')->get();

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

     public function classifications( Request $request){
        if($request->type){
            
            $data =   PlaceClassification::where('defined_user',$request->type)->get();
            
        }else{
            $data = PlaceClassification::all();
        } 
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }


    public function listPlaces(Request $request){

        $query = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
                        ->whereIsActive(1)
                        ->where('is_accepted',1)
                        ->whereIsSuspend(0)
                        ->where('defined_user','!=','other');

        $this->filterSearchForPlace($request , $query);

        $places = $query->with('ratings')->get();

        $data = listPlaces::collection($places);
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    public function getPlaceById(Request $request){
        $place = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
                        ->whereIsActive(1)
                        ->where('is_accepted',1)
                        ->whereIsSuspend(0)
                        ->where('defined_user','!=','other')
                        ->whereId($request->id)->with('placeInformation')->withCount('meals','offers','ratings')->first();

        if (!$place){  return $this->placeNotFound(); }

        $data = new placeIdFilter($place);
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function getPlaceDetails(Request $request){
        $place = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
                        ->whereIsActive(1)
                        ->where('is_accepted',1)
                        ->whereIsSuspend(0)
                        ->where('defined_user','!=','other')
                        ->whereId($request->id)->with('meals','ratings','offers.meal')->first();

        if (!$place){  return $this->placeNotFound(); }
        
        

        $data = $this->getInformationPlaces($request->info,$place->id,$place );

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function listOffers(Request $request){
    

        $query = Offer::whereHas('user',function($q){
            
            $q->where('is_suspend' , 0);
            
        })->where('status' , 0)->where('is_accepted' , 1)->whereDate('period_end','>',today())->with('meal','user.placeInformation');
        
        $this->filterSearchForOffer($request , $query);
        $offers = $query->latest()->get();

        $data =  offerFilter::collection($offers);
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function getOfferById(Request $request){

        $offer = Offer::whereId($request->id)->where('status' , 0)->where('is_accepted' , 1)->whereDate('period_end','>',today())->with('meal')->first();

        if (!$offer){  return $this->offerNotFound(); }

        $data =  new offerFilter($offer);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function getInformationPlaces($info ,$id , $place  )
    {
        $status = $info;
        switch ($status) {
            case 'days':
                return      placeWorkingRecource::collection(PlaceWorkingTime::where('place_id' , $id)->get());
                break;
            case 'location':
                return      new LocationFilter($place);
                break;
            case 'meals':
                return      (int)\request('filter') ? listMealRecource::collection($place['categories']->where('id',\request('filter'))->values()):listMealRecource::collection($place['categories']);
                break;
            case 'offer':
                return      \request('filter') ? offerFilter::collection($place['offers']->where('defined_offer',\request('filter'))->values()) :offerFilter::collection($place['offers']);
                break;
            case 'rating':
                return      listRatingRecource::collection($place['ratings']);
                break;
            default:
                return      $this->getKeywords($place['keywords']);
        }
    }



    public function listRating(Request $request){

        $rating = DB::table('ratings')->where('rateable_id',$request->placeId)->latest()->get();

        $data = listRatingRecource::collection($rating);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    private function getKeywords($ids){

        if(@count($ids) > 0 && is_string($ids)){
            $filterIds = explode(',',$ids);
            $keyword = Keyword::whereIn('id',$filterIds)->select('id')->get();

        }else{
            $keyword = [];
        }
        return $keyword;

    }



    private function filterSearchForOffer($request ,$query ){
        if ($request->type ){ // Cafe Or Resturant
            $query->whereHas('user',function ($q) use ($request){
                $q->where('defined_user',$request->type);
            });
        }

        if ($request->cityId ){ // filter user By City
            $query->whereHas('user',function ($q) use ($request){
                $q->where('city_id',$request->cityId);
            });
        }

        if ($request->classifications ){ // filter (classifications) user have

            $query->whereHas('user',function ($q) use ($request){
                $q->whereHas('placeInformation',function ($q) use ($request){
                    $q->where('classifications', $request->classifications);
                });
            });
        }
    }


    private function filterSearchForPlace($request ,$query ){
        if ($request->type ){ // Cafe Or Resturant
            $query->where('defined_user',$request->type);
        }

        if ($request->cityId ){ // filter user By City
            $query->where('city_id',$request->cityId);
        }

        if ($request->classifications ){ // filter (classifications) user have
                $query->whereHas('placeInformation',function ($q) use ($request){
                    $q->where('classifications', $request->classifications);
                });
        }
    }



    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function offerNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.offer_not_found')   ],200);
    }

    private  function placeNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا المكان غير موجود لدينا'   ],200);
    }


}
