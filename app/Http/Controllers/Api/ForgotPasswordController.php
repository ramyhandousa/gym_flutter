<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
//use App\Transformers\Json;
use App\Http\Resources\UserRecource;
use App\Models\VerifyUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Helpers\Sms;
use Illuminate\Support\Facades\App;

use Validator;

class ForgotPasswordController extends Controller
{
       public $headerApiToken;
       
    public function __construct(Request $request)
    {

        $language = $request->headers->get('lang')  ? $request->headers->get('lang') : 'ar' ;
        App::setLocale($language);
       
	 // api token from header
	 $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

    }


    public function forgetPassword(Request $request)
    {

        $user = User::wherePhone($request->phone)->first();

        if ( ! $user ) {  return $this->UserNotFound();  }

        $action_code = substr(rand(), 0, 4);
        //	 	Sms::sendMessage('Activation code:' . $action_code, $request->phone);
        $foundPhone = VerifyUser::whereUserId($user->id)->wherePhone($request->phone)->first();

        if ($foundPhone){

            $foundPhone->update(['action_code' => $action_code ]);
        }else{

            $this->createVerfiy($request , $user , $action_code);

        }
        
        
		 $data = ['code' => $action_code];

        return response()->json([
            'status' => 200,
            'message' => __('global.activation_code_sent'),
            'data' => $data
        ]);
    }

    private function createVerfiy($request , $user , $action_code){
        $verifyPhone                = new VerifyUser();
        $verifyPhone->user_id       =   $user->id;
        $verifyPhone->phone         =   $request->phone;
        $verifyPhone->action_code   =   $action_code;
        $verifyPhone->save();
    }

    public function resetPassword(Request  $request) {

        $user = User::wherePhone($request->phone)->with('city')->first();

        if (!$user){  return $this->UserPhoneNotFound(); }

        if ($request->password){

            $user->update(['password' => $request->password]);
            $data = new UserRecource($user);

            return response()->json( [
                'status' => 200 ,
                'message' =>   trans('global.password_was_edited_successfully'),
                'data' => $data,
            ] , 200 );

        }else{
            $data = new UserRecource($user);
            return response()->json( [
                'status' => 200 ,
                'message' =>   trans('global.password_not_edited'),
                'data' => $data,
            ] , 200 );
        }
    }
       
    
       
       private  function UserNotFound(){
	    return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
       }
       private  function UserPhoneNotFound(){
	    return response()->json([   'status' => 400,  'error' => (array) trans('global.phone_number_incorrect')   ],200);
       }

}
