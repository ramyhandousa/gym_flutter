<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\GymRecource;
use App\Http\Resources\listPlaces;
use App\Models\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class SearchController extends Controller
{
    public function filterData(Request $request){

        $query = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
            ->whereIsActive(1)
            ->whereIsSuspend(0)
            ->where('defined_user','gym');

        if ($request->cityId){

            $this->filterCity($request,$query);
        }

        $usersCount = $query->get()->count();

        if($request->gallery ) {

            $query->with('gallery');
        }
        if ($request->sessions_day){

            $query->with('sessions_day');
        }

        if ($request->services){

            $query->with('services');
        }

        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1

        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        $users = $query->get();

        $data = GymRecource::collection($users);
        return response()->json( [
            'status' => 200 ,
            'totalCount' => $usersCount,
            'data' => $data ,
        ] , 200 );

    }


    private function filterCity($request ,$query){

        $city = City::whereId($request->cityId)->whereHas('children')->with('children')->first();

        if ($city){

            $query->whereIn('city_id', $city['children']->pluck('id'));

        }else{
            $query->where('city_id', $request->cityId);

        }
    }


}
