<?php

namespace App\Http\Controllers\api;

use App\Http\Helpers\Sms;
use App\Http\Requests\api\resgister;
use App\Models\City;
use App\Models\Day;
use App\Models\PlaceWorkingTime;
use App\Models\Countery;
use App\Models\Device;
use App\Models\Profile;
use App\Models\VerifyUser;
use App\User;
use App\Models\UserSetting ;
use App\Models\PlaceGeneralInfo ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class RegisterController extends Controller
{
       public $public_path;
       public $defaultImage; 
       
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
     
	    $this->public_path = 'files/users/profiles/';
	    
	    // default Image upload in profile user
	    $this->defaultImage = \request()->root() . '/' . 'public/assets/admin/images/defualt_User.png';
	     
	    
       }
       
    public function register(resgister $request){

	    $action_code = substr( rand(), 0, 4);

         $city  = City::whereNull('parent_id')->where('is_suspend', 0)->first();

          if (!$city){  return $this->CityNotFound();  }

        $user = new User();
        $user->defined_user = 'user';
        $user->name = $request->name;
        $user->phone =$request->phone;
        $user->email =$request->email;
        $user->city_id = $request->city_id;
        $user->password = $request->password;
        $user->api_token = str_random(60);
        $user->is_active = 0;
//        $user->image = $request->image ?  'public/' . $this->public_path .\UploadImage::uploadImage( $request , 'image' , $this->public_path ): '';
        $user->save();

        $this->createVerfiy($request , $user , $action_code);
        $this->manageDevices($request, $user);

//	    Sms::sendMessage('Activation code:' . $action_code, $request->phone);
        $data = ['code' => $action_code];
        return response()->json( [
            'status' => 200 ,
            'data' => $data,
            'message' =>trans('global.activation_code_sent')
        ] , 200 );
    }



   private function createVerfiy($request , $user , $action_code){
       $verifyPhone = new VerifyUser();
       $verifyPhone->user_id =  $user->id;
       $verifyPhone->phone =    $request->phone;
       $verifyPhone->action_code = $action_code;
       $verifyPhone->save();
   }


    private function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            $data = Device::where('device', $request->deviceToken)->first();
            if ($data) {
                $data->user_id = $user->id;
                $data->save();
            } else {
                $data = new Device();
                $data->device = $request->deviceToken ;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }

        }
    }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }
  
}
