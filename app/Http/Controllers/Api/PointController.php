<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\walletRecourse;
use App\Models\Point;
use App\Models\Setting;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
    public $headerApiToken;

    public function __construct( )
    {
        $this->middleware(['apiToken']);

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

    }

    public function allPoints(){

        $user = User::where( 'api_token' , $this->headerApiToken )->whereHas('points_not_confirm')->with('points_not_confirm')->first();

        $points = $user['points_not_confirm'] ? $user['points_not_confirm']->sum('value') : 0;

        $data = [   'user_points' => $points  ,
                    'change_point_system' => $this->systemSetting(1),
                    'change_money_point_system' => $this->systemSetting(2) ,
                    'min_point_system' => $this->systemSetting()
        ];
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );

    }

    public function wallet(Request $request){
        $user = User::where( 'api_token' , $this->headerApiToken )->whereHas('wallet')->with('wallet')->first();

        $totalWallet  = $user['wallet'] ? $user['wallet']->where('progress','deposit')->sum('value') : 0;

        if ($user['wallet']){

            $query = Transaction::whereUserId($user->id);
            $tranctionTotal = $query->count();

            if($request->pageSize):
                $pageSize = $request->pageSize;
            else:
                $pageSize = 10;
            endif;

            $skipCount = $request->skipCount;
            $currentPage = $request->get('take', 1); // Default to 1

            $query->skip($skipCount + (($currentPage - 1) * $pageSize));
            $query->take($pageSize);
            $wallet = $query->get();

            $details =  walletRecourse::collection($wallet);

        }else{
            $tranctionTotal = 0;
            $details =  [];
        }

        $data = ['totalCount' => $tranctionTotal , 'totalWallet' => $totalWallet ,'details' => $details];
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }

    public function confirmPoints(){

        // point system to get salary for my Points
        $pointSystem =  $this->systemSetting(3);

        $user = User::where( 'api_token' , $this->headerApiToken )->with('points_not_confirm')->first();

        $points = $user['points_not_confirm'] ? $user['points_not_confirm']->sum('value') : 0;

        $min_point_system = $this->systemSetting();

        if ($min_point_system  > $points){
                return $this->pointsMinSystem();
        }

        $salary = $points / $pointSystem;

        $transactions = new Transaction();
        $transactions->user_id =$user->id;
        $transactions->progress = 'deposit';
        $transactions->type = 'points';
        $transactions->value = number_format($salary, 2, '.', ' ');
        $transactions->save();

        $user['points_not_confirm']->each->update(['type' => 'confirm']);

        $points =  count($user->load('points_not_confirm')['points_not_confirm']) > 0 ? : 0 ;

        $data = [   'user_points' => $points  ,
                    'change_point_system' => $this->systemSetting(1),
                    'change_money_point_system' => $this->systemSetting(2) ,
                    'min_point_system' => $this->systemSetting()
        ];
        return response()->json( [
            'status' => 200 ,
            'message' => 'تم تحويل النقاط بنجاح',
            'data' => $data
        ] , 200 );

    }

    function systemSetting($system = 0){
        $system_setting = Setting::whereIn('key',['point_system','money_point_system','min_point_system','change_point_system','change_money_point_system'])->get();


        if ($system == 1){
            $point_system = $system_setting->where('key','change_point_system')->first();
            $data =  $point_system   ?  (int) $point_system->body         : 0;

        }elseif($system == 2){
            $money_point_system = $system_setting->where('key','change_money_point_system')->first();

            $data =  $money_point_system   ?  (int) $money_point_system->body         : 0;

        }elseif($system == 3){
            $change_point_system = $system_setting->where('key','change_point_system')->first()->body;
            $change_money_point_system = $system_setting->where('key','change_money_point_system')->first()->body;

            $data =  $change_point_system   /  $change_money_point_system;

        }else
        {
            $min_point_system = $system_setting->where('key','min_point_system')->first();
            $data =  $min_point_system   ?  (int) $min_point_system->body         : 0;

        }
        return $data;

    }




    private  function pointsMinSystem(){
        return response()->json([   'status' => 400,  'error' => (array) 'انت لم تصل إالي الحد الأدني للنقاط '],200);
    }



    private  function pointsSystemLarge(){
        return response()->json([   'status' => 400,  'error' => (array) 'نقاط النظام اعلي من نقاطك للتحويل '],200);
    }






}
