<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\dayWorking;
use App\Http\Resources\listRatingRecource;
use App\Http\Resources\placeDetailsRecource;
use App\Http\Resources\placeWorkingRecource;
use App\Http\Resources\UserRecource;
use App\Http\Resources\userSettingRecource;
use App\Models\Day;
use App\Models\Image;
use App\Models\PlaceGeneralInfo;
use App\Models\PlaceWorkingTime;
use App\Models\UserSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SettingUserController extends Controller
{

    public $public_path;

    public function __construct( )
    {
        $this->middleware('apiToken');

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->public_path = 'files/places/';

    }

    public function updateFrontPlaces(Request $request){
        
    //   return $request->all();
        $user = Auth::user();
        
        $user->first_name = $request->name ?: $user->first_name;
        
        if ($request->hasFile('logo')):
            
                $user->image = 'public/' . $this->public_path .\UploadImage::uploadImage( $request , 'logo' , $this->public_path );
        endif;
            
       
        if ($request->keywords){
            $filter =  explode(",",$request->keywords);
            $filterKeywords = implode(",", $filter);
            $user->keywords = $filterKeywords;
        }

        if ($request->images ){
            $this->updateImageForPlace($request, $user);
        }

        $user->save();

        $data = new UserRecource($user);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }


    public function dayPlaceWorking(dayWorking $request){


        $user = Auth::user();

        $data = $request->all();

        $day = Day::find($data['dayId']);

       if (!$day){  return $this->dayNotFound();   }

        $update =  PlaceWorkingTime::createOrUpdatePlace($user, $request);

       $places = PlaceWorkingTime::where('place_id' , $user->id)->get();

       $data = placeWorkingRecource::collection($places);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    public function updateDetails(Request $request){
        $user = Auth::user();
         

        $update = PlaceGeneralInfo::createOrUpdatePlace($user,$request);
         

        // $data = new placeDetailsRecource($update);
        $data = new UserRecource($user);
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function settingNotify(Request $request){

        $user = Auth::user();

        $update = UserSetting::createOrUpdateSetting($user,$request);

        $data = new userSettingRecource($update);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function listRating(Request $request){

        $user = Auth::user();

        $list =  $user->load('ratings');

        $data = listRatingRecource::collection($list['ratings']);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function uploadImage(Request $request){

        $upload = new Image();
        $upload->images_type = 'App\User';

        if ($request->type){

            $upload->type = $request->type;
        }

        $upload->url =  'public/' . $this->public_path . \UploadImage::uploadImage( $request , 'url' , $this->public_path );
        $upload->save();

        return response()->json( [
            'status' => 200 ,
            'data' => $upload ,
        ] , 200 );
    }

    private function updateImageForPlace($request,$user){

        $myids =  implode(',',$request->images) ;

        $filterIds = explode(',',$myids);


        $images = Image::whereIn('id' , $filterIds);

        $images->update(['images_id' => $user->id]);

        // This For check Count Of Image Before Delete
        // Check For Ids Not used Any More in Table Image
        if (count($images->get()) > 0 ){

            $dataRemove =  Image::where('images_id',$user->id)->whereNotIn('id',$filterIds)->get();

            if (count($dataRemove) > 0 ){

                foreach ($dataRemove as $item){
                    $item->delete();
                }

            }

        }
    }



    private  function dayNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)' اليوم غير موجود '   ],200);
    }
}
