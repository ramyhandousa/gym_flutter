<?php

namespace App\Http\Controllers\Api;

use App\Libraries\InsertNotification; 
use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Device; 
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypesSupport;
use App\Models\TypesSupportTranslation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use UploadImage;
use App\Libraries\PushNotification;

class SettingController extends Controller
{
    public $public_path;
    public $headerApiToken;
    public $language;
    public  $notify;
    public $push;
    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $language = request()->headers->get('Accept-Language') ? : 'ar';
        app()->setLocale($language);
        $this->language = $language;

        $this->public_path = 'files/transfers';

        $this->middleware('apiToken')->only(['contactUs']);

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
        $this->notify = $notification;
        $this->push = $push;

    }

    public function testNotify(){
        $devicesAndroid = Device::where('user_id', 93)->pluck('device');

        return   $this->push->sendPushNotification($devicesAndroid, null, 'الحجوزات',
            '2019' );
    }

    public function socialMedia(){
        $setting = Setting::all();
        $phone_contact =  $setting->where('key','phone_contact')->first();
        $faceBook =  $setting->where('key','faceBook')->first();
        $twitter =  $setting->where('key','twitter')->first();
        $snapChat =  $setting->where('key','snapChat')->first();
        $instagram =  $setting->where('key','instagram')->first();
        $telegram =  $setting->where('key','telegram')->first();
        $whatsapp =  $setting->where('key','whatsapp')->first();

        $data = [
            'phone_contact' =>  $phone_contact ? $phone_contact->body : '',
            'faceBook' => $faceBook ? $faceBook->body : '',
            'twitter' => $twitter ? $twitter->body : '',
            'snapChat' => $snapChat ? $snapChat->body : '',
            'instagram' => $instagram ? $instagram->body : '',
            'telegram' => $telegram ? $telegram->body : '',
            'whatsapp' => $whatsapp ? $whatsapp->body : '',
        ];

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    public function aboutUs(){

        $about_us =  Setting::where('key','about_us_' . $this->language)->first();

        return response()->json( [
            'status' => 200 ,
            'data' => $about_us ? $about_us->body : ''  ,
        ] , 200 );

    }

    public function terms_user(){
        $setting = Setting::whereIn('key',['terms_user_' . $this->language ,'key','terms_user1_'. $this->language])
            ->select('body')
            ->get()->pluck('body');

        return response()->json( [
            'status' => 200 ,
            'data' => $setting ,
        ] , 200 );

    }

    public function contactUs(Request $request){
        // check for user
        $user = User::where( 'api_token' , $this->headerApiToken )->first();;
        $type = TypesSupport::whereId($request->typeId)->first();
        if (!$type){  return $this->typeSupportNotFound();  }
        $support = new Support();
        $support->user_id = $user->id;
        $support->type_id = (int) $request->typeId;
        $support->message = $request->message;
        $support->save();
        //	$this->notify->NotificationDbType(2,null,$user->id,$request->message);
        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.message_was_sent_successfully'),
        ] , 200 );
    }


    public function getTypesSupport(){

        $types = TypesSupport::select('id')->get();

        return response()->json( [
            'status' => 200 ,
            'data' => $types ,
        ] , 200 );
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function typeSupportNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'برجاء اختيار قسم التواصل'   ],200);
    }

       
}
