<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\GymRecource;
use App\Http\Resources\GymRecourceFilter;
use App\Http\Resources\sessionGymRecource;
use App\Models\DayTranslation;
use App\Models\GymClass;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GymController extends Controller
{


    public function __construct(  )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->middleware('checkOffer')->only(['checkUserById']);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->pageSize):
            $pageSize = $request->pageSize;
        else:
            $pageSize = 10;
        endif;

        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $gyms = User::where('defined_user','gym')->where('is_active',1)->whereIsSuspend(0)->whereHas('days');

        $gyms->skip($skipCount + (($currentPage - 1) * $pageSize));
        $gyms->take($pageSize);

        $data = GymRecourceFilter::collection($gyms->latest()->get());
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user = User::whereId($id)->whereDoesntHave('roles')->whereDoesntHave('abilities')
        ->whereIsActive(1)->whereIsSuspend(0)
        ->where('defined_user','gym');
//        ->with(['gallery','sessions_day','services'])
//        ->first();
        if($request->gallery ) {

               $user->with('gallery');
        }
        if ($request->sessions_day){

              $user->with('sessions_day');
        }

        if ($request->services){

             $user->with('services');
        }

        if ( ! $user->first() ) { return   $this->UserNotFound(); }

        $data =  new GymRecource($user->first());
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sessions_day(Request $request){

        $session = GymClass::whereId($request->id)->whereHas('daysClassToday')->first();

        if (!$session){   return $this->sessionClassNotFound(); }

        $data =  new sessionGymRecource($session);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function sessionClassNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الكلاس غير موجود او غير متاح اليوم'   ],200);
    }
}
