<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\editUser;
use App\Http\Resources\UserFilterRecource;
use App\Http\Resources\UserRecource;
use App\Http\Resources\notificationRecource;
use App\Models\City;
use App\Models\Notification;
use App\Models\Keyword;
use App\Models\VerifyUser;
use App\User;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\api\rate;
use willvincent\Rateable\Rating;

use Carbon\Carbon;

class UserController extends Controller
{

    public $public_path;
    public $headerApiToken;
    public function __construct( )
    {
        $this->middleware('apiToken', [ 'except' =>  ['index','show']  ]);

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->public_path = 'files/users/profiles';

        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->type ?$request->type : 'resturant';

        $users = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
            ->whereIsActive(1)
            ->where('is_accepted',1)
            ->whereIsSuspend(0)
            ->where('defined_user',$type)
            ->get();

        $data = UserRecource::collection($users);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user  = User::whereId($id)
            ->whereDoesntHave('roles')
            ->whereDoesntHave('abilities')
            ->whereIsActive(1)
            ->where('is_accepted',1)
            ->whereIsSuspend(0)
            ->first();

        if (!$user){  return $this->UserNotFound();  }

        $data = new UserRecource($user);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {




    }


    public function editProfile(editUser $request){

        $data = $request->all();

        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ($request->city_id){

            $city  = City::where('is_suspend', 0)->whereId($data['city_id'])->first();

            if (!$city){  return $this->CityNotFound();  }

        }

        $action_code = substr(rand(), 0, 4);

        if ($request->phone){

            $checkPhoneChange = $user->wherePhone($request->phone)->count();

            if ($checkPhoneChange == 0){

                $foundPhone = VerifyUser::whereUserId($user->id)->first();

                if ($foundPhone){

                    $foundPhone->update(['phone' =>$request->phone , 'action_code' => $action_code ]);

                }else{

                    $verify = new  VerifyUser();
                    $verify->user_id= $user->id;
                    $verify->phone = $request->phone;
                    $verify->action_code = $action_code;
                    $verify->save();

                }

                // $user->update(['is_active' => 0  ]);
            }
        }
        $filterData =  collect($data)->except('phone');

        $user->fill($filterData->toArray());

        if ($request->image){
            $user->image =   'public/' . $this->public_path .\UploadImage::uploadImage( $request , 'image' , $this->public_path );
        }

        $user->save();

        $data = new UserRecource($user);

        return response()->json( [
            'status' => 200 ,
            'code' => $request->phone&&$checkPhoneChange == 0 ?  (string)$action_code : '',
            'message' => 'تم تعديل البيانات بنجاح',
            'data' => $data ,
        ] , 200 );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    public function ratePlace(rate $request){

        $user = Auth::user();

        $place = User::whereId($request->placeId)->first();

        if ( ! $place ) {   return $this->placeNotFound();  }

        $rating = new Rating();

        $userRatingBefore = $rating->where('rateable_id', $place->id)->where('user_id', $user->id)->first();

        if ($userRatingBefore) {

            $rating = Rating::whereId($userRatingBefore->id)->first();
            $rating->user_id = $user->id;
            $rating->rating = $request->rateValue;
            $rating->comment = $request->comment;
            $place->ratings()->save($rating);

        }else{
            $rating->user_id = $user->id;
            $rating->rating = $request->rateValue;
            $rating->comment = $request->comment;
            $place->ratings()->save($rating);
        }

        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.rate_success'),
        ] , 200 );
    }

       public function listNotification(Request $request)
    {

        $notification = Notification::where('user_id',Auth::id())->orWhere('sender_id',Auth::id())->latest()->get();
        
        $data = notificationRecource::collection($notification);

        return response()->json( [
            'status' => 200 ,  
            'data' => $data ,
        ] , 200 ); 
    }

    public function listFavourite(Request $request){
        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( !$user ) {  return    $this->UserNotFound();}

        $data = UserFilterRecource::collection( $user->my_favorites);

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);

    }
    public  function makeFavourite(Request $request) {

        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( !$user ) {  return    $this->UserNotFound();}

        $gym = User::whereId($request->gymId)
            ->whereDoesntHave('roles')->whereDoesntHave('abilities')
            ->whereIsActive(1)->whereIsSuspend(0)
            ->where('defined_user','gym')->first();

        if (!$gym){
            return $this->GymForfav();
        }

        if(!$user->my_favorites()->where('gym_id', $request->gymId)->first()){

            $user->my_favorites()->attach($request->gymId);

            return response()->json([
                'status' => 200,
                'message' => 'تم اضافته ضمن مفضلتي'
            ], 200);

        }else{

            $user->my_favorites()->detach($request->gymId);

            return response()->json([
                'status' => 201,
                'message' => 'تم  حذفه من ضمن مفضلتي'
            ], 200);
        }


    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function GymNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) 'لا يوجد جيم'   ],200);
    }
    private  function GymForfav(){
        return response()->json([   'status' => 400,  'error' => (array) 'لا يوجد جيم لإضافته للمفضلة لديك'   ],200);
    }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }

    private  function KeywordNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)' الكلمات الدلية غير موجودة'   ],200);
    }
    
     private  function placeNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا المكان غير موجود لدينا'   ],200);
    }

}
