<?php

namespace App\Http\Controllers\Api;

use App\Events\AcceptedPlaceOrder;
use App\Events\NewOrder;
use App\Http\Requests\api\rate;
use App\Http\Requests\api\scanRequestOrder;
use App\Http\Resources\scanOrderModel;
use App\Models\DayTranslation;
use App\Models\GymDay;
use App\Models\Order;
use App\Models\Point;
use App\Models\RateOrder;
use App\Models\Setting;
use App\Models\Transaction;
use App\User;
use Carbon\Carbon;
use App\Events\RefuseClientOrder;
use App\Events\RefusePlaceOrder;
use App\Http\Requests\api\makeOrder;
use App\Http\Requests\api\refuseOrder;
use App\Http\Resources\OrderFilterRecource;
use App\Http\Resources\OrderRecource;
use App\Models\OrderOld;
use App\Models\UserSetting;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public $headerApiToken;

    public function __construct( )
    {
        $this->middleware(['apiToken']);
        $this->middleware('order')->only(['store' ,'editOrder' ,'UserRefuseOrder']);

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = date('Y-m-d');
//        $time = Carbon::now()->format('H:i:s');
        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $orders = Order::whereUserId($user->id)->where('status','pending')->where('date','>=',$today)->latest()->get();

        $data = OrderFilterRecource::collection($orders);
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );

    }

    public function listOrderAccepted(){
        
        $today = date('Y-m-d');
        
        $user =  User::where( 'api_token' , $this->headerApiToken )->first();

        $orders = Order::whereUserId($user->id)->where('status','accepted')->where('date','>=',$today)->latest()->get();

        $data = OrderFilterRecource::collection($orders);
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }

    public function listOrderFinish(){
        $user =  User::where( 'api_token' , $this->headerApiToken )->first();

        $orders = Order::whereUserId($user->id)->where('status','finish')->latest()->get();

        $data = OrderFilterRecource::collection($orders);
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }

    public function listOrderRefuse(){
        $user =  User::where( 'api_token' , $this->headerApiToken )->first();

        $orders = Order::whereUserId($user->id)->whereIn('status', ['refuse_user', 'refuse_gym'])->get();

        $data = OrderFilterRecource::collection($orders);

        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(makeOrder $request)
    {

        $app_percentage = Setting::whereKey('app_percentage')->first();

         $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $gym = User::whereId($request->gymId)->first();

        $diffInTime = $this->checkDiffInMinutes($request);

        $order = new Order();
        $order->user_id = $user->id;
        $order->gym_id = $gym->id;
        $order->price_gym = $gym->price;
        $order->time_gym = $diffInTime;
        $order->app_percentage = $app_percentage ? $app_percentage->body : 0;
        $order->time_from = $request->time_from;
        $order->time_to = $request->time_to;
        $order->date = $request->date;
        $order->save();
//        $this->payOrder($user , $gym->price);

        return response()->json( [
            'status' => 200 ,
            'message' => 'تم إنشاء طلبك بنجاح'
        ] , 200 );
    }

    function payOrder($user , $value){
        $tranction  = new  Transaction();
        $tranction->user_id = $user->id;
        $tranction->progress = 'pull';
        $tranction->value = -$value;
        $tranction->save();
    }

    public function editOrder(makeOrder $request)
    {
        $order = Order::find($request->orderId);

         if ($order['status'] != 'pending'){// Check have action before
             return $this->OrderCantEdit();
         }

        $diffInTime = $this->checkDiffInMinutes($request);
        $order->time_gym = $diffInTime;
        $order->time_from = $request->time_from;
        $order->time_to = $request->time_to;
        $order->date = $request->date;
        $order->save();

        $data = new OrderRecource($order);
        return response()->json( [
            'status' => 200 ,
            'message' => 'تم تعديل طلبك علي الجيم بنجاح',
            'data' => $data
        ] , 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        if (!$order){
            return $this->OrderNotFound();
        }

        $data = new OrderRecource($order);

        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if (!$order){  return $this->OrderNotFound();  }

        $order->delete();
        $notify =  Notification::whereOrderId($order->id)->get();
        if (count($notify)){
            $notify->each->delete();
        }
        return response()->json( [
            'status' => 200 ,
            'message' => 'تم مسح طلبك بنجاح'
        ] , 200 );
    }

    public function UserRefuseOrder(refuseOrder $request){

        $order = Order::whereId($request->orderId)->first();

        if ($order['status'] != 'pending'){  // Check have action before
            return $this->actionOrderByPlace();
        }
        $order->update(['status' => 'refuse_user', 'message' => $request->message]);
        $message = 'تم إلغاء الحجز علي الجيم بنجاح';
        return response()->json( [
            'status' => 200 ,
            'message' => $message
        ] , 200 );
    }

    public function scanOrder(scanRequestOrder $request){ // The middleware Valid Request Of Scan Order

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $gym = User::whereId($request->gymId)->first();

        $today = Carbon::now();
            $date = $today->format('Y-m-d');
            $time = $today->format('H:i:s');

        $order = Order::whereUserId($user->id)->whereGymId($gym->id)->where('date','=',$date)->whereStatus('accepted')->first();

        $system_setting = Setting::whereIn('key',['minutes_system_scan','point_system','money_point_system' ,'minutes_control'])->get();

        $point_percentage = $this->calcPointSystem($system_setting );

        if ($order->real_time_from != '00:00:00'){

            //Time out setting By admin Calculates the increment time
             $minutesAdmin = $system_setting->where('key','minutes_system_scan')->first();

            // minutes_control all Gym Have this time To make user finish order
             $minutes_control_by_admin = $system_setting->where('key','minutes_control')->first();
             $minutes_control           = $minutes_control_by_admin? $minutes_control_by_admin->body : 1;

            //minUser the Time user choose in make order to finish
                $minUser = $this->userFinishTimeIn($order);

            // $dateTimeOrder To add hour and minute
                $orderTimeStart  = $order->date . $order->real_time_from;

            // Time User should end with minutes admin
            $actual_user_must_end =  Carbon::parse($orderTimeStart)->addMinutes($minUser  )->addMinutes($minutesAdmin ? $minutesAdmin->body : 0);

//            return $actual_user_must_end;
            $actual_end_at   = Carbon::parse($date . $time);

            return  $this->responseAfterTime($order , $actual_end_at ,$actual_user_must_end , $time , $gym , $user  ,$point_percentage , $minutes_control);

        }else{

            return $this->firstEnterUser($order,$time,$user , $date);
        }
    }

    function responseAfterTime($order , $actual_end_at ,$actual_user_must_end , $time , $gym , $user ,$point_percentage, $minutes_control){

        if (strtotime($actual_end_at) > strtotime($actual_user_must_end)){

            $timeOver = $this->overTime($actual_end_at , $actual_user_must_end);


            $overTime = ceil($timeOver  / $minutes_control) *  $minutes_control;

            $price =    ceil($timeOver  / $minutes_control) *  $gym->price;

            // pay Order To make user pay this over time
            $this->payOrder($user,$price);

            $this->calcPointsOverTime($order,$gym,$user ,$point_percentage);

            $order->update(['real_time_to'=> $time ,'over_time'=> $overTime ,'price'=> $price ,'status' => 'finish' ]);

            return response()->json( [
                'status' => 200 ,
                'check_in ' => false,
                'message' =>    '  مع العلم انه تم احتساب وقت إضافي بمقدار '.$overTime.' دقيقة '. $user->name .' تشرفنا بوجودك  ',
                'data' => new scanOrderModel($order)
            ] , 200 );

        }else{

            $this->calcPointTime($order,$user ,$point_percentage);

            $order->update(['real_time_to'=> $time ,'status' => 'finish' ]);

            return response()->json( [
                'status' => 200 ,
                'check_in ' => false,
                'message' => $user->name .' تشرفنا بوجودك  ',
                'data' => new scanOrderModel($order)
            ] , 200 );
        }
    }

    function userFinishTimeIn($order ){
        // $dateTimeOrder To add hour and minute
        $dateTimeOrderStart = $order->date . $order->time_from;
        $dateTimeOrderFinish = $order->date . $order->time_to;

        $actual_start_at = Carbon::parse($dateTimeOrderStart);

        $actual_end_at   = Carbon::parse($dateTimeOrderFinish);
        $minUserFinish            = $actual_end_at->diffInMinutes($actual_start_at, true);

        return $minUserFinish;
    }

    function overTime($actual_end_at ,$actual_user_must_end ){
        $overTime    = ceil( $actual_end_at->diffInSeconds($actual_user_must_end, true) / 60) ;

        return $overTime ;
    }

    function firstEnterUser($order , $time , $user , $date){

        $order->update(['real_time_from' => $time ,'real_date_enter' => $date]);

        return response()->json( [
            'status' => 200 ,
            'check_in ' => true,
            'message' => $user->name .' تفضل بالدخول ',
            'data' => new scanOrderModel($order)
        ] , 200 );
    }

    function calcPointSystem($system_setting ){

        $point_system = $system_setting->where('key','point_system')->first()->body;

        $money_point_system = $system_setting->where('key','money_point_system')->first()->body;

        $point_percentage = $money_point_system/ $point_system;

        return  $point_percentage;
    }

    function calcPointsOverTime($order ,$gym , $user , $point_percentage){
        $money =  number_format(($order->price_gym + $gym->price ) /$point_percentage ,0);
        $point = new Point();
        $point->user_id = $user->id;
        $point->order_id = $order->id;
        $point->value = $money;
        $point->save();
    }

    function calcPointTime($order  , $user , $point_percentage){
        $money = number_format($order->price_gym / $point_percentage , 0) ;
        $point = new Point();
        $point->user_id = $user->id;
        $point->order_id = $order->id;
        $point->value = $money;
        $point->save();
    }

    public function rateGym(rate $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $order = Order::whereId($request->orderId)->first();

        if ( ! $order ) {   return $this->OrderNotFound();  }

        $rating = new RateOrder();

        $userRatingBefore = $rating->where('user_id', $user->id)->where('order_id', $order->id)->first();

        if ($userRatingBefore) {

            $rating = RateOrder::whereId($userRatingBefore->id)->first();
            $rating->user_id = $user->id;
            $rating->order_id = $order->id;
            $rating->rate = $request->rateValue;
            $rating->comment = $request->comment;
            $rating->save();

        }else{
            $rating->user_id = $user->id;
            $rating->order_id = $order->id;
            $rating->rate = $request->rateValue;
            $rating->comment = $request->comment;
            $rating->save();
        }

        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.rate_success'),
        ] , 200 );
    }

    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }


    function checkDiffInMinutes($request ){
        // $dateTimeOrder To add hour and minute
        $dateTimeOrderStart = $request->date . $request->time_from;
        $dateTimeOrderFinish = $request->date . $request->time_to;

        $actual_start_at = Carbon::parse($dateTimeOrderStart);

        $actual_end_at   = Carbon::parse($dateTimeOrderFinish);
        $minUserFinish            = $actual_end_at->diffInMinutes($actual_start_at, true);

        return $minUserFinish;
    }

    private  function OrderCantActionUser(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب لا ينتمي للمسخدم    '],200);
    }

    private  function OrderCantAction(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب لا ينتمي لصاحب المكان'   ],200);
    }


    private  function OrderCantEdit(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب لم يعد في حالة الغير مؤكد لتعديل طلبك'   ],200);
    }

    private  function refuseOrderByUser(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب لم يعد في حالة الغير مؤكد لقبوله او رفضه'   ],200);
    }

    private  function actionOrderByPlace(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب لا يمكنك إلغاءه لقبوله او رفضه سابقا'   ],200);
    }
}
