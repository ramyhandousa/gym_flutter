<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\OfferStore;
use App\Http\Resources\offerRecource;
use App\Models\Image;
use App\Models\Meal;
use App\Models\Offer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class offerController extends Controller
{

    public $public_path;

    public function __construct(  )
    {

        $this->middleware('apiToken',[ 'except' =>  ['show' ] ]);

        // Middleware Check Offer For Some Validation
        $this->middleware('checkOffer')->only(['store' ,'editOffer']);

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->public_path = 'files/offers/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
         $type = $request->type ?$request->type : 'meal';
         
        $list  = Offer::where('defined_offer', $type)->where('user_id',$user->id )->whereHas('user',function($q){
            $q->where('is_suspend',0);
        })->where('is_accepted', 1)->latest()->whereDate('period_end','>=',today())->with('meal')->get();

        $data  =  offerRecource::collection($list);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferStore $request)
    {

        $user = Auth::user();

        $offer = new Offer();
        $offer->user_id = $user->id;

        if ($request->type){
            $offer->defined_offer = $request->type;
        }

        if ($request->mealId){
            $offer->meal_id = $request->mealId;
            $offer->price = $request->price;
        }

        $offer->period_start = $request->period_start;
        $offer->period_end = $request->period_end;

        // ids from upload Images
        if ($request->images ){
            $myids =  implode(',',$request->images) ;
            $filterIds = explode(',',$myids);
            $offer->images = $filterIds;
        }

        $offer->save();

        if ($request->images ){

            Image::whereIn('id' , $filterIds)->update(['images_id' => $offer->id]);
        }

        $data  =  new offerRecource($offer);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offer = Offer::where('is_accepted', 1)->find($id);

        if (!$offer){  return $this->offerNotFound();  }

        $data  =  new offerRecource($offer);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function editOffer(OfferStore $request){

        $data = $request->all();


        $offer = Offer::find($data['offerId']);
 
        $offer->fill($data);
        
        if ($request->mealId){
            
            $mealExist = Offer::where('id','!=',$offer->id)->where('meal_id',$request->mealId)->whereDate('period_end','>=',today())->first();
            if($mealExist){
               return $this->offerFoundBefore();   
            }
            $offer->meal_id = $request->mealId; 
        }

        // ids from upload Images
        if ($request->images ){
            $this->updateImageForOffer($request, $offer);
        }

        $offer->save();

        $data  =  new offerRecource($offer);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::find($id);

        if (!$offer){  return $this->offerNotFound(); }

        if ($offer->delete()) {

            $this->deleteImageForOffer($id);

            return response()->json( [
                'status' => 200 ,
                'message' => trans('global.offer_delete'),
            ] , 200 );
        }

    }

    private function updateImageForOffer($request,$offer){

        $myids =  implode(',',$request->images) ;

        $filterIds = explode(',',$myids);

        $offer->images = $filterIds;

        $images = Image::whereIn('id' , $filterIds);

        $images->update(['images_id' => $offer->id]);

        // This For check Count Of Image Before Delete
        // Check For Ids Not used Any More in Table Image
        if (count($images->get()) > 0 ){

            $dataRemove =  Image::where('images_id',$offer->id)->whereNotIn('id',$filterIds)->get();

            if (count($dataRemove) > 0 ){

                foreach ($dataRemove as $item){
                    $item->delete();
                }

            }

        }
    }


    private function deleteImageForOffer($id){

        $images = Image::where('images_id',$id)->get();

        if (count($images) > 0 ){

            foreach ($images as $item){
                $item->delete();
            }

        }
    }


    public function uploadImage(Request $request){

        $upload = new Image();
        $upload->images_type = 'App\Models\Offer';

        if ($request->type){

            $upload->type = $request->type;
        }

        $upload->url =  'public/' . $this->public_path . \UploadImage::uploadImage( $request , 'url' , $this->public_path );
        $upload->save();

        return response()->json( [
            'status' => 200 ,
            'data' => $upload ,
        ] , 200 );
    }


    private  function MealNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذه الوجبة غير موجود '   ],200);
    }

    private  function offerPriceError(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.offer_price_error')   ],200);
    }

    private  function offerNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.offer_not_found')   ],200);
    }
    
    
    private  function offerFoundBefore(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذه الوجبة لديها عرض مسبقا'   ],200);
    }

}
