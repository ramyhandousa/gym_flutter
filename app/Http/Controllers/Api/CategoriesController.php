<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\categoryRecource;
use App\Http\Resources\listMealRecource;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{


    public function __construct( )
    {
        $this->middleware('apiToken', [ 'except' =>  [ 'show']  ]);

        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $user = Auth::user();


        $data = categoryRecource::collection($user->categories);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();

        if (collect($user->categories)->contains('name', $request->categoryName)){
           return $this->CategoryFoundBefore();
        }

        $category = new Category();
        $category->name = $request->categoryName;
        $category->save();

        $user->categories()->attach($category);

       $data  =  new categoryRecource($category);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if (!$category){  return $this->CategoryNotFound();  }

        $data  =  new categoryRecource($category);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $category = Category::whereId($id)->first();

        if (!$category){  return $this->CategoryNotFound();  }

        $exists =  $user->categories->where('id',$id)->first();

        $meals =  $user->meals->whereIn('category_id', $id)->values();

        if ($exists){

            $exists['pivot']->delete();

            if (count($meals) > 0){

                $meals->each->delete();
                $meals->each->deleteTranslations();

            }

            return response()->json( [
                'status' => 200 ,
                'message' => trans('global.category_delete_success'),
            ] , 200 );

        }else{

            return response()->json( [
                'status' => 200 ,
                'message' => trans('global.category_not_belong'),
            ] , 200 );
        }

    }

    public function editCategory(Request $request){

        $user = Auth::user();

        $category = Category::whereId($request->categoryId)->with('meals')->first();

        if (!$category){  return $this->CategoryNotFound();  }

        $exists =  $user->categories->where('id',$request->categoryId)->first();

        if (!$exists){
            return response()->json( [
                'status' => 200 ,
                'message' => trans('global.category_not_belong'),
            ] , 200 );
        }

        $category->name = $request->categoryName;
        $category->save();

        $data = new listMealRecource($category);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );


    }


    private  function CategoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذا القسم غير موجود '   ],200);
    }


    private  function CategoryFoundBefore(){
        return response()->json([   'status' => 400,  'error' => (array)'هذا القسم لديك مسبقا '   ],200);
    }
}
