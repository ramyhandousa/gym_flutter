<?php

namespace App\Http\Controllers\Gym;


use App\Models\Order;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Main;


class HomeController extends Controller
{


 

    public function __construct()
    {
    
    
    }

    public function index()
    {

         if (!auth()->check())
            return redirect(route('Gym.login'));

        $id = Auth::id();
        $orderPending = Order::whereGymId($id)->whereStatus('pending')->count();
        $orderAccepted = Order::whereGymId($id)->whereStatus('accepted')->count();
        $orderComplete = Order::whereGymId($id)->whereStatus('finish')->count();
        $orderToday = Order::whereGymId($id)->where('date', '=', Carbon::today())->count();


        return view('Gym.home.index',compact('orderPending','orderAccepted','orderComplete','orderToday'));
    }


    public function information_admin () {

        return view('Gym.staticData');
    }
}
