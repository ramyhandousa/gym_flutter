<?php

namespace App\Http\Controllers\Gym;

use App\Models\GymClass;
use App\Models\GymClassDay;
use App\Models\GymDay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClassessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = GymClass::where('gym_id', \Auth::id())->get();

        $classes->map(function ($q){
           $q->time = date('h:i A', strtotime($q->time));
        });
        $pageName = ' الكلاس';

        return view('Gym.classes.index',compact('classes','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageName = ' الكلاس';

        return view('Gym.classes.create', compact('pageName','days'));
    }

    public function daysClasses(Request $request)
    {

        $pageName = ' الكلاس';

        $days = GymDay::whereGymId(Auth::id())->get();

        $data = $request->all();

        return view('Gym.classes.days', compact('pageName','days'))->with(['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data  =    $request->all();

        $this->validate($request, [
            'image'  => 'required|image|mimes:jpg,jpeg,png|max:2048'
        ]);


        DB::beginTransaction();

            $gym = $this->createClass($request);
        //  $filtered To make Sure Gym Choose One Day
            $filtered = collect($data['day'])->filter(function ($value, $key) {
                return $value['start'] != null && $value['duration_session'] != null;
            });


        if ($filtered->count() == 0){

            return redirect()->back()->withErrors(['يجب اختيار يوم واحد علي الاقل من فضلك مع التاكد من تحديد معاد وتوقيت صحيح', 'The Message']);
        }
            $days = $this->updateDayForGym($filtered,$gym);

        if( !$days || !$gym )
        {
            DB::commit();
            session()->flash('success', 'لقد تم إضافة بيانات الكلاس  بنجاح.');

            return redirect()->route('classes_dashboard.index');
        } else {
            // Else commit the queries
            DB::rollback();
        }

    }

    function createClass($request){
        $gym = new  GymClass();
        $gym->gym_id = Auth::id();
        $gym->{'name:ar'} = $request->name_ar;
        $gym->{'name:en'} = $request->name_en;
        $gym->{'captain_name:ar'} = $request->captain_name_ar;
        $gym->{'captain_name:en'} = $request->captain_name_en;
        $gym->{'description:ar'} = $request->description_ar;
        $gym->{'description:en'} = $request->description_en;
        if ($request->hasFile('image')):
            $name= time() . '.' . str_random(20) . urlencode($request->image->getClientOriginalName());
            $request->image->move(public_path().'/gym/gallery/images/', $name);
            $gym->image = '/public/gym/gallery/images/'.$name;
        endif;

        $gym->save();
        return $gym;
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $days = GymDay::whereGymId(Auth::id())->get();

        $class_days = GymClassDay::where('gym_classes_id',$id)->with('gym_class')->get();

        $pageName = ' تفاصيل الكلاس';

        $gym_class = GymClass::findOrFail($id);

        return view('Gym.classes.show', compact('pageName' ,'class_days','days','data' ,'gym_class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gym_class = GymClass::findOrFail($id);
        $pageName = ' الكلاس';
        return view('Gym.classes.edit', compact('pageName' ,'gym_class'));
    }

    public function editClassDays(Request $request){
        $class_days = GymClassDay::where('gym_classes_id',$request->class_id)->with('gym_class')->get();

        $days = GymDay::whereGymId(Auth::id())->get();

        $data = $request->all();
        $pageName = ' الكلاس';
        return view('Gym.classes.editDays', compact('pageName' ,'class_days','days','data'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();

        $this->validate($request, [
            'image'  => 'image|mimes:jpg,jpeg,png|max:2048'
        ]);


        DB::beginTransaction();

        $gym = $this->editClass($id, $request);
        //  $filtered To make Sure Gym Choose One Day
        $filtered = collect($data['day'])->filter(function ($value, $key) {
            return $value['start'] != null && $value['duration_session'] != null;
        });


        if ($filtered->count() == 0){

            return redirect()->back()->withErrors(['يجب اختيار يوم واحد علي الاقل من فضلك مع التاكد من تحديد معاد وتوقيت صحيح', 'The Message']);
        }
        $days = $this->updateDayForGym($filtered,$gym);

        if( !$days || !$gym )
        {
            DB::commit();
            session()->flash('success', 'لقد تم تعديل بيانات الكلاس  بنجاح.');

            return redirect()->route('classes_dashboard.index');
        } else {
            // Else commit the queries
            DB::rollback();
        }


    }

    function editClass($id , $request){
        $gym = GymClass::findOrFail($id);
        $gym->{'name:ar'} = $request->name_ar;
        $gym->{'name:en'} = $request->name_en;
        $gym->{'captain_name:ar'} = $request->captain_name_ar;
        $gym->{'captain_name:en'} = $request->captain_name_en;
        $gym->{'description:ar'} = $request->description_ar;
        $gym->{'description:en'} = $request->description_en;
        if ($request->hasFile('image')):
            $name= time() . '.' . str_random(20) . urlencode($request->image->getClientOriginalName());
            $request->image->move(public_path().'/gym/gallery/images/', $name);
            $gym->image = '/public/gym/gallery/images/'.$name;
        endif;
        $gym->save();

        return $gym;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function suspend(Request $request)
    {
        $model = GymClass::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم فك الحظر على المنتج بنجاح";
        } else {
            $message = "لقد تم حظر  المنتج بنجاح";
        }


        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


    public function delete(Request $request)
    {
        $model = GymClass::findOrFail($request->id);

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }

    function updateDayForGym($data , $class){
        $allDays =  [ ];
        foreach($data as $value)
        {
            $value['gym_classes_id']    = $class->id;
            $value['time']     = $value['start'] ? date("H:i:s", strtotime($value['start'])) : null;
            $allDays[] = $value;
        }
        GymClassDay::where('gym_classes_id', $class->id)->delete();
        GymClassDay::insert($allDays);
    }


}
