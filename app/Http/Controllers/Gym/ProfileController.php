<?php

namespace App\Http\Controllers\Gym;

use App\Http\Requests\Admin\EditGym;
use App\Models\City;
use App\Models\GymDay;
use App\Models\GymImage;
use App\Models\Order;
use App\Models\Service;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::whereId($id)->with('services.service','gallery','days')->first();

        $services = $user['services'] ? $user['services']->pluck('service') : [];

        $days = $user['days'];

        $gallery = $user['gallery'];

        $cities = City::where('is_suspend',0)->whereParentId(null)->get();

        if ($user->city_id) {

            $city = City::whereId($user->city_id)->first();
            $cityName = $city ? $city->name : ' ';
        }else{
            $cityName = '';
        }
        return view('Gym.profile.show',compact('user', 'cities', 'cityName' ,'days','gallery','services'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::whereId($id)->with('services.service','gallery','days')->first();


//        $userServices = $user['services'] ? $user['services']->pluck('service') : [];

        $services = Service::select('id')->get();

        $days = $user['days'];

        $gallery = $user['gallery'];

        $cities = City::where('is_suspend',0)->whereParentId(null)->get();

        if ($user->city_id) {

            $city = City::whereId($user->city_id)->first();
            $cityName = $city ? $city->name : ' ';
        }else{
            $cityName = '';
        }
        return view('Gym.profile.edit',compact('user', 'cities', 'cityName' ,'days','gallery', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditGym $request, $id)
    {

        $data =  $request->all();

        $user = Auth::user();
        //  $filtered To make Sure Gym Choose One Day
        $filtered = collect($data['day'])->filter(function ($value, $key) {
            return $value['start'] != null && $value['end'] != null;
        });
        if ($filtered->count() == 0){

            return redirect()->back()->withErrors(['يجب اختيار يوم واحد علي الاقل بداية و نهاية له في الاسبوع', 'The Message']);
        }


        $user->city_id      = $request->city ? $request->city  : $user->city_id;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->phone        = $request->phone;
        $user->address      = $request->address;
        $user->latitute     = $request->latitute;
        $user->longitute     = $request->longitute;
        $user->price        = $request->price;
        $user->description  = $request->description;

        if ($request->hasFile('image')):

            $name= time() . '.' . str_random(20) . urlencode($request->image->getClientOriginalName());
            $request->image->move(public_path().'/gym/gallery/images/', $name);
            $user->image = $request->root() .  '/public/gym/gallery/images/'.$name;

        endif;

        $user->save();

        if ($filtered->count() > 0){
            $this->updateDayForGym($data,$user);
        }

        session()->flash('success', 'لقد تم تحديث  البيانات بنجاح.');
        return redirect()->back();
    }

    function updateDayForGym($data , $user){

        $allDays =  [ ];
        foreach($data['day'] as $value)
        {
            $value['gym_id']    = $user->id;
            $value['time_from'] = $value['start'] ? $value['start'] : null;
            $value['time_to']   = $value['end']  ? $value['end'] : null ;
            $value['start']     = $value['start'] ? date("H:i:s", strtotime($value['start'])) : null;
            $value['end']       = $value['end'] ? date("H:i:s", strtotime($value['end'])) : null;
            $value['working']   = $value['start'] && $value['end'] ? 'open' : 'off';
            $allDays[] = $value;

        }
        GymDay::where('gym_id', $user->id)->delete();
        GymDay::insert($allDays);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function myGallery(){

        $user = User::whereId(Auth::id())->with('gallery')->first();

        $gallery = $user['gallery'];


        return view('Gym.profile.gallery' ,compact('gallery'));
    }

    public function updateGallery(Request $request){

        $user = User::whereId(Auth::id())->with('gallery')->first();

        if ($request->file('myImage')){

            foreach ($request->file('myImage') as $id => $url ):

                $name= time() . '.' . str_random(20) . urlencode($url['url']->getClientOriginalName());

                $url['url']->move(public_path().'/gym/gallery/images/', $name);

                GymImage::where('gym_id',Auth::id())->whereId($id)->update(['url' => 'public/gym/gallery/images/'.$name ]);
            endforeach;

        }

        if (count($request->images) > 0){

            foreach ($request->file('images') as $image ):

                $name= time() . '.' . str_random(20) . urlencode($image->getClientOriginalName());

                $image->move(public_path().'/gym/gallery/images/', $name);

                $gallery = new GymImage();
                $gallery->gym_id = Auth::id();
                $gallery->url = 'public/gym/gallery/images/'.$name;
                $gallery->save();

            endforeach;
        }

        session()->flash('success', 'لقد تم تحديث معرض الصور  بنجاح.');
        return redirect()->back();
    }


    public function deleteGallery(Request $request)
    {
        $model = GymImage::findOrFail($request->id);

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }

    public function updateServices(Request $request)
    {
//        return $request->all();
//        $model = GymImage::find
//.OrFail($request->id);

//        if ($model->delete()) {
//
//            return response()->json([
//                'status' => true,
//                'data' => $model->id
//            ]);
//        }

        return response()->json( [
            'status' => true ,
        ] , 200 );
    }


    public function changePassword(Request $request){

        $user = \Auth::user();

        if ( Hash::check( $request->oldPassword , $user->password ) ) {

            $user->update( [ 'password' => $request->newPassword ] );

            return response()->json( [
                'status' => 200 ,
                'message' =>  trans('global.password_was_edited_successfully')  ,
            ] , 200 );

        }
        else {
            return response()->json( [
                'status' => 400 ,
                'error' => (array) trans('global.old_password_is_incorrect')  ,
            ] , 200 );
        }
    }

    public function myWallet(Request $request){

        $id = Auth::id();
        $orders = Order::whereGymId($id)->whereStatus('finish');

        $wallet_orders =  $orders->get();

        $wallet_orders->map(function ($q){
            $q->my_wallet =  ($q->price_gym + $q->price )  - ( ($q->price_gym + $q->price ) * $q->app_percentage  /100 );
        });

        // walletOrders that value must admin pay
        $walletOrders =  $wallet_orders->sum('my_wallet');

        $orderComplete = $orders->count();

        $my_wallet = Transaction::whereUserId($id)->sum('value');


        return view('Gym.profile.wallet' , compact('orderComplete' ,'walletOrders' ,'my_wallet'));

    }

    public function classes(){

        return view('Gym.profile.classes');
    }

}
