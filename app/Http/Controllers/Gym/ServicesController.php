<?php

namespace App\Http\Controllers\Gym;

use App\Models\GymService;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $gym_service = GymService::whereGymId(Auth::id())->with('service')->get();
        $services = Service::all();
        $user = Auth::user();

       $pageName  = 'الخدمات';

        return view('Gym.Services.index',compact('pageName','gym_service','services','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        GymService::whereGymId(Auth::id())->delete();

        foreach ($request->services as $value ){
            GymService::createOrUpdateService(Auth::user(),$value );
        }

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function suspend(Request $request)
    {
        $model = GymService::findOrFail($request->id);

        $model->is_suspend = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم حظر  الخدمة بنجاح";
        } else {
            $message = "لقد تم فك الحظر على الخدمة بنجاح";
        }


        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }
}
