<?php

namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\City;
use App\Models\DealOfDay;
use App\Models\OrderOld;
use App\Models\Product;
use App\Models\ProductPurchase;
use App\Models\PurchaseType;
use App\uploadImages;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class indexController extends Controller
{
    public function index(){

        $cities = City::where('is_suspend',0)->get();


        return view('Site.index', compact('cities'));
    }


    public function register(Request $request){

        if (!$request->idUser){

            session()->flash('errors', 'تاكد من الرابط من فضلك.');
            return redirect()->back()->withInput(Input::all());

        }

        $post_data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'city' => $request->city,
        ];

        //set Rules .....
        $valRules = [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'city' => 'required',
        ];

        //Declare Validation message
        $valMessages = [
            'name.required' => 'الإسم مطلوب',
            'phone.required' => 'رقم الهاتف مطلوب',
            'email.required' => 'البريد الإلكتروني مطلوب',
            'city.required' => '  المدينة مطلوبة',
        ];

        //validate inputs ......
        $valResult = Validator::make($post_data, $valRules, $valMessages);

        if ($valResult->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($valResult)
                    ->withInput();
        }

        $user = User::where('uuid',$request->idUser)->first();


        $order = new OrderOld();
        $order->user_id = $user->id;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone =$request->phone;
        $order->city_id = $request->city;
        $order->user_address = $request->userAddress;
        $order->address = $request->address;
        $order->latitute = $request->latitute;
        $order->longitute = $request->longitute;
        $order->description = $request->description;
        $order->save();


        return redirect()->route('successRegister');

    }

    public function success(){

        return view('Site.success');
    }


}
