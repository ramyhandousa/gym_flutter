<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Validator;
use UploadImage;


class CategoriesController extends Controller
{
 
    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/categories/';
    }
 
    public function index(Request $request)
    {

        if (!$request->type){

            $users = User::whereHas('categories')->with('categories')->get();
            if ($users->count()){
                $categories = $users->pluck('categories')->collapse();
                $categories->map(function ($q){
                    $q->userName  = User::whereId($q->pivot['user_id'])->first()->fullName;
                });
            }else{
                $categories = [];
            }

	        $pageName = 'إدارة الاقسام الرئيسية';
	        
	 }else{

	     
	     $categories = Category::where('parent_id','!=', 0)->latest()->get();
	     $pageName = 'إدارة الاقسام الفرعية';
	     
        }
	
	  
        return view('admin.categories.index',compact('categories','pageName'));
    }
    public function create()
    {
	   $cats = Category::whereParentId(0)->where('is_suspend',0)->get();
	 $pageName = '   اسم القسم  الرئيسي';
	 if (request('type') =='subcategories'  &&request('type') != ' ' ){
	        
	        $pageName = '   اسم القسم  الفرعي';
	 }

        
        return view('admin.categories.create')->with(compact('cats','pageName'));
    }
    
    public function store(Request $request)
    {
   
        $category = new Category;

        if (request('type') =='subcategories'){ $category->parent_id = $request->parentId;  }

        $category->{'name:ar'} = $request->name_ar;
        $category->{'name:en'} = $request->name_en;

        if ($request->hasFile('image')):
            $category->image = $request->root() . '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;

        if ($category->save()) {

            if ($request->type == 'subcategories'){
                $url =  route('categories.index').'?type=subcategories';
                $name = 'القسم الفرعي';
            }else{
                $url =  route('categories.index');
                $name = 'القسم الرئيسي';
            }
            return response()->json([
                'status' => true,
                "message" => __('trans.addingSuccess',['itemName' => $name]),
                "url" => $url,

            ]);


        }
            
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {

    $cats = Category::whereParentId(0)->where('is_suspend',0)->get();

    $category = Category::findOrFail($id);
    $pageName = '   اسم القسم  الرئيسي';
    if (request('type') =='subcategories' &&request('type') != ' ' ){

        $pageName = '   اسم القسم  الفرعي';
    }


        return view('admin.categories.edit')->with(compact('category', 'cats', 'pageName' , 'subSubCategories'));

    }

    
    public function update(Request $request, $id)
    {
 
        $category = Category::findOrFail($id);

        if (request('type') =='subcategories'){ $category->parent_id = $request->parentId;  }

        $category->{'name:ar'} = $request->name_ar;
        $category->{'name:en'} = $request->name_en;

        if ($request->hasFile('image')):
            $category->image = $request->root() . '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;

        if ($category->save()) {

            if ($request->type == 'subcategories'){
                $url =  route('categories.index').'?type=subcategories';
                $name = 'القسم الفرعي';
            }else{
                $url =  route('categories.index');
                $name = 'القسم الرئيسي';
            }
            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => $name]),
                "url" => $url,

            ]);
        }

    }
    

       
       public function delete(Request $request){
	    if (!Gate::allows('settings_manage')) {
		 return abort(401);
	    }
	    
	    $model = Category::findOrFail($request->id);

	    if (count($model->product) > 0) {

		 return response()->json([
		         'status' => false,
		         'message' => "عفواً, لا يمكنك حذف القسم نظراً لوجود منتجات  فيها"
		 ]);
	    }
	   
	    
	    if ($model->delete()) {
            $model->deleteTranslations();
		 return response()->json([
		         'status' => true,
		         'data' => $model->id
		 ]);
	    }
	    
       }
       
       public function suspend(Request $request)
       {
	    $model = Category::findOrFail($request->id);
	    $model->is_suspend = $request->type;
	    if ($request->type == 1) {

            $message = "لقد تم حظر القسم بنجاح";
	    } else {
            $message = "لقد تم فك الحظر على القسم بنجاح";
	    }
//	    if (count($model->users) > 0) {
//		 return response()->json([
//		         'status' => false,
//		         'message' => 'عفواً, لا يمكنك حظر القسم نظراً لوجود مستخدمين مشتركين فيها'
//		 ]);
//	    }
//
//	    if (count($model->product) > 0) {
//
//		 return response()->json([
//		         'status' => false,
//		         'message' => "عفواً, لا يمكنك حذف القسم نظراً لوجود منتجات  فيها"
//		 ]);
//	    }
	    
	    if ($model->save()) {
		 return response()->json([
		         'status' => true,
		         'message' => $message,
		         'id' => $request->id,
		         'type' => $request->type
		 
		 ]);
	    }
	    
       }

    

}
