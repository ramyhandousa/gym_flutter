<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Day;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {

        $brands = Day::latest()->get();

        $pageName = 'الأيام';
        return view('admin.days.index',compact('brands','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageName = 'اليوم';
        return view('admin.days.create',compact('pageName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $brand  = new Day();
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;


        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'اليوم']),
            "url" => route('days.index'),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Day::findOrFail($id);
        $pageName = 'اليوم';

        return view('admin.days.edit',compact('brand','pageName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Day::findOrFail($id);
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;

        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'اليوم']),
            "url" => route('days.index'),

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function delete(Request $request){
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }

        $model = Day::findOrFail($request->id);

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }


}
