<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Currency;
use App\Models\DealOfDay;
use App\Models\ProductPurchase;
use App\Models\PurchaseType;
use App\Models\Size;
use App\uploadImages;
use App\User;
use Validator;
use UploadImage;
use Illuminate\Support\Facades\Gate;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public $public_path;

    function __construct()
    {
        $this->public_path = 'files/products/';

    }

    public function index()
    {
        $products = Product::latest()->with('category')->get();
        $products->map(function ($q) {
            $q->images =  $q->images ?$this->getImagesWithIds($q->images) : null;
        });

        $pageName = 'إدارة المنتجات  ';

        return view('admin.products.index',compact('products','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }
        $users = User::whereIsSuspend(0)->where('defined','company')->get();

        $pageName = ' المنتج';
        $categories = Category::whereParentId(0)->whereIsSuspend(0)->get();
        $brands = Brand::whereIsSuspend(0)->get();
        $currencies = Currency::all();
        $colors = Color::all();
        $sizes = Size::all();
        $purchaseType = PurchaseType::all();



        return view('admin.products.create' ,
            compact('users', 'categories','brands','currencies','colors','sizes','purchaseType','pageName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $product = new  Product();
        $product->{'name:ar'} = $request->name_ar;
        $product->{'name:en'} = $request->name_en;
        $product->{'description:ar'} = $request->description_ar;
        $product->{'description:en'} = $request->description_en;
        $product->user_id = $request->userId;
        $product->category_id = $request->categoryId;
        $product->brand_id = $request->brand;
        $product->currency_id = $request->currency;
        $product->size = $request->sizes;
        $product->colours = $request->colors;
        $product->price = $request->price;
        $product->quantity = $request->quantity;

        if ($request->isOffer == 1){
            $product->is_offer = 1;
            $product->offer_price = $request->priceOffer;
        }

        $product->save();

        if ($request->dealOfToday == 1){
            $deal = new DealOfDay();
            $deal->product_id = $product->id;
            $deal->end_data = $request->dealDate;
            $deal->save();
        }

        if (count($request->purchaseType) > 0){

            foreach ($request->purchaseType as $ids){
                $purchaseType = new ProductPurchase();
                $purchaseType->product_id = $product->id;
                $purchaseType->purchase_type_id = $ids;
                $purchaseType->save();
            }

        }


        if ($request->hasFile('image')):
            $ids = [];
            foreach($request->file('image') as $image)
            {
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);


                $images = new  uploadImages();

                $images->url = $request->root() . '/public/' . $this->public_path .$name;
                $images->save();
                array_push($ids,json_encode($images->id));

            }

            $product->update(['images' => $ids ]);

        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'المنتج  ']),
            "url" => route('products.index'),

        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);


        if (!empty($product->size)){
            $product['size'] =  $product['size'] ?$this->getSizes($product->size) : null;
        }
        if (!empty($product->colours)){
            $product['colours'] =  $product['colours'] ?$this->getColors($product->colours) : null;
        }
        if (!empty($product->images)){
            $product['images'] =  $product['images'] ?$this->getImagesWithIds($product->images) : null;
        }
        $pageName = ' المنتج';
        $users = User::whereIsSuspend(0)->where('defined','company')->get();
        $categories = Category::whereParentId(0)->whereIsSuspend(0)->get();
        $brands = Brand::whereIsSuspend(0)->get();
        $currencies = Currency::all();
        $colors = Color::select('id')->get();
        $sizes = Size::select('id')->get();
        $purchaseType = PurchaseType::all();

        $productPurchase = ProductPurchase::whereProductId($product->id)->with('purchase')->get();

        $purchase =  collect($productPurchase)->map(function ($q){
            return $q->only(['purchase']);
        })->flatten();

        $dealOfDay = DealOfDay::whereProductId($product->id)->first();


        return view('admin.products.edit',
            compact('users', 'categories','brands','currencies','purchase','dealOfDay',
                'colors','sizes','purchaseType','product','pageName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->{'name:ar'} = $request->name_ar;
        $product->{'name:en'} = $request->name_en;
        $product->{'description:ar'} = $request->description_ar;
        $product->{'description:en'} = $request->description_en;
        $product->user_id = $request->userId;
        $product->category_id = $request->categoryId;
        $product->brand_id = $request->brand;
        $product->currency_id = $request->currency;
        $product->size = $request->sizes;
        $product->colours = $request->colors;
        $product->price = $request->price;
        $product->quantity = $request->quantity;

        if ($request->isOffer == 1){
            $product->is_offer = 1;
            $product->offer_price = $request->priceOffer;
        }

        $product->save();

        if ($request->dealOfToday == 1){

            $deal = DealOfDay::whereProductId($product->id)->first();
            if ($deal){

                $deal->update(['product_id' => $product->id,'end_data' => $request->dealDate]);

            }else{
                
                $deal = new DealOfDay();
                $deal->product_id = $product->id;
                $deal->end_data = $request->dealDate;
                $deal->save();
            }

        }

        if (count($request->purchaseType) > 0){

           $found = ProductPurchase::whereProductId($product->id)->count();

           if (count($found) > 0){
               ProductPurchase::whereProductId($product->id)->delete();
           }
            foreach ($request->purchaseType as $ids){
                $purchaseType = new ProductPurchase();
                $purchaseType->product_id = $product->id;
                $purchaseType->purchase_type_id = $ids;
                $purchaseType->save();
            }

        }


        if ($request->hasFile('image')):
            $ids = [];
            foreach($request->file('image') as $image)
            {
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);


                $images = new  uploadImages();

                $images->url = $request->root() . '/public/' . $this->public_path .$name;
                $images->save();
                array_push($ids,json_encode($images->id));

            }

            $product->update(['images' => $ids ]);

        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'المنتج']),
            "url" => route('products.index'),

        ]);
    }

    public function suspend(Request $request)
    {
        $model = Product::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم فك الحظر على المنتج بنجاح";
        } else {
            $message = "لقد تم حظر  المنتج بنجاح";
        }


        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }



    public function delete(Request $request)
    {
        $model = Product::findOrFail($request->id);

        if ($model->delete()) {
            $model->deleteTranslations();
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }
    private function getImages($id) {

        $product =    Product::whereId($id)->first();

//        implode(" ,",  $product->images);
        $filter =  implode(',',  $product->images) ;
        $ids = explode(',',$filter);

        return   uploadImages::whereIn('id',$ids)->get();
    }

    private function getImagesWithIds($images){
        $image = uploadImages::whereIn('id',$images)->get(['url']);

        return $image;
    }

    private function getSizes($sizes){
        $sizes = Size::whereIn('id',$sizes)->select('id')->get();

        return $sizes;
    }

    private function getColors($colors){
        $colors = Color::whereIn('id',$colors)->select('id')->get();

        return $colors;
    }


}
