<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use UploadImage;
use Illuminate\Support\Facades\Gate;

class facilitiesController extends Controller
{
    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/banks/';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $brands = Service::latest()->get();

        $pageName = '   الخدمات';
        return view('admin.facilities.index',compact('brands','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $pageName = 'الخدمة  ';
        return view('admin.facilities.create',compact('pageName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand  = new Service();
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;
        if ($request->hasFile('image')):
            $brand->image =   '/public/' . $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;
        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الخدمة  ']),
            "url" => route('facilities.index'),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $brand = Service::findOrFail($id);
        $pageName = '  الخدمة';

        return view('admin.facilities.edit',compact('brand','pageName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand  =  Service::findOrFail($id);
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;
        if ($request->hasFile('image')):
            $brand->image =  '/public/' . $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;
        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الخدمة  ']),
            "url" => route('facilities.index'),

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function delete(Request $request){
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }

        $model = Service::findOrFail($request->id);

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }
}
