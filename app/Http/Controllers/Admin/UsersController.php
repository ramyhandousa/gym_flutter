<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GymValid;
use App\Mail\NewAccountGym;
use App\Models\Brand;
use App\Models\City;
use App\Models\GymDay;
use App\Models\GymImage;
use App\Models\Image;
use App\Notifications\sendEmailToNewAccount;
use App\User;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Mail;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use UploadImage;
use Validator;

class UsersController extends Controller
{


    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/users/';
    }
    
    public function index(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $quary = User::whereDoesntHave('roles')->whereDoesntHave('abilities');

        if ($request->type == 'gym'){
            $quary->where('defined_user','gym');
            $pageName = 'إدارة الجيم';
        }else{
            $quary->where('defined_user','user');
            $pageName = 'إدارة المستخدمين';
        }

        $users =  $quary->latest()->get();

        return view('admin.users.index', compact('users' , 'pageName'));

    }


  
    public function create()
    {

        $cities = City::where('is_suspend',0)->whereParentId(null)->get();
        return view('admin.users.add', compact('cities'));
    }

    public function testImageView(Request $request){



        $cities = City::where('is_suspend',0)->whereParentId(null)->get();
        return view('admin.users.testing', compact('cities'));
    }


    public function testImage(Request $request){

        return $request->all();
    }

  
    public function store(GymValid $request)
    {

        $data =  $request->all();

        //  $filtered To make Sure Gym Choose One Day
        $filtered = collect($data['day'])->filter(function ($value, $key) {
            return $value['start'] != null && $value['end'] != null;
        });
            if ($filtered->count() == 0){

                return \Redirect::back()->withErrors(['يجب اختيار يوم واحد علي الاقل بداية و نهاية له في الاسبوع', 'The Message']);
            }

        $user               = new User;
        $user->defined_user = 'gym';
        $user->city_id      = $request->city;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->phone        = $request->phone;
        $user->password     = $request->password;
        $user->address      = $request->address;
        $user->latitute     = $request->latitute;
        $user->longitute     = $request->longitute;
        $user->price        = $request->price;
        $user->description  = $request->description;
        $user->api_token = str_random(60);
        $user->is_active    = 1;
        $user->save();

        Mail::to($user->email)->send(new NewAccountGym($user, $request));

        $this->createGalleryGym($request, $data , $user);

        if ($filtered->count() > 0){
            $this->createDayForGym($data,$user);
        }

        session()->flash('success', 'لقد تم إضافة الجيم بنجاح.');
        return redirect()->route('users.index','type=gym');
    }

    function createGalleryGym($request, $data , $user){

        $name= time() . '.' . str_random(20) . urlencode($data['images'][0]->getClientOriginalName());

        $data['images'][0]->move(public_path().'/gym/gallery/images/', $name);

        $user->update(['image' => 'public/gym/gallery/images/'.$name ]);

        if ( count($request->file('images')) > 1 ) {

            array_shift($data['images']);

            foreach ( $data['images'] as $image){

                $name= time() . '.' . str_random(20) . urlencode($image->getClientOriginalName());
                $image->move(public_path().'/gym/gallery/images/', $name);

                $gymImage = new  GymImage();
                $gymImage->gym_id = $user->id;
                $gymImage->url = 'public/gym/gallery/images/'.$name;
                $gymImage->save();
            }

        }
    }

    function createDayForGym($data , $user){

        $allDays =  [ ];
        foreach($data['day'] as $value)
        {
            $value['gym_id']    = $user->id;
            $value['time_from'] = $value['start'];
            $value['time_to']   = $value['end']  ;
            $value['start']     = $value['start'] ? date("H:i:s", strtotime($value['start'])) : null;
            $value['end']       = $value['end'] ? date("H:i:s", strtotime($value['end'])) : null;
            $value['working']   = $value['start'] && $value['end'] ? 'open' : 'off';
            $allDays[] = $value;

        }

        GymDay::insert($allDays);
    }
 
    public function show($id)
    {

        $user = User::with('gallery','days.day')->findOrFail($id);

        $filterDays  = collect($user['days'])->filter(function ($value, $key) {
            return $value['start'] != null && $value['end'] != null;
        })->flatten();

        return view('admin.users.show', compact('user' ,'filterDays'));
    }
    


    public function edit($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.users.edit');
    }

   
    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        
    }
    
    public function destroy($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
       
    }
    
    
    public function accpetedUser(Request $request){
        $user = User::findOrFail($request->id);
        
        if ($user){
            $user->update(['is_accepted' => 1]);
            return response()->json( [
                'status' => true ,
            ] , 200 );
            
        }else{
            
            return response()->json( [
                'status' => false
            ] , 200 );
            
        }
        
    }


    public function suspendUser(Request $request){
        $user = User::with('order')->find($request->id);

        if (  $user['order']->where('status' ,'pending')  ){

            return response()->json([
                'status' => false,
                'message' => "عفواً,يوجد لهذا الجيم طلبات حالية  "
            ]);

        }

        $user->update(['is_suspend' => 1 ]);

        return response()->json([
            'status' => true,
            'message' => 'لقد تم   الحظر  بنجاح',
            'id' => $request->id,
            'type' => $request->type

        ]);

    }






}
