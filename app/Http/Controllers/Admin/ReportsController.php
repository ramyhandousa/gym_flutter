<?php

namespace App\Http\Controllers\Admin;

use App\Models\BankTransfer;
use App\Models\Order;
use App\Models\OrderOld;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reports = Order::whereHas('gym',function ($q) use ($request){
            $q->where('name','like', '%' . $request->name . '%');
        })->with('gym')->groupBy('gym_id')->latest()->get();

        return view('admin.Reports.index', compact('reports'));
    }


    public function gym_data(Request $request)
    {

        $id = $request->id;

        $query  = Order::with('gym')->whereGymId($id);


        if ($request->type == 'accepted'){

            $orders =   $query->whereStatus('accepted')->get();

            $pageName = ' الطلبات المقبولة  ';

            return view('admin.orders.accepted', compact('orders' ,'pageName'));


        }elseif ($request->type == 'refuse'){

            $orders =  $query->whereIn('status', ['refuse_user', 'refuse_gym'])->get();

            $pageName = ' الطلبات المرفوضة   ';

            return view('admin.orders.refuse', compact('orders' ,'pageName'));

        }elseif ($request->type == 'finish'){

            $orders =  $query->whereStatus('finish')->with('my_rate')->get();

            $pageName = ' الطلبات المنتهية   ';

            return view('admin.orders.finish', compact('orders' ,'pageName'));

        }else{

            $orders =  $query->whereStatus('pending')->get();

            $pageName = ' الطلبات الجديدة  ';

            return view('admin.orders.waiting',  compact('orders' ,'pageName'));

        }
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $report = BankTransfer::with('user')->findOrFail($id);

        return view('admin.Reports.show', compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date("D", strtotime($data)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);


        $dayName = config('app.locale') == 'ar' ? $ar_day :date("D", strtotime($data));
        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $month = config('app.locale') == 'ar' ? $months[$month] : $month;

//        $data  = [
//            'dayName' => $dayName,
//            'day' => $day,
//            'month' => $month,
//        ];

        return   $dayName . ' ' .  $day . ' ' . $month;
    }

}
