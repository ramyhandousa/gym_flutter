<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Offer;
use App\Models\UserSetting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OfferController extends Controller
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $offers = Offer::latest()->whereDate('period_end','>',today())->get();

        $pageName = 'العروض';

        return view('admin.offers.index',compact('offers','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $model = Offer::findOrFail($request->id);

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }

    public function accepted(Request $request)
    {
        $model = Offer::with('user.devices')->findOrFail($request->id);
        
        $userSetting = UserSetting::whereUserId($model->user_id)->first();
        
        if ($userSetting  ){ // notify Place Your Offer is Accepted
            if( $userSetting->revers_update == true){
                $this->sendNotificationToPlace($model,$request);
            }
            
        }
         $this->sendNotficationNewOffer($model);
        if ($model->update(['is_accepted' => 1])) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }


    private function sendNotificationToPlace($model , $request){
        // notify Place Your Offer is Accepted
        // check if Place in setting revers_update Equal True
        
        $devicesIos =  $model['user']['devices']->where('device_type','Ios')->pluck('device');
        $devicesAndroid =  $model['user']['devices']->where('device_type','android')->pluck('device');

        if(count($devicesIos ) > 0 || count($devicesAndroid ) > 0) {
            $this->push->sendPushNotification($devicesAndroid, $devicesIos, 'العروض',
                ' تم قبول عرضك رقم ' . $model->id ,
                ['type' => 7, 'offerId' => $model->id]
            );
        }
        $this->notify->NotificationDbType(7,$model['user']->id,Auth::id(),$request->id,$model->id);
    }

    private function sendNotficationNewOffer($model){ 
        //send Notification To users want Notify
        // check if Users Has in setting revers_update Equal True
    
        $users = User::select('id')->whereDoesntHave('roles')->whereDoesntHave('abilities')
                        ->where('is_suspend',0)
                        ->where('defined_user','other')
                        ->whereHas('devices')
                        ->whereHas('userSetting',function ($q){
                            $q->where('notify_offer' , 'true');
                        })->with('devices')->get();

        if (count($users) > 0){

            foreach ($users as $user):
                foreach ($user->devices as $device):
                    $devicesIos     =       $device['devices'] ?  $device['devices']->where('device_type','Ios')->pluck('device'): [];
                    $devicesAndroid =   $device['devices'] ? $device['devices']->where('device_type','android')->pluck('device'): [];
                    $this->push->sendPushNotification($devicesAndroid, $devicesIos, 'العروض',
                        'تم إضافة عرض جديد من المطعم' . $model['user']->fullName ,
                        [
                            'type' => 7,
                            'offerId' => $model->id
                        ]
                    );
                endforeach;
            endforeach;
        }

    }

}
