<?php

namespace App\Http\Controllers\Admin;

use App\Models\PlaceClassification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class PlaceClassificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = PlaceClassification::latest()->get();

        $pageName = '  تصنيف المكان';
        return view('admin.PlaceClassification.index',compact('brands','pageName'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageName = '  تصنيف المكان';
        return view('admin.PlaceClassification.create',compact('pageName'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand  = new PlaceClassification();
        $brand->defined_user = $request->defined_user;
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;


        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => '  تصنيف المكان']),
            "url" => route('placeClassification.index'),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $brand = PlaceClassification::findOrFail($id);


        $pageName = '  تصنيف المكان';

        return view('admin.PlaceClassification.edit',compact('brand','pageName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = PlaceClassification::findOrFail($id);
        $brand->defined_user = $request->defined_user;
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;

        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => '  تصنيف المكان']),
            "url" => route('placeClassification.index'),

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function delete(Request $request){
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }

        $model = PlaceClassification::findOrFail($request->id);

        if ($model) {
            $model->delete();
            $model->deleteTranslations();
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }

}
