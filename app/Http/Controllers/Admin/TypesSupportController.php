<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypesSupport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Carbon\Carbon;

class TypesSupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	 $types = TypesSupport::whereIsSuspend(0)->get();
  
	 return view('admin.types_support.index')->with(compact( 'types'));
    }
    
    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {
	 $type = new TypesSupport();
	 $type->name = $request->name_ar;
  
	 $type->save();
	 session()->flash('success', 'لقد تم إضافة  نوع الرسالة  بنجاح.');
	 return redirect(route('types.index'));
    }

  
    public function show($id)
    {
	 $type = TypesSupport::findOrFail($id);
  
	 return view('admin.types_support.show')->with(compact( 'type'));
    }
 
    public function edit($id)
    {
	 $type = TypesSupport::findOrFail($id);
  
	 return view('admin.types_support.edit')->with(compact( 'type'));
    }

    
    public function update(Request $request, $id)
    {
	 $type =  TypesSupport::findOrFail($id);
	 $type->name = $request->name_ar;
	 $type->save();
	 
	 session()->flash('success', 'لقد تم تعديل  نوع الرسالة  بنجاح.');
	 return redirect(route('types.index'));
    }

 
}
