<?php

namespace App\Http\Requests\api;

use App\Models\DayTranslation;
use App\Models\GymDay;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class makeOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date_format:Y-m-d||after:yesterday',
            'time_from' => 'required|date_format:H:i:s',
            'time_to' => 'required|date_format:H:i:s|after:time_from',
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'من فضلك تاريخ الحجز',
            'date.after' => 'من فضلك تأكد من تاريخ الحجز',
            'time_from.required' => 'من فضلك بداية الوقت  ',
            'time_to.required' => 'من فضلك انتهاء الوقت   ',
            'time_to.after' => 'من فضلك انتهاء الوقت يجب ان يكون اكبر من بداية الوقت   ',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $date = $this->date;

            if ($date){
                $unixTimestamp = strtotime($date);

                $dayOfWeek = date("l", $unixTimestamp);

                $dayId =  DayTranslation::where('name', $dayOfWeek)->first();

                $my_day =  GymDay::whereDayId($dayId ? $dayId->day_id : 1);

                if ($my_day->first()){

                    if ($my_day->first()->working == 'off'){

                        $validator->errors()->add('working', 'لا يمكنك حجز هذا اليوم لان الجيم مغلق' );
                    }

                    if (!$my_day->where('start', '<=' , $this->time_from)->where('end', '>=' , $this->time_to)->first()){

                        $validator->errors()->add('place not Found',  'نعتذر ...  خارج نطاق توقيت  الجيم في ذلك اليوم ' );
                    }


                    if (!request()->orderId){

                        $this->checkDateOrder($validator);
//                        $this->checkWalletEmpty($validator);

                    }else{

                        $this->checkEditOrder($validator);
                    }

                }
            }


        });
        return;
    }

    function checkDateOrder($validator){
        $user = User::where('api_token', request()->headers->get('apiToken'))->whereHas('orders',function ($q)  {

            $q->where('gym_id', request()->gymId)->where('date',   request()->date)->where('status','!=','finish');

        })->first();

        if ($user){   return $this->userHaveOrderThisDay($validator);   }

    }

    function checkEditOrder($validator){

        $user = User::where('api_token', request()->headers->get('apiToken'))->whereHas('orders',function ($q){

            $q->where('id','!=', $this->orderId)->where('gym_id', request()->gymId)
                ->where('date', $this->date)->where('status','!=','finish');

        })->first();

        if ($user){ return $this->userHaveOrderThisDay($validator);  }
    }


    function checkWalletEmpty($validator){
        $user = User::where('api_token', request()->headers->get('apiToken'))->whereHas('wallet')->with('wallet')->first();

        if ($user){

            $value = $user['wallet']->sum('value');
            if ( $value <=  0 ){
                return $this->chargeWalletPls($validator);
            }

        }
    }

       
   protected function failedValidation(Validator $validator)
   {

    $values = $validator->errors()->all();

    throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
   }


   private function userHaveOrderThisDay($validator){

       $validator->errors()->add('have_this_day_before',  'لديك حجز مسبق علي هذا اليوم' );

   }

   private function chargeWalletPls($validator){

       $validator->errors()->add('have_this_day_before',  'من فضلك يرجي شحن المحفظة ' );

   }
}
