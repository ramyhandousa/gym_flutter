<?php

namespace App\Http\Requests\api;

use App\Rules\storeOffer;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class OfferStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'period_start' => [   'date', new  storeOffer()],
            'period_end' => 'required|date',
        ];
    }
       
       public function withValidator($validator)
       {
	    $validator->after(function ($validator) {

            if ( $this->period_start >=  $this->period_end ) {
                
                $validator->errors()->add('period_end',trans('global.offer_date_end') );
                
            }
            
            if($this->type == 'meal'){
                
             
                if ($this->mealId == null){
                    $validator->errors()->add('mealId','يجب اختيار الصنف لاستكمال العرض' );
                }    
                
                if ($this->price == null){
                    $validator->errors()->add('price','يجب اختيار السعر لاستكمال العرض' );
                }
            
            }

	    });
	    return;
       }
       protected function failedValidation(Validator $validator) {
	    
	    $values = $validator->errors()->all();
	    
	    throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
       }
}
