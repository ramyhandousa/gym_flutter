<?php

namespace App\Http\Requests\api;

use App\Models\VerifyUser;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;

class checkActivation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $user = User::where('phone',\request()->phone)->first();
//
        return [
//            'phone' => 'unique:users,phone,' . $user->id ,
//            'phone' => 'unique:users,phone,|unique:verify_users,phone,' . $user->id ,
        ];
    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

            $user = User::where('phone', \request()->phone)->first();


            if ($user){

                $myphone =  VerifyUser::where('user_id', '!=', $user->id)->wherePhone($this->phone)->first();

                if ($myphone){

                    $find =  User::where('phone', \request()->phone)->exists();

                    if ( $find ) {

                        $validator->errors()->add('newPhone',  trans('global.unique_phone') );
                    }
                }

            }


        });
        return;
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
