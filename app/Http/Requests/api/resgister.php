<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class resgister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'name' => 'required|max:75',
             'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users|numeric',
            'password' => 'required|min:6',
            // 'cityId' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => trans('validation.required'),
            'email.required' => trans('validation.required'),
            'email.unique' => trans('global.unique_email'),
            'phone.unique' => trans('global.unique_phone'),
            'phone.required' => trans('validation.required'),
            'password.required' => trans('validation.required'),
            'cityId.required' => trans('validation.required'),
        ];
    }
  

    protected function failedValidation(Validator $validator) {
 
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }


}
