<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class dayWorking extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'working' => 'required',
            'start' => 'required_if:working,==,open',
            'end' => 'required_if:working,==,open'
        ];
    }

    public function messages()
    {
        return [
            'start.required_if' => 'بدا الوقت مطلوب طالما المكان غير مغلق',
            'end.required_if' => 'نهاية الوقت  مطلوب طالما المكان غير مغلق',
        ];
    }



    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}
