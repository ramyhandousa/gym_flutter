<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class rate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'rateValue' => 'required|numeric|min:0|max:5',
            'comment' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'rateValue.max' => 'اعلي قيمة للتقيم هي 5 او اقل ',
        ];
    }

    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}
