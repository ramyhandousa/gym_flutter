<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class GymValid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|unique:users',
            'email' => 'required|unique:users',
            'images' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ];
    }


    public function messages()
    {
        return [
            'phone.required' => 'رقم الهاتف مطلوب',
            'phone.unique' => 'هذا الرقم مستخدم من قبل',
            'email.required' => 'البريد الإلكتروني مطلوب',
            'images.required' => 'إختيار صورة واحدة علي الاقل لتكون بروفيل للجيم',
            'email.unique' => 'هذا البريد مستخدم من قبل',
            'password.required' => 'كلمة المرور مطلوبة',
            'confirm_password.same' => 'كلمة المرور غير متطابقة',
        ];
    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

            if (  count($this->images) > 6 ){

                $validator->errors()->add('files', 'اكبر عدد ممكن هو 6 صور');
            }

        });
        return;
    }
}
