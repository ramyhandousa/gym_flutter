<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;
use App\Models\OrderOld;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null){

            $admins= Device::whereDeviceType('web')->pluck('user_id');
            $devicesWeb= Device::whereDeviceType('web')->pluck('device');
             $userIos= Device::where('user_id','!=',$sender)->whereDeviceType('Ios')->pluck('user_id');

             $orderInfo = OrderOld::whereId($order)->with('place')->first();

            switch($type){

            case $type == 1:

                // contactUs Form admin
                //  $sender  admin who send
                // admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                  ];
                  $this->insertData($data);
             break;

            case $type == 2:

                     // contactUs Form Users
                  foreach ($admins as $admin){
                         $data = [
                           'user_id' => $admin ,
                           'sender_id' => $sender ,
                           'title' => trans('global.connect_us'),
                           'body' => $request ,
                           'type' => 2,
                         ];
                         $this->insertData($data);
                  }
                $this->push->sendPushNotification(null, $devicesWeb, 'تواصل معنا', $request);
                  break;

            case $type == 3:

                 $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'order_id' => $order,
                    'title' =>   'الحجوزات'  ,
                    'body' =>   ' لديك طلب حجز جديد يوم ' .
                               $this->getArabicMonth($orderInfo->book_day).'  الساعة ' . date('h:i a ', strtotime($orderInfo->come_time))
                     ,
                    'type' => 3,
                ];
                $this->insertData($data);

             break;

            case $type == 4:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'order_id' => $order,
                    'title' =>   'الحجوزات'  ,
                    'body' =>  ' تم تأكيد الحجز يوم ' . 
                    			$this->getArabicMonth($orderInfo->book_day). ' بمطعم ' . $orderInfo['place']->fullName 
                    ,
                    'type' => 4,
                ];
                $this->insertData($data);
             break;

            case $type == 5:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'order_id' => $order,
                    'title' =>   'الحجوزات'  ,
                    'body' =>  ' تم رفض الحجز يوم ' .
                    		$this->getArabicMonth($orderInfo->book_day) . ' بمطعم ' . $orderInfo['place']->fullName. ' بسبب '. $request->message
                    ,
                    'type' => 5,
                ];
                $this->insertData($data);

             break;

            case $type == 6:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'order_id' => $order,
                    'title' =>   'الحجوزات'  , 
                    'body' =>  ' تم رفض الحجز من قبل العميل يوم ' .
                    		$this->getArabicMonth($orderInfo->book_day) . ' بمطعم ' . $orderInfo['place']->fullName. ' بسبب '. $request->message
                    ,
                    'type' => 6,
                ];
                $this->insertData($data);

             break;
             
             case $type == 7:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'offer_id' => $order,
                    'title' =>   'العروض '  ,
                    'body' =>   $request.' تم قبول عرضك رقم  ' ,
                    'type' => 7,
                ];
                $this->insertData($data);

             break;
             

            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }


    private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date("D", strtotime($data)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);


        $dayName = config('app.locale') == 'ar' ? $ar_day :date("D", strtotime($data));
        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $month = config('app.locale') == 'ar' ? $months[$month] : $month;

        $data = $dayName . ' '.$day . ' ' . $month ;


        return $data;
    }

}
       
       