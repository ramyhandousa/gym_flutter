<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceWorkingTime extends Model
{
     protected $fillable = ['place_id','day_id','start','end','working'];


    protected $hidden = [
        'created_at','updated_at'
    ];



    public static function createOrUpdatePlace($user , $request)
    {

        $place_working_times = self::updateOrCreate(
            ['place_id' => $user->id,'day_id' => $request['dayId']],
            [
                'start' => $request->start,
                'end' => $request->end ,
                'working' => $request->working  ? : 'open',
            ]

        );

        return $place_working_times;
    }
}
