<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Service extends Model
{

    use Translatable;


    public $translatedAttributes = ['name'];

    protected $hidden = [
        'translations'
    ];

}
