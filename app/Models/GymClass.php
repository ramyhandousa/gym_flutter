<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class GymClass extends Model
{
    use Translatable;


    public $translatedAttributes = ['name', 'captain_name','description'];
    protected $fillable = ['name','duration_session','time' ,'image'];

    protected $hidden = [
        'created_at','updated_at' ,'translations'
    ];


    public function daysClass(){

        return $this->hasMany(GymClassDay::class,'gym_classes_id');

    }

    public function daysClassToday(){
        $dayId =  DayTranslation::where('name', getdate()['weekday'])->first();

        return $this->hasOne(GymClassDay::class,'gym_classes_id')
            ->where('day_id' , $dayId->day_id);

    }
}
