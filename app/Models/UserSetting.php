<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $fillable = ['user_id','revers_update','near_revers_date','notify_offer'];


    protected $hidden = [
        'created_at','updated_at'
    ];


    public static function createOrUpdateSetting($user , $request)
    {

        $settingPlace = self::updateOrCreate(
            ['user_id' => $user->id],
            [
                'revers_update' => $request->revers_update ?: "false",
                'near_revers_date' => $request->near_revers_date ?: "false",
                'notify_offer' => $request->notify_offer ?: "false",
            ]

        );

        return $settingPlace;
    }
}
