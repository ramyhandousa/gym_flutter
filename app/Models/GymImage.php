<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GymImage extends Model
{

    protected $hidden = [
        'created_at','updated_at'
    ];

    protected $fillable = ['gym_id' , 'url'];

}
