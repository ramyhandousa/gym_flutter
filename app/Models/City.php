<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
class City extends Model
{
    use Translatable;


    public $translatedAttributes = ['name'];
    protected $fillable = ['name','parent_id','is_suspend'];

    protected $hidden = [
        'created_at','updated_at' ,'translations'
    ];




    public function children()
    {
        return $this->hasMany(City::class, 'parent_id');
    }


    public function parent(){
        return $this->belongsTo(City::class,'parent_id');
    }

       public function users(){
       
	    return $this->hasMany(User::class);
       }


}
