<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'gym_id',
        'price_gym',
        'time_gym',
        'app_percentage',
        'time_from',
        'time_to',
        'date',
        'real_date_enter',
        'real_time_from',
        'real_time_to',
        'over_time',
        'price',
        'status' ,
        'message'
    ];



    protected $appends = ['status_translation'];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function gym(){
        return $this->belongsTo(User::class , 'gym_id');
    }

    public function my_rate(){

        return $this->hasOne(RateOrder::class);
    }

    public function getStatusTranslationAttribute()
    {
        if ( $this->status){
            $status = $this->attributes['status'];
            switch ($status) {
                case 'pending':
                    return 'طلب جديد';
                    break;
                case 'preparing':
                    return 'جاري الطلب';
                    break;
                case 'accepted':
                    return 'تم  الموافقة علي الطلب  ';
                    break;
                case 'refuse_gym':
                    return 'تم رفض الحجز من قبل الجيم';
                    break;
                case 'refuse_user':
                    return 'تم رفض الحجز من قبل العميل';
                    break;
                case 'finish':
                    return 'تم إنتهاء الطلب';
                    break;
                default:
                    return '';
            }
        }
    }
}
