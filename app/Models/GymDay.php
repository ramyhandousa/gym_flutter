<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GymDay extends Model
{
    protected $fillable = ['gym_id','day_id','start','end','working'];


    protected $hidden = [
        'created_at','updated_at'
    ];



    public static function createOrUpdatePlace($user , $request)
    {

        $place_working_times = self::updateOrCreate(
            ['gym_id' => $user->id,'day_id' => $request['day_id']],
            [
                'start' => $request->start,
                'end' => $request->end ,
                'working' => $request->working  ? : 'open',
            ]

        );

        return $place_working_times;
    }


    public function day(){
        return $this->belongsTo(Day::class, 'day_id');
    }
}
