<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GymService extends Model
{
     protected $fillable = ['gym_id','service_id'];


    protected $hidden = [
        'created_at','updated_at'
    ];


    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

    public static function createOrUpdateService($user , $request)
    {
        $GymService = self::updateOrCreate(
                         [
                             'gym_id' => $user->id,
                            'service_id' => $request
                        ]
        );
        return $GymService;
    }

}
