<?php

namespace App\Providers;

use App\Events\AcceptedPlaceOrder;
use App\Events\RefusePlaceOrder;
use App\Events\RefuseClientOrder;
use App\Events\NewOrder;
use App\Listeners\AcceptedPlaceOrderListener;
use App\Listeners\RefusePlaceOrderListener;
use App\Listeners\RefuseClientOrderListener;
use App\Listeners\NewOrderListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NewOrder::class => [
            NewOrderListener::class,
        ],
        AcceptedPlaceOrder::class => [
            AcceptedPlaceOrderListener::class,
        ],
        RefuseClientOrder::class =>[
            RefuseClientOrderListener::class,
        ],

         RefusePlaceOrder::class => [
            RefusePlaceOrderListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
