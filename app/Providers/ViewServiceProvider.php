<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 11/16/2017
 * Time: 9:29 AM
 */

namespace App\Providers;

use App\Libraries\Main;
use App\Models\City;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Support;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
 
            $helper = new \App\Http\Helpers\Images();
            $main_helper = new \App\Http\Helpers\Main();
            $setting = new Setting;
            $main = new Main();

            $usersCount = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->where('defined_user','user')->count();
            $gym_count_admin = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->where('defined_user', 'gym')->count();
            $categoriesCountAdmin = 0;

            $userHelpAdminCount = User::where('id', '!=', \Auth::id())->whereHas('roles', function ($q) {
                $q->where('name', '!=', 'owner');
            })->count();

            $citiesCount = City::where('parent_id',null)->count();
            $cityChildren = City::where('parent_id','!=',null)->count();

            $messageNotReadCount = Support::whereUserId(Auth::id())->whereIsRead(0)->count();
            $messageReadCount =  Support::whereIsRead(1)->count();


            $orderPending = Order::whereStatus('pending')->count();
            $orderAccepted = Order::whereStatus('accepted')->count();
            $orderRefuseByUser = Order::whereStatus('refuse_user')->count();
            $orderRefuseByGym = Order::whereStatus('refuse_gym')->count();
            $orderComplete = Order::whereStatus('finish')->count();
            $orderToday = Order::where('date', '=', Carbon::today())->count();
            $wallet_orders = Order::whereStatus('finish')->get();
            $wallet_orders->map(function ($q){
                $q->my_wallet =   ( ($q->price_gym + $q->price ) * $q->app_percentage  /100 );
            });

            // walletOrders that value must admin pay
            $walletOrders =  $wallet_orders->sum('my_wallet');

            $view->with(
                compact('usersCount','gym_count_admin','cityChildren',
                    'orderToday','orderAccepted','orderPending','orderComplete','orderRefuseByUser','walletOrders',
                    'orderRefuseByGym', 'citiesCount',
                    'userHelpAdminCount','messageNotReadCount','messageReadCount',
                'helper', 'main', 'setting','main_helper','cities', 'branches')
            );
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


