<?php

namespace App\Listeners;

use App\Events\NewOrder;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Notifications\CommentsNotification;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */


   public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NewOrder $event
     * @return void
     */
    public function handle(NewOrder $event)
    {
        $device = Device::where('user_id', $event->request->placeId);
        $devicesIos =  $device->where('device_type','Ios')->pluck('device');
        $devicesAndroid =  Device::where('user_id', $event->request->placeId)->where('device_type','android')->pluck('device');

        $this->push->sendPushNotification($devicesAndroid, $devicesIos, 'الحجوزات',
                ' لديك طلب حجز جديد يوم ' .
               $this->getArabicMonth($event->order->book_day).'  الساعة ' . date('h:i a ', strtotime($event->order->come_time))
                , ['type' => 3, 'orderId' => $event->order->id]
            );
        
        $this->notify->NotificationDbType(3,$event->order->place_id,$event->user->id,$event->request,$event->order->id);

    }
     private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date("D", strtotime($data)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);


        $dayName = config('app.locale') == 'ar' ? $ar_day :date("D", strtotime($data));
        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $month = config('app.locale') == 'ar' ? $months[$month] : $month;

        $data = $dayName . ' '.$day . ' ' . $month ;


        return $data;
    }

}
