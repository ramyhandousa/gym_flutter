<?php

namespace App\Listeners;

use App\Events\AcceptedPlaceOrder;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;

class AcceptedPlaceOrderListener
{


    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  AcceptedPlaceOrder  $event
     * @return void
     */
    public function handle(AcceptedPlaceOrder $event)
    {
        $device = Device::where('user_id', $event->order->user_id);
        $devicesIos =  $device->where('device_type','Ios')->pluck('device');
        $devicesAndroid =  Device::where('user_id', $event->order->user_id)->where('device_type','android')->pluck('device');


        if(count($devicesIos ) > 0 || count($devicesAndroid ) > 0) {

            $this->push->sendPushNotification($devicesAndroid, $devicesIos, 'الحجوزات',

                ' تم تأكيد الحجز يوم ' .$this->getArabicMonth($event->order->book_day)
                ,
                ['type' => 4, 'orderId' => $event->order->id]
            );
        }
        
        $this->notify->NotificationDbType(4,$event->user->id,$event->order->place_id,$event->request,$event->order->id);

        // $event->order->update(['status' => 'accepted']);

    }
    
     private  function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date("D", strtotime($data)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);


        $dayName = config('app.locale') == 'ar' ? $ar_day :date("D", strtotime($data));
        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $month = config('app.locale') == 'ar' ? $months[$month] : $month;

        $data = $dayName . ' '.$day . ' ' . $month ;


        return $data;
    }

}
