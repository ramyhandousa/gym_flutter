<?php

namespace App\Listeners;

use App\Events\RefuseClientOrder; 
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification; 
use App\Models\Device; 

class RefuseClientOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
   public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  RefuseClientOrder  $event
     * @return void
     */
    public function handle(RefuseClientOrder $event)
    {
        $device = Device::where('user_id', $event->order->place_id);
        $devicesIos =  $device->where('device_type','Ios')->pluck('device');
        $devicesAndroid =  Device::where('user_id', $event->order->place_id)->where('device_type','android')->pluck('device');

        if(count($devicesIos ) > 0 || count($devicesAndroid ) > 0) {

            $this->push->sendPushNotification($devicesAndroid, $devicesIos, 'الحجوزات',

                '  تم رفض الحجز من قبل العميل يوم ' . ' بسبب '. $event->request->message
                ,
                ['type' => 6, 'orderId' => $event->order->id]
            );
        }
        $this->notify->NotificationDbType(6,$event->order->place_id,$event->user->id,$event->request,$event->order->id);

    //   $event->order->update(['status' => 'refuse_client', 'message' => $event->request->message]);

    }
}
