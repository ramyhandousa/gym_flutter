<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGymClassDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gym_class_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gym_classes_id')->unsigned();
            $table->foreign('gym_classes_id')->references('id')->on('gym_classes')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('day_id')->unsigned();
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade')->onUpdate('cascade');
            $table->time('time');
            $table->string('duration_session');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gym_class_days');
    }
}
