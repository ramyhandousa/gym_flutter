<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'defined_user' =>  'cafe',
            'phone' =>  Str::random(20),
            'first_name' => Str::random(10),
            'last_name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('secret'),
//            'city_id' => 1,
//            'is_active' => 1,
        ]);

//        $count = 2;
//        factory(\App\User::class, $count)->create();
    }
}
