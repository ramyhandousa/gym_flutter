<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGymClassTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gym_class_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gym_class_id')->unsigned();
            $table->string('locale')->index();

            // Required For Business
            $table->string('name');
            $table->string('captain_name');
            $table->string('description');

            $table->unique(['gym_class_id', 'locale']);
            $table->foreign('gym_class_id')->references('id')->on('gym_classes')->onDelete('cascade')->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gym_class_translations');
    }
}
