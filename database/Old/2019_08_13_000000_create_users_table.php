<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('defined_user', ['admin', 'user','gym', 'other'])->default('user');
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('image')->nullable();
            $table->string('latitute')->nullable();
            $table->string('longitute')->nullable();
            $table->string('address')->nullable();
            $table->string('price')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('password');
            $table->string('api_token');
            $table->tinyInteger('is_active')->default('0');
            $table->tinyInteger('is_suspend')->default('0');
            $table->string('message')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
