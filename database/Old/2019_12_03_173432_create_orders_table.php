<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('gym_id')->unsigned();
            $table->foreign('gym_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('price_gym');
            $table->string('time_gym');
            $table->string('app_percentage');
            $table->time('time_from');
            $table->time('time_to');
            $table->date('date');
            $table->string('qr_code');
            $table->time('real_time_from');
            $table->time('real_time_to');
            $table->time('over_time')->nullable();
            $table->float('price');
            $table->enum('status', ['pending', 'preparing', 'accepted', 'refuse_user', 'refuse_gym', 'finish'])->default('pending');
            $table->string('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
